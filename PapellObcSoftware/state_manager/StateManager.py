##################################################
# PAPELL
# first created by Robin Schweigert
# modified by Martin (23.3.2018)
# modified by Chris
#
#
# The StateManager is the central routine for the
# overall experiment. It manages the the whole
# procedure and starts all necessary additional
# threads.
#
##################################################
import os
import time
from multiprocessing import Process, Value
import Main
from helpers.ChangeListener import ChangeListener

# Imports for IdleState
from helpers.ConfigRead import ConfigReader

# Imports for Operations
from magnets.magnets import Magnets
from aktuators.transfer import PumpingSystem
from camera.cameras import Camera
from sensors.sensor_high_level import Sensor

from helpers.PiCom import PiCom

from state_manager.Logger import Logger, LogLevel
from state_manager.PiShutdown import PiShutdown


class StateManager:
    framerate = 2

    IDLESTATE = 0
    SAFESTATE = 1
    SAVETHEASTRONAUT = 2
    OPERATIONSTATE = 3

    safe_state_sig = Value('b', False)
    save_the_astronaut_sig = Value('b', False)
    changed_files_sig = Value('b', False)

    current_state = Value('i', 0)

    def __init__(self):
        """
        initiates the state manager with all necessary variables
        """
        Logger.get_instance().log(LogLevel.INFO, "StateManager", "Initializing StateManager.")
        self.__running = True
        self.states = [
            IdleState(self),
            SafeState(self),
            SaveTheAstronaut(self),
            OperationState(self)
        ]
        self.__current_state = self.states[0]

        # collect all threads/processes
        self.threads = []

        # additional setup
        Logger.get_instance().log(LogLevel.INFO, "StateManager", "Initializing Pi Shutdown.")
        self.__pi_shutdown = PiShutdown(19, self)  # ToDo: from electronic: USB_+_SENSE
        self.__save_the_astronaut_thread = Process(target=self.__pi_shutdown.shutdowncheck, args=())
        self.__save_the_astronaut_thread.start()
        self.threads.append(self.__save_the_astronaut_thread)

        # Setting up Database
        from helpers.Database import QueuedDatabase
        self.queue = QueuedDatabase("Database/"+Logger.get_instance().new_beginning(".db", "Database/")+"_NameDatabase")
        self.queue.start()

        # Initializing PSU
        from psu.psu import PSU
        self.psu = PSU(self)
        self.psu.psu.all_info()

        # Check for systems with malfunctions
        from state_manager.initialcheck import i2c_action
        self.psu.enter_execution()
        i2c = i2c_action()
        i2c.detect()
        self.psu.exit_execution()
        self.change_listener = ChangeListener(self)

        Logger.get_instance().log(LogLevel.INFO, "StateManager", "going in 5 seconds sleep before Main Loop.")
        time.sleep(5)

        try:
            if os.path.exists(Main.swapp_folder + "logs/"):
                os.system("sudo rm -r " + Main.swapp_folder + "logs/")
            os.makedirs(Main.swapp_folder + "logs/")

            if os.path.exists(Main.swapp_folder + "Database/"):
                os.system("sudo rm -r " + Main.swapp_folder + "Database/")
            os.makedirs(Main.swapp_folder + "Database/")

            if os.path.exists(Main.swapp_folder + "videos/"):
                os.system("sudo rm -r " + Main.swapp_folder + "videos/")
            os.makedirs(Main.swapp_folder + "videos/")

            if os.path.exists(Main.swapp_folder + "updatefolder/"):
                os.system("sudo rm -r " + Main.swapp_folder + "updatefolder/")
        except Exception as e:
            Logger.get_instance().log(LogLevel.EXCEPTION, "StateManager",
                                      str("copy logs and/or tidy up swapp folder did not work %s " % str(e)))

#        try:
#            if not os.path.exists(Main.swapp_folder + "logs/"):
#                os.makedirs(Main.swapp_folder + "logs/")
#            if not os.path.exists(Main.swapp_folder + "Database/"):
#                os.makedirs(Main.swapp_folder + "Database/")
#            if not os.path.exists(Main.swapp_folder + "videos/"):
#                os.makedirs(Main.swapp_folder + "videos/")
#            from helpers.data_copy_delete import DataCopyDelete
#            DataCopyDelete.get_instance().move_old_stuff_to_safe_folder()
#            DataCopyDelete.get_instance().check_for_copystuff()
#            if os.path.exists(Main.swapp_folder + "updatefolder/"):
#                os.system("sudo rm -r " + Main.swapp_folder + "updatefolder/")
#        except Exception as e:
#            Logger.get_instance().log(LogLevel.EXCEPTION, "StateManager",
#                                      str("copy logs/ to swapp folder did not work %s " % str(e)))

        # main loop
        while self.__running:
            try:
                self.__main_loop()
            except Exception as e:
                Logger.get_instance().log(LogLevel.ERROR, "StateManager", "Exception occured: " + str(e))
                time.sleep(1)

    def get_state(self):
        return StateManager.current_state.value

    def __reset(self):
        """
        resets the whole state manager to the initial state
        """
        Logger.get_instance().log(LogLevel.WARNING, "StateManager", "Reset of StateManager not yet supported.")

    def terminate_threads(self):
        self.psu.active_log_loop = False  # psu logging thread
        self.psu.active_check_loop = False  # psu checking thread
        self.queue.running = False  # Database thread
        self.__pi_shutdown.__running = False  # Pi Shutdown thread

    def reinit(self):
        Logger.get_instance().__init__()
        Logger.get_instance().log(LogLevel.INFO, "StateManager", "Reinitialize Database and Logger")
        # self.psu.stop_logging()
        from helpers.Database import QueuedDatabase
        QueuedDatabase.running.value = False
        time.sleep(2)
        QueuedDatabase("Database/"+Logger.get_instance().new_beginning(".db", "Database/")+"_NameDatabase").start()

        # self.psu.start_logging()

    def current_time(self):
        """
        :returns the current time in milliseconds
        """
        return int(round(time.time() * 1000))

    def change_state(self, new_state):
        """
        changes the current state
        :param new_state: the new state to run
        """
        self.__current_state = self.states[new_state]
        Logger.get_instance().log(LogLevel.INFO, "StateManager", "Switching to state %s" % self.__current_state.name)
        StateManager.current_state.value = new_state
        # self.reinit()

    def __main_loop(self):
        """
        main loop of the state manager, running at given framerate
        """
        Logger.get_instance().log(LogLevel.INFO, "StateManager", "StateManager is entering main loop.")
        while self.__running:
            self.last_execution = self.current_time()
            # execution block
            self.__current_state.__run__()
            # end of execution block
            # checking execution time
            if self.current_time() - self.last_execution < 1 / StateManager.framerate * 1000:
                time.sleep(((1 / StateManager.framerate * 1000) - (self.current_time() - self.last_execution)) / 1000)
            else:
                Logger.get_instance().log(LogLevel.WARNING, "StateManager",
                                          "Can't reach target fps, current fps: %s" % (
                                              1 / (self.current_time() - self.last_execution) * 1000))
        self.__end()

    def stop_states(self):
        """
        function to stop the state manager, current execution will still finish
        """
        self.__running = False

    def __end(self):
        """
        end is executed after the main loop at the end of the state manager
        """
        Logger.get_instance().log(LogLevel.WARNING, "StateManager", "StateManager shutting down.")


class AbstractState:
    def __init__(self, state_manager):
        self._state_manager = state_manager
        self.name = "AbstractState"

    def __run__(self):
        if self._state_manager.save_the_astronaut_sig.value:
            self._state_manager.change_state(self._state_manager.SAVETHEASTRONAUT)
            return
        elif self._state_manager.safe_state_sig.value:
            self._state_manager.change_state(self._state_manager.SAFESTATE)
            return


class IdleState(AbstractState):
    def __init__(self, state_manager):
        super().__init__(state_manager)
        self.name = "IdleState"
        self.finished = Value('b', False)
        self.config_read_process = None

        self.__config_reader = ConfigReader.get_instance()

    def __run__(self):
        super().__run__()
        if self.config_read_process is None:
            self._state_manager.psu.charging()

            self.finished.value = False
            self.config_read_process = Process(target=self.__read_config, args=())
            self.config_read_process.start()
        if self.finished.value:
            self.finished.value = False
            self.config_read_process = None
            if not self._state_manager.safe_state_sig.value and not self._state_manager.save_the_astronaut_sig.value:
                self._state_manager.change_state(StateManager.OPERATIONSTATE)

    def __read_config(self):
        while (not self.finished.value) and (not self._state_manager.safe_state_sig.value) and \
                (not self._state_manager.save_the_astronaut_sig.value):
            while not self.__config_reader.check_for_config():
                time.sleep(0.1)

            # read in config
            try:
                self.__config_reader.parse_config()
                self.finished.value = True
            except Exception as e:
                self.__config_reader.update_version()
                Logger.get_instance().log(LogLevel.WARNING, "IdleState", "Could not parse Config. "
                                                                         "Exception occurred: "+str(e))
                Logger.get_instance().log(LogLevel.WARNING, "IdleState", "%s is ignored"
                                          % self.__config_reader.get_config_name())


class OperationState(AbstractState):
    def __init__(self, state_manager):
        super().__init__(state_manager)
        self.name = "OperationState"
        self.finished = Value('b', False)
        self.operation_process = None
        self.__state_manager = state_manager

        """Get Instance of Config Reader for operations"""
        self.__config_reader = ConfigReader.get_instance()

        '''Create objects for all the hardware'''
        self.mag_ea = None
        self.camera = None
        self.pump_sys = None
        self.sensors = None

        '''Processes'''
        self.sensor_process = None

        """Flags"""
        self.__is_pumping = False
        self.__is_recording = False
        self.__video_counter = 1

        # Communication
        self.picom = PiCom()

    def __run__(self):
        super().__run__()
        if self.operation_process is None:

            # Setting up initial variables
            self.sensor_process = None

            """Flags"""
            self.__is_pumping = False
            self.__is_recording = False
            self.__video_counter = 1
            self.finished.value = False

            # Start experiment when battery charge status is high enough
            if self._state_manager.psu.SOC() >= self.__config_reader.bat_charge.value:
                self.operation_process = Process(target=self.__operations, args=())
                self.operation_process.start()
        if self.finished.value:
            self.finished.value = False
            self.operation_process = None
            if not self._state_manager.safe_state_sig.value and not self._state_manager.save_the_astronaut_sig.value:
                self._state_manager.change_state(StateManager.IDLESTATE)

    def __operations(self):

        # Switch to execution mode - Turn on 5V and 12V
        self._state_manager.psu.enter_execution()

        # Create Objects for all components
        self.mag_ea = Magnets(self.__config_reader.ea.value)
        self.camera = Camera()  # ToDo Different Cameras
        self.pump_sys = PumpingSystem(self._state_manager)
        self.sensors = Sensor()

        magnets_old = []
        time_old = 0

        if self.__config_reader.ea.value == 2:
            self.picom.send_command("p", self.__config_reader.version.value)

        for operation in self.__config_reader.operations.get():  # for all tasks from config file

            # sleep for given amount of time
            time.sleep(operation.time-time_old)
            time_old = operation.time

            # Check if safe state or save the astronaut is triggered
            if self._state_manager.safe_state_sig.value or self._state_manager.save_the_astronaut_sig.value:
                break

            Logger.get_instance().log(LogLevel.INFO, "OperationState",
                                      "Time: %s, Pumps: %s, Camera: %s, Sensors: %s"
                                      % (operation.time, operation.pump, operation.cameras, operation.sensors))

            """ ---------- Magnets ---------- """
            # 1. switch off magnets
            for magnet_old in magnets_old:
                if not (magnet_old in operation.magnets):
                    self.mag_ea.disable(magnet_old)

            # 2. switch on magnets
            for magnet in operation.magnets:
                if not (magnet in magnets_old):
                    self.mag_ea.switch_on(magnet)

            magnets_old = operation.magnets

            """ ---------- Pumps ---------- """
            if operation.pump and not self.__is_pumping:

                self.pump_sys.start((self.pump_sys.EA_1 if self.__config_reader.ea.value == 1 else self.pump_sys.EA_2),
                                    (self.pump_sys.PUMP_1 if self.__config_reader.default_pump.value == 1 else self.pump_sys.PUMP_2))
                self.__is_pumping = True
            elif not operation.pump and self.__is_pumping:
                self.pump_sys.stop()
                self.__is_pumping = False

            """ ---------- Motors ---------- """
            # ToDo motors

            """ ---------- Cameras ---------- """
            if operation.cameras and not self.__is_recording:
                if self.__config_reader.ea.value == 1:
                    filename = "videos/EA_1_Experiment_" + str(self.__config_reader.version.value).zfill(2) \
                               + "_" + str(self.__video_counter).zfill(2) + ".h264"
                    self.camera.start_recording(filename)
                else:
                    self.picom.send_command("sv")
                    t = time.time()
                    self.picom.send_command(str(t))
                    Logger.get_instance().log(LogLevel.INFO, "OperationState",
                                              str("Camera for EA2 is recording (hopefully)! [%s]" % str(t)))
                    # ToDo Camera on EA2

                self.__is_recording = True
            elif not operation.cameras and self.__is_recording:
                if self.__config_reader.ea.value == 1:
                    self.camera.stop_recording()
                else:
                    self.picom.send_command("av")
                    pass  # ToDo Camera on EA2
                self.__is_recording = False
                self.__video_counter += 1

            """ ---------- Sensors ---------- """
            if operation.sensors and self.sensor_process is None:
                self.sensor_process = Process(target=self.take_sensor_data, args=())
                self.sensor_process.start()
            elif (not operation.sensors) and (self.sensor_process is not None):
                self.sensor_process.terminate()
                self.sensor_process = None

        # Disable/Stop all the components, finishing the experiment
        self.mag_ea.clear_all()
        if self.__is_pumping:
            self.pump_sys.stop()
            self.__is_pumping = False
        # ToDo motors
        if self.__is_recording:
            self.camera.stop_recording()
            self.__is_recording = False
        if self.sensor_process is not None:
            self.sensor_process.terminate()
            self.sensor_process = None

        self.finished.value = True
        self.__state_manager.change_listener.swap()

    """Additional functions for Multiprocessing"""
    # Sensor
    def take_sensor_data(self):
        while (not self._state_manager.safe_state_sig.value) and (not self._state_manager.save_the_astronaut_sig.value):
            self.sensors.data_full()
            time.sleep(1)


class SafeState(AbstractState):
    def __init__(self, state_manager):
        super().__init__(state_manager)
        self.name = "SafeState"
        self.recovered = Value('b', False)
        self.recover_process = None

    def __run__(self):
        if self.recover_process is None:
            self.recover_process = Process(target=self.__recover, args=())
            self.recover_process.start()
        if self.recovered.value:
            self.recovered.value = False
            self.recover_process = None
            Logger.get_instance().log(LogLevel.INFO, "SafeState", "The experiment was successfully recovered.")
            self._state_manager.change_state(StateManager.IDLESTATE)

    def __recover(self):
        Logger.get_instance().log(LogLevel.INFO, "SafeState", "The SafeState was triggered.")

        try:
            from camera.cameras import Camera
            cam = Camera()
            cam.stop_recording()
        except Exception as e:
            Logger.get_instance().log(LogLevel.WARNING, "SafeState", "Could not stop recording. Error: %s" % e)

        # 1. Turn off Battery Power
        self._state_manager.psu.exit_execution()
        self._state_manager.psu.psu.disconnect_batteries()

        # 1.1 Flag
        is_safe = False

        # 1.2 Trying to recover
        while not is_safe:
            is_safe = True
            Logger.get_instance().log(LogLevel.INFO, "SafeState", "Trying to recover.")

            # 2. Try to disable Pumps and Valves
            try:
                from aktuators.transfer import PumpingSystem
                pump_sys = PumpingSystem(self._state_manager)
                pump_sys.stop()

                """for pump in pump_sys.pumps:
                    try:
                        pump.stop()
                    except Exception as e:
                        # is_safe = False
                        Logger.get_instance().log(LogLevel.ERROR, "SafeState",
                                                  "%s is not Working. Staying in Safe mode until it is fixed! Error: %s"
                                                  % (pump, e))
                for valve in pump_sys.valves:
                    try:
                        valve.close()
                    except Exception as e:
                        # is_safe = False
                        Logger.get_instance().log(LogLevel.ERROR, "SafeState",
                                                  "%s is not Working. Staying in Safe mode until it is fixed! Error: %s"
                                                  % (valve, e))"""
            except Exception as e:
                # is_safe = False
                Logger.get_instance().log(LogLevel.EXCEPTION, "SafeState",
                                          "Could not import PumpingSystem. Staying in Safe mode until fixed! Error: %s" % e)

            # 3. Read Temperature data on OBC and Sensor board
            try:
                from sensors.temperature_mcp9808 import Temperature
                from state_manager.initialcheck import i2c_action
                from config.configurable_variables import TEMP_MIN, TEMP_MAX
                temp_sens = Temperature(address=i2c_action.Addr_TempSens_SensBoard)
                temp_obc = Temperature(address=i2c_action.Addr_TempSens_obc)

                try:
                    read_sens = temp_sens.read()
                    read_obc = temp_obc.read()
                    Logger.get_instance().log(LogLevel.INFO, "SafeState",
                                              "Temperature sensor on Sensorboard reads: %s" % read_sens)
                    Logger.get_instance().log(LogLevel.INFO, "SafeState",
                                              "Temperature sensor on OBC reads: %s" % read_obc)
                    if not (TEMP_MIN < int(read_sens) < TEMP_MAX):
                        is_safe = False
                        Logger.get_instance().log(LogLevel.WARNING, "SafeState",
                                                  "Temperature on Sensorboard critical. Staying in Safe Mode.")
                    if not (TEMP_MIN < int(read_obc) < TEMP_MAX):
                        is_safe = False
                        Logger.get_instance().log(LogLevel.WARNING, "SafeState",
                                                  "Temperature on OBC critical. Staying in Safe Mode.")
                except Exception as e:
                    is_safe = False
                    Logger.get_instance().log(LogLevel.ERROR, "SafeState",
                                              "Could not read Temperature sensors. "
                                              "Staying in Safe mode until fixed! Error: %s" % e)
            except Exception as e:
                is_safe = False
                Logger.get_instance().log(LogLevel.EXCEPTION, "SafeState",
                                          "Could not import Temperature Sensor stuff. Staying in Safe mode until fixed! Error: %s" % e)

            # 4. Turn on 5V/12V to test Magnets
            """if is_safe:
                self._state_manager.psu.enter_execution()

                try:
                    from magnets.magnets import Magnets
                    mag_ea = Magnets(Magnets.EA_1)
                    if not mag_ea.clear_all():
                        is_safe = False
                        Logger.get_instance().log(LogLevel.EXCEPTION, "SafeState",
                                          "Could not turn off magnets. "
                                          "Staying in Safe mode until fixed!")
                except Exception as e:
                    is_safe = False
                    Logger.get_instance().log(LogLevel.EXCEPTION, "SafeState",
                                              "Could not import Magnets. "
                                              "Staying in Safe mode until fixed! Error: %s" % e)

                self._state_manager.psu.charging()"""

            # 5. sleep for 30 minutes
            if not is_safe:
                Logger.get_instance().log(LogLevel.WARNING, "SafeState",
                                          "Could not recover. Trying again in 30 minutes.")
                time.sleep(60*30)

        self._state_manager.safe_state_sig.value = False
        self.recovered.value = True


class SaveTheAstronaut(AbstractState):
    def __init__(self, state_manager):
        super().__init__(state_manager)
        self.name = "SaveTheAstronautState"

    def __run__(self):
        Logger.get_instance().log(LogLevel.WARNING, "SaveTheAstronaut",
                                  "SaveTheAstronaut triggered, terminating all operations and shutting down.")
        print("shutting down system, testing only")
        # os._exit(0)
        os.system("/usr/bin/sudo /sbin/shutdown -h 0")
