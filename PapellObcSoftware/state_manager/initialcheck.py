##################################################
# PAPELL
# first created by Chris
# modified by -- Martin Siedorf
# successfully tested --
#
# The Main file is the entrance point of our software.
# It starts with an update of the whole software,
# excluding the Main file itself for security
# purposes, This file must be secured completely so
# nothing can go wrong, no matter what happens
# afterwards. After a finished update, the
# state manager is started.
#
##################################################
# print("no PSUTest needed for now")
# from psu.PSUTest import PSUTest
#from psu.psu import PSU
from state_manager.Logger import Logger, LogLevel


def check_actuatorboard():
    # check activity of actuatorboard with each component first !
    from helpers.PCA9557 import PCA9557 as portexpander
    import time

    gpio_actuatorboard = True

    if gpio_actuatorboard is True:
        exp1 = portexpander(address=0x18)
        exp2 = portexpander(address=0x19)

        exp1.output(1, 1)  # makes motor go
        time.sleep(.3)
        # ToDo: include Ampere measurement
        exp1.output(1, 0)  # stop motor

        exp1.output(7, 1)  # makes Ventil1 open
        exp1.output(6, 1)  # makes Ventil2 open

        time.sleep(.3)
        # ToDo: include Ampere measurement

        exp1.output(7, 0)  # makes Ventil1 close
        exp1.output(6, 0)  # makes Ventil2 close

        # start motor check
        exp1.output(2, 1)  # defines the step size
        exp1.output(3, 1)  # defines the step size (see datasheet)
        exp1.output(4, 1)  # enables motor to turn

        # turn motor for 180 deg
        for i in range(50):
            exp1.output(5, 1)
            exp1.output(5, 0)
            time.sleep(.01)  # longer time makes it turn slower

        input("Turning all off? (very important, press enter!!)")
        for i in range(9):
            exp1.output(i, 0)
            exp2.output(i, 0)


class i2c_action():
    """
    This class defines all Addresses on the OBC.
    This class gives the availability variables to each to each component.
    """

    gpio_sensorboard = False
    gpio_actuatorboard = False
    gpio_EA1 = False
    gpio_EA2 = False

    gpio_expander2 = False
    gpio_expander1 = False
    Switch1 = False  # for Fuel Gauge
    Switch_Mag = False  # on Sensorboard for 4 MGM
    AC_converter1 = False
    AC_converter2 = False
    LED = False
    Fuel_gauge = False
    Adc_current = False
    TempSens_obc = False
    TempSens_sensboard = False
    SoundSens = False
    TempSens_SensBoard = False
    Acc = False
    Magnetometer = False

    Addr_exp1 = 0x18  # GPIO Expander 1
    Addr_exp2 = 0x19  # GPIO Expander 2
    Addr_Switch1 = 0x72  # i2c Switch 1
    Addr_Switch_Mag = 0x70  # i2c Switch 2
    # Addr_ac_converter1 = 0x20  # not active, A/D Converter
    """
    0x23 is doubled. First instance is now disabled.
    """
    #Addr_ac_converter2 = 0x23  # ea 1, A/D Converter 
    Addr_LED = 0x32  # LED Controller
    Addr_ADC_Curr_Act = 0x4B  # ADC Current Sensor on Actuator board
    Addr_TempSens_obc = 0x1b  # Temperature Sensor on PSU
    Addr_TempSens_SensBoard = 0x1f  # Temperature Sensor on Sensor board
    Addr_ADC_Sound = 0x49  # Sound Sensor
    Addr_Acc = 0x48  # Acceleration Sensor

    Addr_ea2_adc1 = 0x21  #strom sensoren
    Addr_ea2_adc2 = 0x22
    Addr_ea1_adc1 = 0x23
    Addr_ea1_adc2 = 0x24

    Pin_exp1_pump = 1
    Pin_exp1_valve1 = 6
    Pin_exp1_valve2 = 7
    Pin_exp2_pump = 1
    Pin_exp2_valve3 = 6
    Pin_exp2_valve4 = 7
    Pin_exp1_motor_direct = 0
    Pin_exp2_motor_direct = 0

    """
    Addr_Switch1 makes all Fuel_Gages with each address Addr_Fuel_Gauge 0x55 available.
    Addr_Switch2 makes all Magnetometer with each the address Addr_Mgm (0x1b) available
    """

    Addr_Fuel_Gauge = 0x55  # Fuel Gauge or PSU
    Addr_Mgm = 0x1e  # Magnetometer 1-4 over Switch2 on Sensor Board

    def __init__(self):

        self.bus_number = 1  # 1 indicates /dev/i2c-1
        self.device_count = 0
        try:
            from camera.lightning import Lightning
            light = Lightning(9)
        except Exception as eee:
            Logger.get_instance().log(LogLevel.EXCEPTION, "i2cDetect", "Import of Lightning for LEDf detection failed")

    def detect(self):

        try:
            import smbus
            import errno
        except Exception:
            Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "Import of smbus or errno failed")
        bus = smbus.SMBus(self.bus_number)
        self.device_count = 0
        #self.enter_execution()
        for device in range(3, 128):
            try:
                bus.write_byte(device, 0)
                Logger.get_instance().log(LogLevel.VERBOSE, "i2cDetect", "Found Address {0}".format(hex(device)))
                self.device_count = self.device_count + 1

                if hex(device) == hex(self.Addr_exp1):
                    i2c_action.gpio_expander1 = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "GPIO Expander 1 is accessible")
                elif hex(device) == hex(self.Addr_exp2):
                    i2c_action.gpio_expander2 = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "GPIO Expander 2 is accessible")
                elif hex(device) == hex(self.Addr_Switch1):
                    i2c_action.Switch1 = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "i2c Switch1 for Fuel Gage is accessible")
                elif hex(device) == hex(self.Addr_Switch_Mag):
                    i2c_action.Switch_Mag = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "switch2 on Sensorboard is accessible")
                # elif hex(device) == hex(self.Addr_ac_converter1):
                    # i2c_action.AC_converter1 = True
                    # Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "ADC1 is accessible")
                #elif hex(device) == hex(self.Addr_ac_converter2):
                    #i2c_action.AC_converter2 = True
                    #Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "ADC 1 is accessible")
                elif hex(device) == hex(self.Addr_LED):
                    i2c_action.LED = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "LED is accessible")
                elif hex(device) == hex(self.Addr_Fuel_Gauge):
                    i2c_action.Fuel_gauge = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "Fuel Gauge is accessible")
                elif hex(device) == hex(self.Addr_ADC_Curr_Act):
                    i2c_action.Adc_current = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "ADC Current Sensor is accessible")
                elif hex(device) == hex(self.Addr_TempSens_obc):
                    i2c_action.TempSens_obc = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "Temperature Sensor on OBC is accessible")
                elif hex(device) == hex(self.Addr_TempSens_SensBoard):
                    i2c_action.TempSens_SensBoard = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect",
                                              "Temperature Sensor on Sensor board is accessible")
                elif hex(device) == hex(self.Addr_ADC_Sound):
                    i2c_action.SoundSens = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "ADC for Sound Sensor is accessible")
                elif hex(device) == hex(self.Addr_Acc):
                    i2c_action.Acc = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "ADC for Accelerometer Sensor is accessible")
                elif hex(device) == hex(self.Addr_Mgm):
                    i2c_action.Magnetometer = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "Magnetometer is accessible")

                elif hex(device) == hex(self.Addr_ea2_adc1):
                    i2c_action.MGM1_ea1 = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "ADC1 on EA1 is accessible")
                elif hex(device) == hex(self.Addr_ea2_adc2):
                    i2c_action.MGM1_ea2 = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "ADC2 on EA1 is accessible")
                elif hex(device) == hex(self.Addr_ea1_adc1):
                    i2c_action.MGM1_ea1 = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "ADC1 on EA2 is accessible")
                elif hex(device) == hex(self.Addr_ea1_adc2):
                    i2c_action.MGM2_ea2 = True
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "ADC2 on EA2 is accessible")
                # ToDo: implement mgm text
                else:
                    pass
            except IOError as e:
                if e.errno != errno.EREMOTEIO:
                    Logger.get_instance().log(LogLevel.INFO, "i2cDetect",
                                              "Error: {0} on address {1}".format(e, hex(device)))
            except Exception as e:  # exception if read_byte fails
                Logger.get_instance().log(LogLevel.INFO, "i2cDetect",
                                          "Error unk: {0} on address {1}".format(e, hex(device)))

        self.gpio_actuatorboard = self.gpio_expander1 & self.gpio_expander2
        self.gpio_sensorboard = self.TempSens_sensboard & self.SoundSens & self.Acc & self.Switch_Mag

        Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "Aktuator board is accessible")
        bus.close()
        bus = None
        Logger.get_instance().log(LogLevel.INFO, "i2cDetect", "Found {0} device(s)".format(self.device_count))
        if self.device_count > 100:
            Logger.get_instance().log(LogLevel.WARNING, "i2cDetect", "All I2C addresses were found."
                                                                     "Our I2C bus and our complete project is screed ")
            i2c_action.gpio_sensorboard = False
            i2c_action.gpio_actuatorboard = False
            i2c_action.gpio_EA1 = False
            i2c_action.gpio_EA2 = False

            i2c_action.gpio_expander2 = False
            i2c_action.gpio_expander1 = False
            i2c_action.Switch1 = False
            i2c_action.Switch_Mag = False
            i2c_action.AC_converter1 = False
            i2c_action.AC_converter2 = False
            i2c_action.LED = False
            i2c_action.Fuel_gauge = False
            i2c_action.Adc_current = False
            i2c_action.TempSens_obc = False
            i2c_action.SoundSens = False
            i2c_action.Acc = False
            i2c_action.Magnetometer = False
        else:
            pass
        #self.exit_execution()

'''
def Battery_test_old():
    # written by Chris
    PSU = PSUTest()

    print("____________________________________")
    print("Battery Charging Status:... %s%%" % (PSU.BATTERIES.getStateOfCharge()))
    print("Battery Voltage:........... %smV" % (PSU.BATTERIES.get_voltage()))
    print("Battery Current Capacity:.. %smAh" % (PSU.BATTERIES.get_capacity()))
    print("Battery Full Capacity:..... %smAh" % (PSU.BATTERIES.get_full_capacity()))
    print("Battery Design Capacity:... %smAh" % (PSU.BATTERIES.get_design_capacity()))
    print("Charger Temperature:....... %sC" % (round(PSU.BATTERIES.getTemperature() * 0.1 - 273.15, 2)))
    print("Charger Internal Temp:..... %sC" % (round(PSU.BATTERIES.getIntTemperature() * 0.1 - 273.15, 2)))
    print("Used Power for Charging:... %smW" % (PSU.BATTERIES.get_power()))
    Logger.log(Logger.INFO, "Used Current for Charging" % (PSU.BATTERIES.get_current()))
'''

# if __name__ == "__main__":
