##################################################
# PAPELL
# first created by Jan-Erik Brune
# modified by Chris; Moritz
# successfully tested
#
# This is the test procedure for Houston Pre-Flight Tests
#
##################################################


"""
This is the test, that will be run at nanoracks to check our system.

What can not be done:
-Valves can not be opened
-Motors can not be moved
-Pumps can not be activated


What should be done:
-show power consumption of the experiment
-demonstrate showing up as USB-Device

Programm:

-initialise PSU
-run demo of magnets
-go to charging
-do shutdown whenever cable is pulled

"""



import psu.PSUTest as PSU
import time
import magnets.magnets as mag
from state_manager.Logger import Logger, LogLevel
# PSU init

class HoustonMarch:

    def __init__(self):
        self.psu = PSU.PSUTest()
        self.initialize()
        time.sleep(0.5)
        self.psu.system_info()
        self.mag1 = mag.Magnets(1)
        self.mag2 = mag.Magnets(2)

    def initialize(self):
        self.psu.info_log("", "Doing Alex Routine")
        time.sleep(1)
        if self.psu.connect_batteries():
            time.sleep(1)
            self.psu.disconnect_usb_power()
            time.sleep(1)
            self.psu.connect_vext()
            time.sleep(1)
            self.psu.disconnect_vext()
            time.sleep(1)
            self.psu.connect_usb_power()
            time.sleep(1)
            self.psu.disconnect_batteries()
            time.sleep(1)

    def start(self):
        self.psu.connect_batteries()
        time.sleep(1)
        self.psu.disconnect_usb_power()

        self.mag_run_1()
        self.mag_run_2()
        self.charge()

        #while true, mit system info

    def mag_run_1(self):
        magn = [16, 17, 18, 22, 23, 24, 25, 29, 30, 31]
        try:
            for i in range(9):
                self.mag1.switch_on(magn[i])
            time.sleep(60)
            self.mag1.clear_all()
            time.sleep(120)

        except Exception as e:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Houstentest", "Could not turn on magnets for ea1:%s" % e)
            self.mag1.clear_all()

    def mag_run_2(self):
        magn = [2, 3, 12, 13, 16, 17, 18, 19, 22, 23]
        try:
            for i in range(9):
                self.mag2.switch_on(magn[i])
            time.sleep(60)
            self.mag2.clear_all()
            time.sleep(120)

        except Exception as e:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Houstentest", "Could not turn on magnets for ea2:%s" % e)
            self.mag2.clear_all()

    def charge(self):

        try:
            self.psu.connect_batteries()
            time.sleep(1)
            self.psu.connect_usb_power()
            time.sleep(1800)
            self.psu.disconnect_batteries()
            
        except Exception as e:
            Logger.get_instance().log(LogLevel.INFO, "PSU", "Could not charge:%s" % e)


