##################################################
# PAPELL
# first created by Robin Schweigert
# modified by --
# successfully tested --
#
# The config reader is able to continuously read
# the config files. It stores default values for
# all parameters internally. While reading the
# file the internal parameters are adjusted
# accordingly.
#
##################################################
import time
import xml.etree.ElementTree as et
from multiprocessing import Value

from helpers.Singleton import Singleton
from state_manager.Logger import Logger, LogLevel


@Singleton
class ConfigReader:

    def __init__(self):
        """
        all default values are initiated
        """
        self.ENABLED = Value('b', False)
        self.CR_WAIT = Value('d', 1.0)

        self.running = True

    def run(self):
        """
        continuously reading the config file
        """
        while self.running:
            tree = et.parse("config/papell.xml")
            root = tree.getroot()
            # config reader settings
            cr = root.find("ConfigReader")
            if cr is not None:
                try:
                    self.CR_WAIT.value = float(cr.find("wait").text)
                except ValueError as e:
                    Logger.get_instance().log(LogLevel.EXCEPTION, "ConfigReader", str(e))

            # misc settings
            misc = root.find("misc")
            if misc is not None:
                enabled = misc.find("enabled")
                if enabled is not None:
                    if enabled.text == "0":
                        if self.ENABLED.value:
                            Logger.get_instance().log(LogLevel.INFO, "ConfigReader",
                                                      "Setting ENABLED to FALSE.")
                            self.ENABLED.value = False
                    elif enabled.text == "1":
                        if not self.ENABLED.value:
                            Logger.get_instance().log(LogLevel.INFO, "ConfigReader",
                                                      "Setting ENABLED to TRUE.")
                            self.ENABLED.value = True

            time.sleep(self.CR_WAIT.value)
