##################################################
# PAPELL
# first created by Moritz Sauer
# modified by chris
# successfully tested --
#
# Charging script
#
#
##################################################
from state_manager.Logger import Logger, LogLevel
from state_manager.initialcheck import i2c_action

from state_manager.initialcheck import i2c_action
#from WIP_automatic_test import Test
import time



if __name__ == "__main__":
    from helpers.Database import QueuedDatabase
    queue = QueuedDatabase("Database/" + Logger.get_instance().new_beginning(".db", "Database/")+"database_charging")
    queue.start()
    from psu.psu import PSU
    temp = PSU(None)
    try:
        temp.charging()

        temp.psu.system_info()
        print("charging till death")
        time.sleep(10)
        temp.psu.system_info()
        while True:
            time.sleep(600)
            temp.psu.system_info()
        input("finishing charging")
        temp.psu.connect_vext()
        temp.psu.disconnect_batteries()

    except Exception as e:
        print(e)
    print("test finished")
