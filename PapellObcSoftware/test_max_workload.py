def is_prime(n):
    i = 2
    while i < n:
        if n % i == 0:
            return False
        i += 1
    return True

n = int(input("What number should I go up to? "))

p = 2
while p <= n:
    if is_prime(p):
        print(p),
    p = p+1

print("Done")
