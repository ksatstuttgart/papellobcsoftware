import ivport
import os


# raspistill capture
def capture(camera, i):
    "This system command for raspistill capture"
    cmd = "raspistill -t 10 -o pics/still_CAM%d_%s.jpg" % (camera, i)
    os.system(cmd)

iv = ivport.IVPort(ivport.TYPE_QUAD2)
for i in range(0, 10):
    print(i)
    iv.camera_change(1)
    capture(1, i)
    iv.camera_change(3)
    capture(3, i)

print("close")
iv.close()
