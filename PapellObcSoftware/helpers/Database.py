##################################################
# PAPELL
# first created by Robin Schweigert
# modified by --
# successfully tested 09.05.2018 by Robin Schweigert
#
# The database implements the central storing point
# for all measurement values. This Database is
# already synchronized.
#
##################################################

import sqlite3
import time
from multiprocessing import Process, Queue, Value
import random
from helpers.Singleton import Singleton
from state_manager.Logger import LogLevel, Logger


class QueuedDatabase:

    db_queue = Queue()
    running = Value("b")

    def __init__(self, db_name):
        self.db_name = db_name
        #self._state_manager = state_manager
        QueuedDatabase.running.value = True
        self.con = sqlite3.connect(str(self.db_name) + ".db")
        self.cur = self.con.cursor()

        # add tables
        self.create_psu_database()
        self.create_sensor_database()
  #      self.add_table("test1", ["test1", "test2", "test3"])
   #     self.add_table("test2", ["test1", "test2", "test3"])
        self.read_thread = Process(target=self.read_queue, args=())



    def add_table(self, name,  column_lst):
        # TODO urgent: remove after testing
        try:
            self.cur.execute("DROP TABLE %s;" % name)
        except:
            Logger.get_instance().log(LogLevel.EXCEPTION,"Database", "could not delete database " + name)
        create_str = "CREATE TABLE IF NOT EXISTS %s(" % name
        for i in range(0, len(column_lst)):
            if i == 0:
                create_str += column_lst[i] + " text"
            else:
                create_str += ", " + column_lst[i] + " text"
        create_str += ");"
        self.cur.execute(create_str)

    def create_psu_database(self):
        #database for each battery or database for whole psu?
        #new database every restart? (so we can get a db down if we want)
        # i will always save the mode in the database, so you can directly see if charging, idle or execution

        data = [ "mode", "SOC",]
        for a in range(5):
            i = str(a)
            data.append(str("BAT" + i + "_Voltage"))
            data.append(str("BAT" + i + "_Current"))
            data.append(str("BAT" + i + "_SOC"))
            data.append(str("BAT" + i + "_current_Capacity"))
            data.append(str("BAT" + i + "_full_capacity"))
            data.append(str("BAT" + i + "_Temperature"))
            data.append(str("BAT" + i + "_internal_Temperature"))
            data.append(str("BAT" + i + "_Power_Usage"))
            data.append(str("BAT" + i + "_Flags_A"))
            data.append(str("BAT" + i + "_Flags_B"))
            data.append(str("BAT" + i + "_MaxError"))
            data.append(str("BAT" + i + "_StateOfHealth"))
            data.append(str("BAT" + i + "_Avg_Time_to_Empty"))
            data.append(str("BAT" + i + "_Time_to_full"))
            data.append(str("BAT" + i + "_Learned_Status"))
            data.append(str("BAT" + i + "_Cycle_Count"))

        cr_str = str("time")
        for item in data:
            cr_str = cr_str + ", " + item
        create_str = str("CREATE TABLE IF NOT EXISTS psu (%s)" % cr_str)
        try:
            self.cur.execute(create_str)
            self.con.commit()
        except:
            Logger.get_instance().log(LogLevel.EXCEPTION,"Database", "could not create psu database")

    def create_sensor_database(self):
        data = ["temp_obc", "temp_sensor", "mag1", "mag2", "mag3", "mag4", "sound",  "acc", "cur_ac", "cur_ac2"]
        for i in range(8):
            data.append("cur_ea1_1%s" % str(i))
            data.append("cur_ea1_2%s" % str(i))

        for i in range(8):
            data.append("cur_ea2_1%s" % str(i))
            data.append("cur_ea2_2%s" % str(i))

        cr_str = str("time")
        for item in data:
            cr_str = cr_str + ", " + item
        create_str = str("CREATE TABLE IF NOT EXISTS sensors (%s)" % cr_str)
        try:
            self.cur.execute(create_str)
            self.con.commit()
        except:
            Logger.get_instance().log(LogLevel.EXCEPTION,"Database", "could not create psu database")

    @staticmethod
    def columns_to_str(colums):
        cr_str = str(colums[0])
        if len(colums) > 1:
            for col in colums[1:]:
                cr_str = cr_str + ",  " + col
        return cr_str

    @staticmethod
    def psu_insert(values, columns=""):
        QueuedDatabase.insert_value(str("psu (%s)" % str(QueuedDatabase.columns_to_str(columns))), tuple(values))

    @staticmethod
    def sensor_insert(values, columns=""):
        QueuedDatabase.insert_value(str("sensors (%s)" % str(QueuedDatabase.columns_to_str(columns))), tuple(values))

    def read_queue(self):
        while QueuedDatabase.running.value:
            try:
                while QueuedDatabase.db_queue.qsize() > 0 and QueuedDatabase.running.value:
                    temp_val = QueuedDatabase.db_queue.get()
                    try:
                        self.cur.execute("INSERT INTO %s VALUES %s" % (str(temp_val[0]), temp_val[1]))

                    except:
                        Logger.get_instance().log(LogLevel.EXCEPTION, "Database", "Error, could not insert "
                                                  + str(temp_val[1]) + " into database " + str(temp_val[0]))
                        Logger.get_instance().log(LogLevel.EXCEPTION, "Database",
                                                  str(len(temp_val[1])) + str(len(temp_val[0])))
                try:
                    self.con.commit()
                except:
                    Logger.get_instance().log(LogLevel.EXCEPTION, "Database",
                                              "Error, could not commit database " + str(self.db_name))
            except:
                Logger.get_instance().log(LogLevel.EXCEPTION, "Database", "Error reading db queue")
            time.sleep(0.1)

    def start(self):
        self.read_thread.start()

    def stop(self):
        QueuedDatabase.running.value = False

    @staticmethod
    def insert_value(table, values):
        try:

            QueuedDatabase.db_queue.put((table, values))
        except:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Database",
                                      "Error, could not insert value " + str( values) + " to queue")

    def get_all(self, table):
        return self.cur.execute("SELECT * FROM %s" % table)

def fetch_ten(db,  table):
    while True:
        db.cur.execute("SELECT * FROM %s ORDER BY time DESC" % table)
        Logger.get_instance().log(LogLevel.EXCEPTION, "Database", str(db.cur.fetchmany(10)))
        time.sleep(0.5)

def test_add(db, i):
    while True:
        db.insert_value("test1", (i, random.randint(0, 100), random.randint(0, 100)))
        db.insert_value("test2", (i, random.randint(0, 100), random.randint(0, 100)))
        test_values = [time.time()]
        for a in range(82):
            test_values.append(random.random()*100)
        db.psu_insert(test_values)
        time.sleep(2)


if __name__ == '__main__':
    queue = QueuedDatabase("examples")
    queue.start()
    threads = []
    for i in range(0, 10):
        thread = Process(target=test_add, args=[queue, i])
        thread2 = Process(target=fetch_ten, args=[queue, "psu"])
        thread.start()
        thread2.start()
        threads.append(thread2)
        threads.append(thread)
    time.sleep(2)

    time.sleep(10)
    for thread in threads:
        thread.terminate()
    queue.stop()
    time.sleep(1)
    print("test1:")
    for res in queue.get_all("test1"):
        print(res)
    print("test2:")
    for res in queue.get_all("test2"):
        print(res)
