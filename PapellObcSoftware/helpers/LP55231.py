##########Python library module for ST LP55231 LED breakout board#############
# lp55231.py
# Implementation file for LP55231 breakout board Python Library.
#
# ----copied from----
# Byron Jacquot @ SparkFun Electronics
# October 21, 2016
# https://github.com/sparkfun/SparkFun_LP55231_Arduino_Library
# -------------------
#
# Can be instanciated two different ways:
# As an Lp55231 object, offering simple control over the IC, with minimum ROM footprint.
# TODO As an Lp55231Engines object, adding diagnostic and execution engine support.
#
###############################################################################

from helpers.i2c import I2C
import RPi.GPIO as GPIO

class LP55231(I2C):
    """
    Simple base class
    Allows direct control over the LED outputs, and basic chip features like output current setting
    """

    NumChannels = 9
    NumFaders = 3
    NumEngines = 3
    NumInstructions = 96

    # register stuff
    REG_CNTRL1 = 0x00
    REG_CNTRL2 = 0x01
    REG_RATIO_MSB = 0x02
    REG_RATIO_LSB = 0x03
    REG_OUTPUT_ONOFF_MSB = 0x04
    REG_OUTPUT_ONOFF_LSB = 0x05

    # Per LED control channels - fader channel assig, log dimming enable, temperature compensation
    REG_D1_CTRL = 0x06
    REG_D2_CTRL = 0x07
    REG_D3_CTRL = 0x08
    REG_D4_CTRL = 0x09
    REG_D5_CTRL = 0x0a
    REG_D6_CTRL = 0x0b
    REG_D7_CTRL = 0x0c
    REG_D8_CTRL = 0x0d
    REG_D9_CTRL = 0x0e

    # 0x0f to 0x15 reserved

    # Direct PWM control registers
    REG_D1_PWM = 0x16
    REG_D2_PWM = 0x17
    REG_D3_PWM = 0x18
    REG_D4_PWM = 0x19
    REG_D5_PWM = 0x1a
    REG_D6_PWM = 0x1b
    REG_D7_PWM = 0x1c
    REG_D8_PWM = 0x1d
    REG_D9_PWM = 0x1e

    # 0x1f to 0x25 reserved

    # Drive current registers
    REG_D1_I_CTL = 0x26
    REG_D2_I_CTL = 0x27
    REG_D3_I_CTL = 0x28
    REG_D4_I_CTL = 0x29
    REG_D5_I_CTL = 0x2a
    REG_D6_I_CTL = 0x2b
    REG_D7_I_CTL = 0x2c
    REG_D8_I_CTL = 0x2d
    REG_D9_I_CTL = 0x2e

    # 0x2f to 0x35 reserved

    REG_MISC = 0x36
    REG_PC1 = 0x37
    REG_PC2 = 0x38
    REG_PC3 = 0x39
    REG_STATUS_IRQ = 0x3A
    REG_INT_GPIO = 0x3B
    REG_GLOBAL_VAR = 0x3C
    REG_RESET = 0x3D
    REG_TEMP_CTL = 0x3E
    REG_TEMP_READ = 0x3F
    REG_TEMP_WRITE = 0x40
    REG_TEST_CTL = 0x41
    REG_TEST_ADC = 0x42

    # 0x43 to 0x44 reserved

    REG_ENGINE_A_VAR = 0x45
    REG_ENGINE_B_VAR = 0x46
    REG_ENGINE_C_VAR = 0x47

    REG_MASTER_FADE_1 = 0x48
    REG_MASTER_FADE_2 = 0x49
    REG_MASTER_FADE_3 = 0x4A

    # 0x4b Reserved

    REG_PROG1_START = 0x4C
    REG_PROG2_START = 0x4D
    REG_PROG3_START = 0x4E
    REG_PROG_PAGE_SEL = 0x4f

    # Memory is more confusing - there are 6 pages, sel by addr 4f
    REG_PROG_MEM_BASE = 0x50
    # REG_PROG_MEM_SIZE = 0x#
    REG_PROG_MEM_END = 0x6f

    REG_ENG1_MAP_MSB = 0x70
    REG_ENG1_MAP_LSB = 0x71
    REG_ENG2_MAP_MSB = 0x72
    REG_ENG2_MAP_LSB = 0x73
    REG_ENG3_MAP_MSB = 0x74
    REG_ENG3_MAP_LSB = 0x75

    REG_GAIN_CHANGE = 0x76

    def __init__(self, address=0x32, busId=1):
        self.Enable()
        super(LP55231, self).__init__(busId)
        self._address = address

        #self.Reset()

        # Set enable bit
        self.WriteReg(self.REG_CNTRL1, 0x40)

        # enable internal clock & charge pump & write auto increment
        self.WriteReg(self.REG_MISC, 0x53)

    def Enable(self):
        try:
            GPIO.setmode(GPIO.BCM)
        except:
            pass
        GPIO.setup(21, GPIO.OUT)
        GPIO.output(21, GPIO.HIGH)

    def Disable(self):
        val = self.ReadReg(self.REG_CNTRL1)
        val &= ~0x40
        self.WriteReg(self.REG_CNTRL1, val)

    def Reset(self):
        # force reset
        self.WriteReg(self.REG_RESET, 0xff)

    def SetChannelPWM(self, channel, value):
        if channel >= self.NumChannels:
            return 0

        self.WriteReg(self.REG_D1_PWM + channel, value)
        return 1

    def SetMasterFader(self, fader, value):
        if fader >= self.NumFaders:
            return 0

        self.WriteReg(self.REG_MASTER_FADE_1 + fader, value)
        return 1

    def SetLogBrightness(self, channel, enable):
        if channel >= self.NumChannels:
          return 0

        regVal = self.ReadReg(self.REG_D1_CTRL + channel)
        if enable:
            bitVal = 0x20
        else:
            bitVal = 0x00
        regVal &= ~0x20
        regVal |= bitVal
        self.WriteReg(self.REG_D1_CTRL + channel, regVal)

        return 1

    def SetDriveCurrent(self, channel, value):
        if channel >= self.NumChannels:
            return 0

        self.WriteReg(self.REG_D1_I_CTL + channel, value)
        return 1

    def AssignChannelToMasterFader(self, channel, fader):
        if channel >= self.NumChannels:
            return 0
        elif fader >= self.NumFaders:
            return 0

        regVal = self.ReadReg(self.REG_D1_CTRL + channel)
        bitVal = (fader + 1) & 0x03
        bitVal <<= 6
        regVal &= ~0xc0
        regVal |= bitVal
        self.WriteReg(self.REG_D1_CTRL + channel, regVal)

        return 1

# private base class member functions.

    def ReadReg(self, reg):
        return self._readRegister(self._address, reg)

    def WriteReg(self, reg, val):
        self._writeRegister(self._address, reg, val)
