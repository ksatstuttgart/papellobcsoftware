"""
created by robin on 28/02/18
"""
from multiprocessing import Process, Value, Queue
import time
import os
from helpers.Singleton import Singleton
from state_manager.Logger import Logger, LogLevel

@Singleton
class ConfigReader:

    def __init__(self):

        self.operations = Queue()
        self.ea = Value('i', 1)
        self.default_pump = Value('i', 0)
        self.version = Value('i', 0)
        self.bat_charge = Value('i', 0)

        self.__start_time = self.current_time_millis()
        self.__config = None
        self.__version = None
        self.__config_name = None
        self.__config_version_filename = "config/experiment_config_version"

    def check_for_config(self):
        """
        Checks for new Config files. Looks after beginning numbers (e.g. 02_Lin...) of config files.
        :return: Returns True if a new config file is available
        """

        # ToDo eventuell aus Loggerklasse (Logger.new_beginning)
        try:
            with open(self.__config_version_filename) as __version_check:
                self.__version = int(__version_check.read())
        except FileNotFoundError:
            with open(self.__config_version_filename, 'w+') as __version_check:
                __version_check.write("1")
                self.__version = 1
        except Exception as e:
            Logger.get_instance().log(LogLevel.Error, "ConfigReader", "Could not load version: %s" % e)

        self.__config_name = [file for file in os.listdir('config') if file.startswith(str(self.__version).zfill(3))]

        if self.__config_name:
            with open("config/" + self.__config_name[0], 'r') as __conf:
                self.__config = __conf.read().splitlines()
            Logger.get_instance().log(LogLevel.INFO, "ConfigReader", "Read in config: %s" % self.__config_name[0])
            return True
        else:
            self.__config = None
            return False

    def parse_config(self):
        conf_lst = self.__config
        title = conf_lst[1].split(";")[0].replace("#", "")
        version = conf_lst[0].split(";")[0].replace("#PAPELL EA", "")
        bat = conf_lst[3].split(";")[0].split(":")[1]
        print("title: %s" % title)
        print("version: %s" % version)
        print("bat: %s" % bat)
        default_pumps = int(conf_lst[2].split(";")[0].split(":")[1].replace("#Pump:", ""))
        default_ea = int(version.split(".")[0])
        steps = []
        for i in range(5, len(conf_lst)):
            current_conf = conf_lst[i].split(";")
            time = current_conf[0]
            magnets = current_conf[1].split(",")
            cameras = current_conf[2]
            sensors = current_conf[3]
            pump = current_conf[4]
            motor = current_conf[5]

            # make integer
            time = int(time) if time.strip() else 0
            magnets = [int(i) for i in magnets] if magnets[0].strip() else []
            cameras = int(cameras) if cameras.strip() else 0
            sensors = int(sensors) if sensors.strip() else 0
            pump = int(pump) if pump.strip() else 0
            motor = int(motor) if motor.strip() else 0
            steps.append(TimeStep(time, magnets, cameras, sensors, pump, motor))

        # Check for bad config file
        time_old = -1
        for step in steps:
            if step.time <= time_old:
                raise Exception("We cannot go back in time!")
            time_old = step.time
            for magnet in step.magnets:
                if magnet > 37:
                    raise Exception("We only have 37 Magnets on EA 1. NO Magnet: %s" % magnet)
            if not (cameras == 0 or cameras == 1):
                raise Exception("Camera can either be on(1) or off(0). NOT: %s " % cameras)
            if not (sensors == 0 or sensors == 1):
                raise Exception("Sensors can either be on(1) or off(0). NOT: %s " % sensors)
            if not (pump == 0 or pump == 1):
                raise Exception("Pump can either be on(1) or off(0). NOT: %s " % pump)
            if not (motor == 0 or motor == 1):
                raise Exception("Motor can either be on(1) or off(0). NOT: %s " % motor)

        if not (default_pumps == 1 or default_pumps == 2):
            raise Exception("Default pump must be 1 or 2. NOT: %s" % default_pumps)
        if not (default_ea == 1 or default_ea == 2):
            raise Exception("Default ea must be 1 or 2. NOT: %s" % default_ea)

        self.operations.put(steps)
        self.ea.value = default_ea
        self.default_pump.value = default_pumps
        self.version.value = int(version.split(".")[1])
        self.bat_charge.value = int(bat)
        self.update_version()
        Logger.get_instance().log(LogLevel.INFO, "ConfigReader", "Parsed config %s [Version: %s]" % (title, version))

    def update_version(self):
        with open(self.__config_version_filename, 'w') as __version_check:
            __version_check.write(str(self.__version+1))

    def get_config_name(self):
        """
        :return: Returns name of last read config
        """
        if self.__config_name:
            return str(self.__config_name[0])

        else:
            return "NameNotFound"

    def current_time_millis(self):
        return int(time.time() * 1000)


class TimeStep:

    def __init__(self, time, magnets, cameras, sensors, pumps, motors):
        self.time = time
        self.magnets = magnets
        self.cameras = cameras
        self.sensors = sensors
        self.pump = pumps
        self.motors = motors
