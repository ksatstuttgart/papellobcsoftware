##################################################
# PAPELL
# first created by Tobias Ott
# modified by --
# successfully tested --
#
# This class implements a basic functionality
# for the AD7997 A/D-Converter
#
##################################################

from smbus import SMBus


class AD7998:

    # address pointer register values
    CONVERSION_RESULT_REGISTER = 0b0000
    CONFIGURATION_REGISTER = 0b0010     # unused

    def __init__(self, address, bus_id=1):
        self._i2c = SMBus(bus_id)
        self._i2c_address = address

    def __del__(self):
        """Clean up routines."""
        try:
            # Remove SMBus connection
            del self._i2c
        except:
            pass

    def analog_input(self, pin):
        """
        Returns a value between 0 and 2^12 of a given analog lane
        :param pin: pin from 0 to 7
        :return:
        """

        if not (0 <= pin < 8):
            print("invalid pin")
            return None
        else:
            # Set up the lane to be read from
            cmd_addr_point_byte = (1 << 7) | ((pin) << 4) | self.CONVERSION_RESULT_REGISTER

            # Read from Conversion result register (4 bit channel, 12 bit data)
            read_byte = self._i2c.read_i2c_block_data(self._i2c_address, cmd_addr_point_byte, 2)

            # check if rec. chan. data is from correct chan.
            if ((read_byte[0] >> 4) & 0b0111) == (pin):

                read_byte[0] &= 0b1111
                return (read_byte[0] << 8) | read_byte[1]
            else:
                print("Error, no correct data from adc")
                return None

