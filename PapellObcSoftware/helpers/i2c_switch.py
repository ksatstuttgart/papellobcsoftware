#!/usr/bin/env python3
# -*- coding: utf-8 -*-
##################################################
# PAPELL
# first created by "chris"
# successfully tested "Date 17.01.2017" by "chris"
#
# Class to control I2C-Switch to reach FuelGauges
#
##################################################

from smbus import SMBus
import RPi.GPIO as GPIO


class I2C_Switch:

    def __init__(self, address=0x70, pin_reset=18):

        # setup reset pin ToDo insert pin_reset for psu
        self._pin_reset = pin_reset
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(pin_reset, GPIO.OUT)
        GPIO.output(pin_reset, GPIO.HIGH)

        self._address = address
        self._bus = SMBus(1)
        self._CHANNEL = [0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80]

    @staticmethod
    def log(msg):
        print(msg)

    def openCH(self, nbr: int):
        try:
            self._bus.write_byte_data(self._address, 0x00, self._CHANNEL[nbr])
        except Exception as e:
            I2C_Switch.log("Couldn't switch to Channel %s: %s" % (nbr,e))

    def readValue(self, nbr, address, register, length=2):
        try:
            self.openCH(nbr)
            if length == 2:
                return self._bus.read_word_data(address, register)
            if length == 1:
                return self._bus.read_byte_data(address, register)
        except:
            I2C_Switch.log("Could not read i2c")
            return -1

    def _writeValue(self):
        pass

    def reset(self):
        GPIO.output(self._pin_reset, GPIO.LOW)
        GPIO.output(self._pin_reset, GPIO.HIGH)
