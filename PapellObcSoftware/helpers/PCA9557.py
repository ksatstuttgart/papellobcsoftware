#!/usr/bin/python


'''##################################################
# PAPELL
# first created by Tobias Ott
# modified by --
# successfully tested by Tobias Ott
#
# This class implements a fully functional
# PCA9557 I2C Port Expander.
#
##################################################'''

from smbus import SMBus


class PCA9557:

    # Define states and modes
    OUTPUT = 3
    INPUT = 2
    HIGH = 1
    LOW = 0

    def __init__(self, address, busid=1):
        """
        Creates a new PortExpander with a specified i2c address
        :param address:
        :param busid:
        """
        self._i2c = SMBus(busid)
        self._i2c_address = address

        self._port_values = 0x00
        self._port_active = 0xFE

        self._i2c.write_byte_data(self._i2c_address, 0x01, self._port_values)
        self._i2c.write_byte_data(self._i2c_address, 0x03, self._port_active)

        for i in range(0, 8):
            self.activate(i)

    def __del__(self):
        """ Clean up routines. """
        try:
            # Remove SMBus connection
            del self._i2c
        except:
            pass

    def __str__(self):
        return str("Expander @ %s" % hex(self._i2c_address))

    def output(self, pin, state):
        """
        Sets an output pin to LOW/ground or high/Vcc. Same as GPIO.output
        :param pin:
        :param state:
        :return:
        """

        if pin == 0:
            self._port_active = (self._port_active >> 1 << 1) | (1-state)
            self._i2c.write_byte_data(self._i2c_address, 0x03, self._port_active)
        else:
            if state is self.LOW:
                self._port_values &= ~(0x01 << pin)  # Clear bit

            else:
                self._port_values |= (0x01 << pin)   # Set bit

        self._i2c.write_byte_data(self._i2c_address, 0x01, self._port_values)

    def activate(self, pin):
        self._port_active &= ~(0x01 << pin)
        self._i2c.write_byte_data(self._i2c_address, 0x03, self._port_active)

    def deactivate(self, pin):
        self._port_active |= (0x01 << pin)
        self._i2c.write_byte_data(self._i2c_address, 0x03, self._port_active)


import time

if __name__ == "__main__":
    exp = PCA9557(0x18)

    for i in range(8):
        exp.activate(i)

    for i in range(8):
        exp.output(i, exp.HIGH)
        time.sleep(0.1)
        exp.output(i, exp.LOW)

    for i in range(8):
        exp.deactivate(i)

    for i in range(8):
        exp.output(i, exp.HIGH)
        time.sleep(0.1)
        exp.output(i, exp.LOW)

    for i in range(8):
        exp.activate(i)

    for i in range(8):
        exp.output(i, exp.HIGH)
        time.sleep(0.1)
        exp.deactivate(i)
