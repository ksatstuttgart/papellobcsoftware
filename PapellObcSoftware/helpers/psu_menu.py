#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 20:37:02 2017

@author: chris
Class to create Menus
"""
#
#def testfunction():
#    print("Hallo Welt")


class Item(object):
    def __init__(self,label, function=None, kind ="item"):
        self.kind = kind
        self.label = label
        self.function = function
    
    def draw(self):
        print(self.label)
        
    def execute(self):
        if self.function is None:
            print(self.label)
        else:
            self.function()


class Menu(object):
    def __init__(self, name, parent=None, items=None):
        self.name  = name 
        self.parent = parent
        self.items = items or []
        self.indent = "   "
    
    def getName(self):
        return self.name
    
    def drawmenu(self):
        i=1
        print()
        print(self.name)
        for item in self.items:
            print(end=self.indent)
            print(i, end=" ")
            i=i+1
            item.draw()
        if self.parent is not None:
            print(end=self.indent)
            print("0 back to " + self.parent.getName())
    
    def draw(self):
        print(self.name)
        
    def addItem(self, item):
        self.items.append(item)
    
    def addMenu(self,menu):
        self.items.append(menu)
        menu.parent = self       
    
    def removeItem(self,item):
        if item in self.items:
            self.items.remove(item)
    
    def execute(self):
        a = 1
        while a:
            self.drawmenu()
            choice = input(">>")
            
            try:
                if choice == "q": 
                    break
                elif choice == "b" or choice == "0": 
                    self.parent.execute()
                    break
                elif int(choice) in range(1,len(self.items)+1):
                    self.items[int(choice)-1].execute()
                    if isinstance(self.items[int(choice)-1],Menu): break
                else:
                    raise(ValueError)
            except TypeError:
                print("TypeError")
            except ValueError:
                print("Not a possible choice")
#            
#root= Menu("Root Menu")
#root.addItem(Item("Erstes Dings"))
#root.addItem(Item("Zweites Dings",testfunction))
#Submenu = Menu("Submenu")
#Submenu.addItem(Item("Erstes unteres Dings"))
#root.addMenu(Submenu)
#root.execute()
