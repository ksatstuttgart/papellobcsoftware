#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 18:39:18 2017

@author: chris
"""

from smbus import SMBus


class bq27441g1(object):

    def __init__(self,adress=0x55,bus=1):
        self._adress= adress
        self._bus = SMBus(bus)

    def _writeValue(self,cmd, text):
        self._bus.write_i2c_block_data(self._adress,cmd,text)

    def _readValue(self,cmd):
        try:
            return self._bus.read_word_data(self._adress,cmd)
        except:
            Logger.log(Logger.ERROR, "Couldn't read i2c bus")
            return -1

    def _readSignedValue(self,cmd):
        value=self._readValue(cmd)
        if (value<=32767):
            return value
        else:
            return (value-65535)


    def getTemperature(self): #0x02,0x03
        """returns Temperature in 0.1K"""
        return self._readValue(0x02)

    def getIntTemperature(self): #0x1e,0x1f
        """returns Internal Temperature in 0.1K"""
        return self._readValue(0x1e)

    def getVoltage(self): #0x04,0x05
        """return Voltage in mV"""
        return self._readValue(0x04)

    def getCurrent(self): #0x10,0x11, signed
        """returns Current in mA"""
        return self._readSignedValue(0x10)

    def getPower(self): #0x18,0x19, signed
        """returns Power in mW"""
        return self._readSignedValue(0x18)

    def getCapacity(self): #0x0C,0x0D
        """returns Capacity in mAh"""
        return self._readValue(0x0C)

    def getFullCapacity(self): #0x0E,0x0F
        """returns Capacity when full in mAh"""
        return self._readValue(0x0E)

    def getDesignCapacity(self): #0x08,0x09
        """returns Capacity when full in mAh"""
        return self._readValue(0x0E)

    def getStateOfCharge(self): #0x1C,0x1D
        """returns State of Charge in %"""
        return self._readValue(0x1C)
