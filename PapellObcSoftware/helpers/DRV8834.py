# translated from https://github.com/laurb9/StepperDriver/blob/master/src/DRV8834.cpp
import time

from helpers.PCF8574 import PCF8574


class DRV8834:

    DIR_CW = 1
    DIR_CCW = 0

    def __init__(self, expander):
        self.dir_pin = 0
        self.step_pin = 5
        self.m0_pin = 2
        self.m1_pin = 3
        self.expander = expander

        self.expander.output(self.dir_pin, 0)
        self.expander.output(self.m0_pin, 1)
        self.expander.output(self.m1_pin, 0)
        self.expander.output(4, 1)

    def step(self, steps):
        self.expander.output(4, 1)
        for i in range(0, steps):
            self.expander.output(self.step_pin, 1)
            time.sleep(0.001)
            self.expander.output(self.step_pin, 0)
            time.sleep(0.001)
        self.expander.output(4, 1)
    def set_dir(self, dir):
        if dir == 0 or dir == 1:
            self.expander.output(self.dir_pin, dir)
