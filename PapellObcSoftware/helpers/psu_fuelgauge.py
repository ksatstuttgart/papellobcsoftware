#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##################################################
# PAPELL
# first created by "chris"
# successfully tested "Date 17.01.2017" by "chris"
#
# Class to use switch with FuelGauges
#
##################################################

from helpers.bq34z100g1 import bq34z100g1
from helpers.i2c_switch import I2C_Switch


class FuelGauge(bq34z100g1):
    """Class for reading sensor information about batteries"""

    def __init__(self, address: int, switch: I2C_Switch, nbr: int):
        """initialise the FuelGauge
        :param address:
        :param switch:
        :param nbr:
        """
        self._address = address
        self._switch = switch
        self._nbr = nbr
        super().__init__(self._address)

    def _readValue(self, register, length=2):
        try:
            self._switch.openCH(self._nbr)
            return super()._readValue(register, length)
        except Exception as e:
            bq34z100g1.log("Couldn't read i2c bus")









