
import RPi.GPIO as GPIO
from state_manager.Logger import Logger, LogLevel


class ShiftRegister:

    HIGH = 1
    LOW = 0

    def __init__(self, num_of_pins, pin_data, pin_clk, pin_stb, pin_clr):
        """
        Pins the shift register is connected to must be specified
        :param num_of_pins: number of used pins
        :param pin_data:
        :param pin_clk:
        :param pin_stb:
        :param pin_clr:
        """
        self.num_of_pins = num_of_pins
        self.pin_data = pin_data
        self.pin_clk = pin_clk
        self.pin_stb = pin_stb
        self.pin_clr = pin_clr

        self._register = []

        for i in range(self.num_of_pins):
            self._register.append(0)

        self.setup()

        self.clear_all()

    def setup(self): #-> object:
        """
        Sets up the GPIO pins
        :return:
        """

        GPIO.setmode(GPIO.BCM)
        
        GPIO.setup(self.pin_data, GPIO.OUT)
        GPIO.setup(self.pin_clk, GPIO.OUT)
        GPIO.setup(self.pin_stb, GPIO.OUT)
        GPIO.setup(self.pin_clr, GPIO.OUT)

        GPIO.output(self.pin_data, GPIO.LOW)
        GPIO.output(self.pin_clk, GPIO.LOW)
        GPIO.output(self.pin_stb, GPIO.LOW)
        GPIO.output(self.pin_clr, GPIO.HIGH)

    def shift_data(self):
        """
        Shifts the data of the shift register into the latches
        :return:
        """

        # Logger.log(Logger.VERBOSE, "Shifting to latches")

        for pin in range(self.num_of_pins - 1, -1, -1):
            GPIO.output(self.pin_clk, GPIO.LOW)

            pin_mode = self._register[pin]
            GPIO.output(self.pin_data, pin_mode)

            GPIO.output(self.pin_clk, GPIO.HIGH)

        GPIO.output(self.pin_stb, GPIO.LOW)
        1+1
        GPIO.output(self.pin_stb, GPIO.HIGH)

    def output(self, pin, state):
        """
        Shifts the data into the register and sets a pin on/off. Starting with 0
        :param pin: pin to be set on/off
        :param state: State of the pin (on = 1, off = 0)
        :return:
        """
        if pin >= self.num_of_pins:
            Logger.get_instance().log(LogLevel.WARNING, "SR", "Shiftregister cannot use Pin %s of %s pins" % (pin, self.num_of_pins))
        elif state != self.HIGH and state != self.LOW:
            Logger.get_instance().log(LogLevel.WARNING, "SR", "Shiftregister could not shift data. Invalid state [%s]" % state)
        else:
            self._register[pin] = state
            self.shift_data()

    def set_pins(self, pins, state):
        """
        Shifts the data into the register and sets a number of pins on/offSRsssdfasdfasdfasdf
        :param pins: List of pins to be set on/off
        :param state: state of the pins (on = 1, off = 0)
        :return:
        """
        for pin in pins:
            self.output(pin, state)

        self.shift_data()

    def clear_all(self):
        """
        Sets all pins off.
        :return:
        """

        try:
            self._register = [0] * self.num_of_pins

            GPIO.output(self.pin_clr, GPIO.LOW)
            GPIO.output(self.pin_clr, GPIO.HIGH)
            # Logger.log(Logger.LOG, "----All Magnets off----")

        except Exception as e:
            Logger.get_instance().log(LogLevel.ERROR, "StateManager", "Exception occured: " + str(e))




# Test

# if __name__ == '__main__':
