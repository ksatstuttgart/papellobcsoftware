import os
import pickle
import config.configurable_variables as cv


class PSUConfig(object):

    def __init__(self):
        self.path = os.getcwd() + "/psu/config/"
        self.dir = os.listdir(self.path)


    def write_files(self):
        with open(self.path+"BATTERIES", 'wb') as fp:
            pickle.dump(cv.BAT_STATS, fp)

    def new_config(self): # ToDo: auch neu, wenn kaputt
        file = open(self.path + "/revision.config", "r")
        revision = int(file.read())
        if revision < int(cv.PSU_CONSTANTS_REVISION):
            file = open(self.path+"/revision.config", "w")
            file.write(str(cv.PSU_CONSTANTS_REVISION))
            file.close()
            self.write_files()
            print("wrote")


class PSUConstants(object):
    def __init__(self):
        self.PSC = PSUConfig()
        try:
            with open(self.PSC.path+"BATTERIES", 'rb') as fp:
                self.BAT_STATS = pickle.load(fp)
        except Exception as e:
            self.PSC.new_config()
            print("load failed, %s" %str(e))

    def read_config(self):
        pass