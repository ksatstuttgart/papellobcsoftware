#!/usr/bin/env python3
# -*- coding: utf-8 -*-
##################################################
# PAPELL
# first created by "chris"
# successfully tested "Date 17.01.2017" by "chris"
#
# Class to control powerlines
#
##################################################
try:
    import RPi.GPIO as GPIO
    from state_manager.Logger import LogLevel, Logger
except Exception as e:
    Logger.get_instance().log(LogLevel.EXCEPTION, str("PSUCIRCUIT"), "Couldn't import RPi due to: %s" % str(e))


class Powerline:

    def __init__(self, pin: int, default: bool, name: str = "Powerline0"):
        """ initialise a Circuit with pin, defaultstate and name
        :param pin:
        :param default:
        :param name:
        """
        self._pin = pin
        self._default = default
        self._name = name
        GPIO.setmode(GPIO.BCM)

        if name == "VEXT_EN":
            GPIO.setup(self._pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        else:
            GPIO.setup(self._pin, GPIO.OUT)
            GPIO.output(self._pin, default)  # Circuit is disconnected by default

    def __str__(self):
        return self._name

    def connect(self):
        """Switches Circuit on through enabling Pin"""
        if self._name == "VEXT_EN":
            GPIO.setup(self._pin, GPIO.OUT)
            GPIO.output(self._pin, GPIO.LOW)
        else:
            GPIO.output(self._pin, GPIO.HIGH)
        return 1
 
    def disconnect(self):
        """Switches Circuit off through enabling Pin"""
        if self._name == "VEXT_EN":
            GPIO.setup(self._pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            Logger.get_instance().log(LogLevel.INFO, str("PSUCIRCUIT"), "V_EXT disconnected")
        else:
            GPIO.output(self._pin, GPIO.LOW)
            Logger.get_instance().log(LogLevel.INFO, str("PSUCIRCUIT"), "Powerline %s disconnected" % self._name)
        return 1
