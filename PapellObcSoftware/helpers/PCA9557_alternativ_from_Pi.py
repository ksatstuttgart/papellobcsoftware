#!/usr/bin/python


'''##################################################
# PAPELL
# first created by Tobias Ott
# modified by --
# successfully tested
#
# This class implements a fully functional
# PCA9557 I2C Port Expander.
#
##################################################'''

from smbus import SMBus


class PCA9557:

    # Define states and modes
    OUTPUT = 3
    INPUT = 2
    HIGH = 1
    LOW = 0

    def __init__(self, address, busid=1):
        """
        Creates a new PortExpander with a specified i2c address
        :param address:
        :param busid:
        """
        self._i2c = SMBus(busid)
        self._i2c_address = address

        self._port_values = 0x00
        self._i2c.write_byte_data(self._i2c_address, 0x03, self._port_values)
        self._i2c.write_byte_data(self._i2c_address, 0x01, 0x00)
        self._i2c.write_byte_data(self._i2c_address, 0x03, 0x00) #~self._port_values)

    def __del__(self):
        """ Clean up routines. """
        try:
            # Remove SMBus connection
            del self._i2c
        except:
            passj

    def __str__(self):
        return str("Expander @ %s" % hex(self._i2c_address))

    def setup(self, pin, mode):
        """
        Setup the pins. Default mode is OUTPUT.
        The input pins must be set as HIGH so the pin is able to detect a change when pulled low.
        Same as GPIO.setup
        :param pin:
        :param mode:
        :return:
        """
        if mode is self.INPUT:
            self.output(pin, self.HIGH)
        else:
            self.output(pin, self.LOW)

    def output(self, pin, state):
        """
        Sets an output pin to LOW/ground or high/Vcc. Same as GPIO.output
        :param pin:
        :param state:
        :return:
        """
        if state is self.LOW:
            self._port_values &= ~(0x01 << pin)  # Clear bit

        else:
            self._port_values |= (0x01 << pin)  # Set bit

        self._i2c.write_byte_data(self._i2c_address, 0x01, self._port_values & 0b11111110)
        #self._i2c.write_byte_data(self._i2c_address, 0x03, 0b00000000)#~self._port_values)

    def input(self, pin):
        """
        Reads the state of the pin. Set HIGH by default.
        Can be pulled to ground to indicate a changed state.
        Same as GPIO.input
        :param pin:
        :return:
        """
        if (self._i2c.read_byte_data(self._i2c_address, 0x00) & (1 << pin)) > 0:
            return self.HIGH
        else:
            return self.LOW

    def toggle(self, pin):
        """
        Inverts the sate of an output pin
        :param pin:
        :return:
        """
        self._port_values ^= (1 << pin)
        self._i2c.write_byte_data(self._i2c_address, 0x01, self._port_values & 0b11111110)
        self._i2c.write_byte_data(self._i2c_address, 0x03, ~self._port_values)
#!/usr/bin/python


'''##################################################
# PAPELL
# first created by Tobias Ott
# modified by --
# successfully tested
#
# This class implements a fully functional
# PCA9557 I2C Port Expander.
#
##################################################'''

from smbus import SMBus


class PCA9557:

    # Define states and modes
    OUTPUT = 3
    INPUT = 2
    HIGH = 1
    LOW = 0

    def __init__(self, address, busid=1):
        """
        Creates a new PortExpander with a specified i2c address
        :param address:
        :param busid:
        """
        self._i2c = SMBus(busid)
        self._i2c_address = address

        self._port_values = 0x00
        self._i2c.write_byte_data(self._i2c_address, 0x03, self._port_values)
        self._i2c.write_byte_data(self._i2c_address, 0x01, 0x00)
        self._i2c.write_byte_data(self._i2c_address, 0x03, 0x00) #~self._port_values)

    def __del__(self):
        """ Clean up routines. """
        try:
            # Remove SMBus connection
            del self._i2c
        except:
            passj

    def __str__(self):
        return str("Expander @ %s" % hex(self._i2c_address))

    def setup(self, pin, mode):
        """
        Setup the pins. Default mode is OUTPUT.
        The input pins must be set as HIGH so the pin is able to detect a change when pulled low.
        Same as GPIO.setup
        :param pin:
        :param mode:
        :return:
        """
        if mode is self.INPUT:
            self.output(pin, self.HIGH)
        else:
            self.output(pin, self.LOW)

    def output(self, pin, state):
        """
        Sets an output pin to LOW/ground or high/Vcc. Same as GPIO.output
        :param pin:
        :param state:
        :return:
        """
        if state is self.LOW:
            self._port_values &= ~(0x01 << pin)  # Clear bit

        else:
            self._port_values |= (0x01 << pin)  # Set bit

        self._i2c.write_byte_data(self._i2c_address, 0x01, self._port_values & 0b11111110)
        #self._i2c.write_byte_data(self._i2c_address, 0x03, 0b00000000)#~self._port_values)

    def input(self, pin):
        """
        Reads the state of the pin. Set HIGH by default.
        Can be pulled to ground to indicate a changed state.
        Same as GPIO.input
        :param pin:
        :return:
        """
        if (self._i2c.read_byte_data(self._i2c_address, 0x00) & (1 << pin)) > 0:
            return self.HIGH
        else:
            return self.LOW

    def toggle(self, pin):
        """
        Inverts the sate of an output pin
        :param pin:
        :return:
        """
        self._port_values ^= (1 << pin)
        self._i2c.write_byte_data(self._i2c_address, 0x01, self._port_values & 0b11111110)
        self._i2c.write_byte_data(self._i2c_address, 0x03, ~self._port_values)
#!/usr/bin/python


'''##################################################
# PAPELL
# first created by Tobias Ott
# modified by --
# successfully tested
#
# This class implements a fully functional
# PCA9557 I2C Port Expander.
#
##################################################'''

from smbus import SMBus


class PCA9557:

    # Define states and modes
    OUTPUT = 3
    INPUT = 2
    HIGH = 1
    LOW = 0

    def __init__(self, address, busid=1):
        """
        Creates a new PortExpander with a specified i2c address
        :param address:
        :param busid:
        """
        self._i2c = SMBus(busid)
        self._i2c_address = address

        self._port_values = 0x00
        self._i2c.write_byte_data(self._i2c_address, 0x03, self._port_values)
        self._i2c.write_byte_data(self._i2c_address, 0x01, 0x00)
        self._i2c.write_byte_data(self._i2c_address, 0x03, 0x00) #~self._port_values)

    def __del__(self):
        """ Clean up routines. """
        try:
            # Remove SMBus connection
            del self._i2c
        except:
            passj

    def __str__(self):
        return str("Expander @ %s" % hex(self._i2c_address))

    def setup(self, pin, mode):
        """
        Setup the pins. Default mode is OUTPUT.
        The input pins must be set as HIGH so the pin is able to detect a change when pulled low.
        Same as GPIO.setup
        :param pin:
        :param mode:
        :return:
        """
        if mode is self.INPUT:
            self.output(pin, self.HIGH)
        else:
            self.output(pin, self.LOW)

    def output(self, pin, state):
        """
        Sets an output pin to LOW/ground or high/Vcc. Same as GPIO.output
        :param pin:
        :param state:
        :return:
        """
        if state is self.LOW:
            self._port_values &= ~(0x01 << pin)  # Clear bit

        else:
            self._port_values |= (0x01 << pin)  # Set bit

        self._i2c.write_byte_data(self._i2c_address, 0x01, self._port_values & 0b11111110)
        #self._i2c.write_byte_data(self._i2c_address, 0x03, 0b00000000)#~self._port_values)

    def input(self, pin):
        """
        Reads the state of the pin. Set HIGH by default.
        Can be pulled to ground to indicate a changed state.
        Same as GPIO.input
        :param pin:
        :return:
        """
        if (self._i2c.read_byte_data(self._i2c_address, 0x00) & (1 << pin)) > 0:
            return self.HIGH
        else:
            return self.LOW

    def toggle(self, pin):
        """
        Inverts the sate of an output pin
        :param pin:
        :return:
        """
        self._port_values ^= (1 << pin)
        self._i2c.write_byte_data(self._i2c_address, 0x01, self._port_values & 0b11111110)
        self._i2c.write_byte_data(self._i2c_address, 0x03, ~self._port_values)
#!/usr/bin/python


'''##################################################
# PAPELL
# first created by Tobias Ott
# modified by --
# successfully tested
#
# This class implements a fully functional
# PCA9557 I2C Port Expander.
#
##################################################'''

from smbus import SMBus


class PCA9557:

    # Define states and modes
    OUTPUT = 3
    INPUT = 2
    HIGH = 1
    LOW = 0

    def __init__(self, address, busid=1):
        """
        Creates a new PortExpander with a specified i2c address
        :param address:
        :param busid:
        """
        self._i2c = SMBus(busid)
        self._i2c_address = address

        self._port_values = 0x00
        self._i2c.write_byte_data(self._i2c_address, 0x03, self._port_values)
        self._i2c.write_byte_data(self._i2c_address, 0x01, 0x00)
        self._i2c.write_byte_data(self._i2c_address, 0x03, 0x00) #~self._port_values)

    def __del__(self):
        """ Clean up routines. """
        try:
            # Remove SMBus connection
            del self._i2c
        except:
            passj

    def __str__(self):
        return str("Expander @ %s" % hex(self._i2c_address))

    def setup(self, pin, mode):
        """
        Setup the pins. Default mode is OUTPUT.
        The input pins must be set as HIGH so the pin is able to detect a change when pulled low.
        Same as GPIO.setup
        :param pin:
        :param mode:
        :return:
        """
        if mode is self.INPUT:
            self.output(pin, self.HIGH)
        else:
            self.output(pin, self.LOW)

    def output(self, pin, state):
        """
        Sets an output pin to LOW/ground or high/Vcc. Same as GPIO.output
        :param pin:
        :param state:
        :return:
        """
        if state is self.LOW:
            self._port_values &= ~(0x01 << pin)  # Clear bit

        else:
            self._port_values |= (0x01 << pin)  # Set bit

        self._i2c.write_byte_data(self._i2c_address, 0x01, self._port_values & 0b11111110)
        #self._i2c.write_byte_data(self._i2c_address, 0x03, 0b00000000)#~self._port_values)

    def input(self, pin):
        """
        Reads the state of the pin. Set HIGH by default.
        Can be pulled to ground to indicate a changed state.
        Same as GPIO.input
        :param pin:
        :return:
        """
        if (self._i2c.read_byte_data(self._i2c_address, 0x00) & (1 << pin)) > 0:
            return self.HIGH
        else:
            return self.LOW

    def toggle(self, pin):
        """
        Inverts the sate of an output pin
        :param pin:
        :return:
        """
        self._port_values ^= (1 << pin)
        self._i2c.write_byte_data(self._i2c_address, 0x01, self._port_values & 0b11111110)
        self._i2c.write_byte_data(self._i2c_address, 0x03, ~self._port_values)

