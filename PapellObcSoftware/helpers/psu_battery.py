#!/usr/bin/env python3
# -*- coding: utf-8 -*-
##################################################
# PAPELL
# first created by "chris"
# successfully tested "Date 17.01.2017" by "chris"
#
# Class to wrap battery with battery connect, disconnect pin, and checkfunctions
#
##################################################


try:
    import RPi.GPIO as GPIO
except Exception as e:
    print("Couldn't import RPi\n%s" % str(e))

from helpers.psu_fuelgauge import FuelGauge  # for Babysitter Board replace fuelgauge with fuelgauge2
from helpers.i2c_switch import I2C_Switch
from state_manager.Logger import Logger, LogLevel
from config import configurable_variables as cv


class Battery(FuelGauge):
    """Class to control the Batteries"""
    
    def __init__(self, switch: I2C_Switch, nbr: int, pin: int, name: str="BAT"):
        """ initialise a Battery with FuelGaugeID (nbr) and Disconnect Pin
        :param switch:
        :param nbr:
        :param pin:
        :param name:
        """

        self._pin = pin
        self._address = 0x55
        self._name = name
        self._working = True  # if set to false, battery will not be connectible
        self._dead = False  # if set to true, battery will not be used anymore
        self._connected = False
        self._set_intern()

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self._pin, GPIO.OUT)

        GPIO.output(self._pin, GPIO.LOW)  #Battery is disconnected by default

        Logger.get_instance().log(LogLevel.INFO, self._name, " Battery initialised")

        super().__init__(self._address, switch, nbr)

    def __str__(self):
        """
        returns name of the battery
        :rtype: str
        """
        return self._name

    def _set_intern(self):
        try:
            data = psc.BAT_STATS[self._name]
        except:
            data = cv.BAT

        self._working = data[2]
        self._dead = data[3]
        self.temp_threshold = data[0]
        self.temp_critical = data[1]


    def disconnect(self):
        """Disconnects Battery"""
        try:
            GPIO.output(self._pin, GPIO.LOW)
            self._connected= False
            Logger.get_instance().log(LogLevel.INFO, str("PSU" + self._name), " Battery disconnected")
        except Exception as e:
            return "disconnect failed %s" % e

    def connect(self, force=False):
        """Connects Battery"""
        if self._working or force:
            try:
                GPIO.output(self._pin, GPIO.HIGH)
                self._connected = True
                Logger.get_instance().log(LogLevel.INFO, str("PSU" + self._name), " Battery connected" )
            except Exception as e:
                Logger.get_instance().log(LogLevel.ERROR, str("PSU" + self._name), " Connect failed: %s" % str(e) )
        else:
            Logger.get_instance().log(LogLevel.ERROR, self._name, " Battery broken, can't connect")

    def broken(self):
        self._working = False

    def working(self):
        return self._working

    def dead(self):
        return self._dead

    def kill_battery(self):
        self._dead = True

    def repair(self):
        self._working = True

    def info(self):
        """returns soc, voltage, current, power, capacity, full capacity, temp and int_temp"""
        return [self.get_state_of_charge(), self.get_voltage(), self.get_current(), self.get_power(), self.get_capacity(), self.get_full_capacity(), self.get_temperature(), self.get_internal_temperature()]

    def connect_status(self):
        """returns connect status"""
        return  self._connected
