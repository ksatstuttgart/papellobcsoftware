import sys
import time

class Test:

    def __init__(self, argv):
        import psu.PSUTest as PSU
        time.sleep(0.5)
        input("initializing psu>")
        self.psu = PSU.PSUTest()
        self.argv = argv
        if "all" in argv:
            argv.append("ea1")
            argv.append("ea2")
            argv.append("actuator")
            argv.append("sensors")
            argv.append("light")
        if "ea1" in argv:
            input("initialising ea1>")
            self.ea1 = self.init_ea1()
        if "ea2" in argv:
            input("initialising ea2>")
            self.ea2 = self.init_ea2()
        if "light" in argv:
            from camera.lightning import Lightning
            import RPi.GPIO as GPIO
            GPIO.setup(21, GPIO.OUT)
            self.light = Lightning(20)
            GPIO.output(21, GPIO.HIGH)

        input("going to do alex routine>")
        self.start_charge()
        time.sleep(1)
        self.psu.system_info()
        if "actuator" in argv:
            import helpers.PCA9557 as PCA9557
            input("initialising expander for actuator>")
            self.expander1 = PCA9557.PCA9557(address=0x18)
            self.expander2 = PCA9557.PCA9557(address=0x19)
        if "sensors" in argv:
            pass

    def init_ea1(self):
        """makes all pin to low for saving battery"""
        import helpers.ShiftRegister as SR
        ea1 = SR.ShiftRegister(40, 8, 23, 25, 24)
        for pin in range(40):
            ea1.output(pin, ea1.LOW)
        return ea1

    def init_ea2(self):
        """makes all pin to low for saving battery"""
        import helpers.ShiftRegister as SR
        ea2 = SR.ShiftRegister(27, 7, 20, 12, 16)
        for pin in range(40):
            ea2.output(pin, ea2.LOW)
        return ea2

    def init_aktuator(self):
        pass

    def start_charge(self):
        input("connect batteries>")
        self.psu.connect_batteries()
        time.sleep(1)
        input("disconnect usb power>")
        self.psu.disconnect_usb_power()
        time.sleep(1)
        input("connect V_EXT>")
        self.psu.connect_vext()
        time.sleep(1)


if __name__ == "__main__":
    #from helpers.Database import QueuedDatabase
    #queue = QueuedDatabase("examplen")
    #queue.start()
    #queue.create_psu_database()
    #queue.stop()
    test = Test("light")

