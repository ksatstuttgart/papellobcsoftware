##################################################
# PAPELL
# first created by Robin Schweigert
# modified by -- Martin Siedorf
# successfully tested -- Martin Siedorf
#
# The Main file is the entrance point of our software.
# It starts with an update of the whole software,
# excluding the Main file itself for security
# purposes, This file must be secured completely so
# nothing can go wrong, no matter what happens
# afterwards. After a finished update, the
# state manager is started.
#
##################################################

# for testing only
import sys, traceback

try:
    import os
    import time
except Exception:
    print("Import of os or time is working")


"""
always keep the state machine running, no matter what happens
"""

swapp_folder = "/home/pi/mntData/"
update_folder = swapp_folder + "updatefolder/"
updateversion_new = update_folder + "config/obcVersion.txt"
updateversion_old = "config/obcVersion.txt"


if __name__ == '__main__':

    try:  # change share folder for ISS
        os.system("sudo swapData")
        # pass
    except Exception:
        try:
            print("No piData_1 working.")
        except Exception:
            pass

    try:
        from state_manager.Logger import Logger, LogLevel
    except Exception:
        print("no logger Import working")

    try:
        os.chdir("/home/pi/papellobcsoftware/PapellObcSoftware/")
    except Exception:
        try:
            Logger.get_instance().log(LogLevel.INFO, "VersionControl", "changing root directory failed")
        except Exception:
            try:
                print("changing root directory failed")
            except Exception:
                pass

    try:
        Logger.get_instance().log(LogLevel.INFO, "VersionControl", "Looking for a version update.")
    except Exception:
        try:
            print("Logger failure, looking for a version update.")
        except Exception:
            pass

    try:
        f_old = open(updateversion_old)
        l_old = f_old.readline()
    except Exception:
        l_old = -1
        try:
            Logger.get_instance().log(LogLevel.WARNING, "VersionControl", "Old OBCVersion file is not existing."
                                                                          "Something strange happened during last"
                                                                          "experiment. Update will be done.")
        except Exception:
            try:
                print("VersionControl, Old OBCVersion file is not existing."
                      "Something strange happened during last"
                      "experiment. Update will be done.")
            except Exception:
                pass

    try:
        if os.path.isdir(update_folder):  # check for file
            f_new = open(updateversion_new)
            l_new = f_new.readline()

            try:
                obc_old = int(l_old)
            except Exception:
                obc_old = 0
                try:
                    Logger.get_instance().log(LogLevel.INFO, "VersionControl",
                                              "The old OBC version is no number. Crazy stuff did happen before."
                                              "Update will be initiated.")
                except Exception:
                    try:
                        print("Logger failure, the old OBC version is no number. Crazy stuff did happen before."
                              "Update will be initiated.")
                    except Exception:
                        pass
            try:  # check if version is a number
                obc_new = int(l_new)
                if obc_new > obc_old:
                    try:
                        Logger.get_instance().log(LogLevel.INFO, "VersionControl",
                                                  "The new OBC version %s is available." % str(obc_new))
                    except Exception:
                        try:
                            print("Logger failure, the new OBC version %s is available." % str(obc_new))
                        except Exception:
                            pass

                    for file in os.listdir(update_folder):
                        if file.startswith("Main.py"):  # delete update Main.py
                            os.system("sudo rm -f " + update_folder + "Main.py")
                            try:
                                Logger.get_instance().log(LogLevel.INFO, "VersionControl",
                                                "some idiot working again? There is no update for Main.py possible")
                            except Exception:
                                try:
                                    print("some idiot working again? There is no update for Main.py possible")
                                except Exception:
                                    pass
                    try:  # copy all new files
                        os.system("cp -r " + update_folder + "* .")
                    except Exception:
                        try:
                            Logger.get_instance().log(LogLevel.INFO, "VersionControl",
                                                      "The new OBC has files with no rights to read")
                        except Exception:
                            try:
                                print("Logger failure, The new OBC has files with no rights to read")
                            except Exception:
                                pass
                    try:  # status update for Logger
                        Logger.get_instance().log(LogLevel.INFO, "VersionControl",
                                                  "The new OBC version %s is now active." % str(obc_new))
                    except Exception:
                        try:
                            print("Logger failure, the new OBC version %s is now active." % str(obc_new))
                        except Exception:
                            pass
                else:
                    try:
                        Logger.get_instance().log(LogLevel.INFO, "VersionControl", "Already using the latest OBC Version.")
                    except Exception:
                        try:
                            print("Logger failure, already using the latest OBC Version.")
                        except Exception:
                            pass
            # --------------------------------
            except Exception:
                try:
                    Logger.get_instance().log(LogLevel.INFO, "VersionControl",
                                              "The new OBC version is no number. Please make a better Update!")
                except Exception:
                    try:
                        print("The new OBC version is no number. Please make a better Update!")
                    except Exception:
                        pass

            f_new.close()
            if l_old == -1:  # in case there is no old obcVersion file
                pass
            else:
                f_old.close()
        else:
            try:
                Logger.get_instance().log(LogLevel.INFO, "VersionControl", "Update folder is missing.")
            except Exception:
                try:
                    print("VersionControl, update folder is missing.")
                except Exception:
                    pass
    except Exception:  # generell read permission denied
        try:
            Logger.get_instance().log(LogLevel.INFO, "VersionControl", "Update files are missing read permission.")
        except Exception:
            try:
                print("Logger failure, Update files are missing read permission.")
            except Exception:
                pass

    try:
        Logger.get_instance().log(LogLevel.INFO, "Main.py", "Going into Safety Sleep for 10 seconds now.")
        time.sleep(10)#1800
    except Exception:
        try:
            print("Logger failure, Going into Safety Sleep for 30 minuites now.")
        except Exception:
            pass

    try:
        from state_manager.StateManager import StateManager
        while True:
            try:
                StateManager()
                time.sleep(60)
                break
            except Exception as e:
                print(str(e))
    except Exception as ee:
        try:
            Logger.get_instance().log(LogLevel.INFO, "Main.py", "The import of Statemanager did not work")
            print(traceback.print_exc(file=sys.stdout))
        except Exception as ee:
            try:
                print(ee.with_traceback())
                print("The import of Statemanager did not work")
                print("end")
            except Exception:
                pass
