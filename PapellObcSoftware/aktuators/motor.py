##################################################
# PAPELL
# first created by Martin Siedorf
# modified by --
# successfully tested --
#
# The motor.py starts and uses the motor helper to work
#
#
##################################################

import time
from helpers.PCA9557 import PCA9557
from helpers.DRV8834 import DRV8834
from state_manager.initialcheck import i2c_action


class motor():

    def __init__(self):
        self.exp1 = PCA9557(address=i2c_action.Addr_exp1)  # 0x18)
        self.exp2 = PCA9557(address=i2c_action.Addr_exp2)  # 0x19)
        self.mot1 = DRV8834(self.exp1)
        self.mot2 = DRV8834(self.exp2)

    def start(self,t):
        """
        running motor for time t in sec
        :param t: seconds
        :return:
        """
        self.mot1.set_dir(DRV8834.DIR_CCW)
        self.mot2.set_dir(DRV8834.DIR_CCW)
        for i in range(0, t*5):  # ToDo: time is do be determined exactly
            self.mot1.step(1)
            self.mot2.step(1)
            time.sleep(0.5)

    """ 
    old from tests_without_statemanager.py
     for j in range(0, 2):
         print("first direction")
         print("other direction")
         mot1.set_dir(DRV8834.DIR_CW)
         mot2.set_dir(DRV8834.DIR_CW)
         for i in range(0, 20):
             mot1.step(1)
             mot2.step(1)
             time.sleep(0.5)
     temp.disconnect_12v_powerline()
     temp.disconnect_vext()
     temp.disconnect_batteries()
     print("robin motor test finished")
    """