##################################################
# PAPELL
# first created by Robin Schweigert
# modified by Chris
# successfully tested --
#
# This is the assembly for all actuators.
#
##################################################

from helpers.PCF8574 import PCF8574
from aktuators.pump import Pump


class Actuators:

    def __init__(self):
        expander1 = PCF8574(0x38)
        expander2 = PCF8574(0x39)
        self._pumps = [Pump(expander1, 0), Pump(expander2, 0)]
        self._valves = []
