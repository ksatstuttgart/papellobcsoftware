"""##################################################
# PAPELL
# first created by "Jan-Erik Brune"
# modified by "name"
# successfully tested "Date 21.01.2018" by "Jan-Erik Brune"
#
# class definition of valves (open, close, open_for)
#
##################################################"""

import time
from state_manager.Logger import Logger, LogLevel


class Valve:

    def __init__(self, expander, pin):
        self._pin = pin
        self._expander = expander
        self.close()
        Logger.get_instance().log(LogLevel.VERBOSE, "Valve", "(%s . %s) initialized" % (self._expander, self._pin))

    def open(self):
        """
        Opens the Valve
        :return:
        """
        self._expander.output(self._pin, self._expander.HIGH)
        Logger.get_instance().log(LogLevel.VERBOSE, "Valve", " (%s . %s): open "
                                  % (self._expander, self._pin))

    def close(self):
        """
        Closes the Valve
        :return:
        """
        self._expander.output(self._pin, self._expander.LOW)
        Logger.get_instance().log(LogLevel.VERBOSE, "Valve", " (%s . %s): closed "
                                  % (self._expander, self._pin))

    def open_for(self, t):
        try:
            self._expander.output(self._pin, 1)
            Logger.get_instance().log(LogLevel.VERBOSE, "Valve", "Expander %s, Pin %s: open for %s seconds"
                                      % (self._expander, self._pin, t))
            time.sleep(t)
            self._expander.output(self._pin, 0)
            Logger.get_instance().log(LogLevel.VERBOSE, "Valve",
                                      "Expander %s, Pin %s: closed after opened for %s seconds"
                                      % (self._expander, self._pin, t))
        except:
            Logger.get_instance().log(LogLevel.WARNING, "Valve",
                                      "Expander %s, Pin %s: unable to run for %s seconds"
                                      % (self._expander, self._pin, t))

    def __str__(self):
        return str(" Valve (%s . %s)" % (self._expander, self._pin))