"""##################################################
# PAPELL
# first created by Jan-Erik Brune
# modified by chris
# successfully tested "Date 21.01.2018 by "Jan-Erik Brune"
#
# definition of class Pump (start,stop,run_for)
#
##################################################
"""

import time
from state_manager.Logger import Logger, LogLevel


class Pump:

    def __init__(self, expander, pin: int):
        self._pin = pin
        self._expander = expander
        self.stop()
        Logger.get_instance().log(LogLevel.VERBOSE, "Pump", "(%s . %s) initialized" % (self._expander, self._pin))

    def start(self):
        """
        Starts the pump until it is stopped
        :return:
        """
        self._expander.output(self._pin, 1)
        Logger.get_instance().log(LogLevel.VERBOSE, "Pump", " (%s . %s): started"
                                  % (self._expander, self._pin))

    def stop(self):
        """
        Stops the pump and does not care if it is running
        :return:
        """
        self._expander.output(self._pin, 0)
        Logger.get_instance().log(LogLevel.VERBOSE, "Pump",
                                  " (%s . %s): stopped" % (self._expander, self._pin))

    def run_for(self, t):
        """
        Activates the Pump for a specific time period
        :param t: Time in seconds
        :return:
        """
        if type(t) != int and type(t) != float:
            Logger.get_instance().log(LogLevel.WARNING, "Pump", "Pump on Pin %s at %s cannot run for %s seconds (wrong format)"
                                      % (self._pin, self._expander, t))            
        elif t > 0:
            self.start()
            Logger.get_instance().log(LogLevel.VERBOSE, "Pump", "Pump on Expander %s, Pin %s is running for %s seconds"
                                      % (self._pin, self._expander, t))
            time.sleep(t)
            self.stop()
        else:
            Logger.get_instance().log(LogLevel.WARNING, "Pump", "Pump on Pin %s at %s cannot run for %s seconds (wrong format)"
                                      % (self._pin, self._expander, t))

    def __str__(self):
        return str(" Pump (%s . %s)" % (self._expander, self._pin))
