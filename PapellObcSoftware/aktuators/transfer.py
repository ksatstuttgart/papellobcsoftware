##################################################
# PAPELL
# first created by "Jan-Erik Brune"
# modified by "Jan-Erik Brune"
# successfully tested "21.01.2018" by "Jan-Erik Brune"
# modified by Martin
#
# transfer management of ferrofluid
#
##################################################

from aktuators.pump import Pump
from helpers.PCA9557 import PCA9557
from aktuators.valve import Valve
from state_manager.Logger import LogLevel, Logger
from state_manager.initialcheck import i2c_action


class PumpingSystem:

    EA_1 = 0
    EA_2 = 1

    PUMP_1 = 0
    PUMP_2 = 1

    VALVE_1 = 0
    VALVE_2 = 1
    VALVE_3 = 2
    VALVE_4 = 3

    def __init__(self, state_manager):
        """
        Initializes pumping system
        """
        # Get State Manager
        self.__state_manager = state_manager

        # Pins/Addresses
        self.__expander_address = [i2c_action.Addr_exp1, i2c_action.Addr_exp2]
        self.__valve_pin = [i2c_action.Pin_exp1_valve1, i2c_action.Pin_exp1_valve2,
                            i2c_action.Pin_exp2_valve3, i2c_action.Pin_exp2_valve4]
        self.__pump_pin = [i2c_action.Pin_exp1_pump, i2c_action.Pin_exp2_pump]

        # Initializing expanders
        self.expander = []
        for addr in range(2):
            for attempt in range(3):
                try:
                    self.expander.append(PCA9557(address=self.__expander_address[addr]))
                    break
                except:
                    self.expander.append(None)
                    Logger.get_instance().log(LogLevel.WARNING, "Transfer", "Expander on Address %s is not available! "
                                              "%s. Attempt" % (hex(self.__expander_address[addr]), attempt+1))

        # Initializing valves
        self.__valve_expander = [self.expander[0], self.expander[0], self.expander[1], self.expander[1]]

        self.valves = []
        for pin in range(4):
            for attempt in range(3):
                try:
                    self.valves.append(Valve(self.__valve_expander[pin], self.__valve_pin[pin]))
                    break
                except:
                    self.valves.append(None)
                    Logger.get_instance().log(LogLevel.WARNING, "Transfer", "Valve %s is not available! "
                                                                            "%s. Attempt" % (str(pin+1), attempt+1))

        # Initializing pumps
        self.pumps = []
        for pin in range(2):
            for attempt in range(3):
                try:
                    self.pumps.append(Pump(self.expander[pin], self.__pump_pin[pin]))
                    break
                except:
                    self.pumps.append(None)
                    Logger.get_instance().log(LogLevel.WARNING, "Transfer", "Pump %s is not available! "
                                                                            "%s. Attempt" % (str(pin + 1), attempt+1))

        self.__valve_modes = {self.EA_1: {self.PUMP_1: self.VALVE_4, self.PUMP_2: self.VALVE_3},
                              self.EA_2: {self.PUMP_1: self.VALVE_1, self.PUMP_2: self.VALVE_2}}

        self.stop()

    '''    
           T1              T2
            |               |
            |               |
           P1              P2
           |               |
           |------------   |
           |    -------x---|
           |    |      |   |
           V4   V3     V1  V2
           |    |      |   |
            EXP1        EXP2
    '''

    def stop(self):
        """
        closes and stops all valves and pumps
        :return:
        """
        try:

            for valve in self.valves:
                for attempt in range(3):
                    try:
                        valve.close()
                        break
                    except:
                        Logger.get_instance().log(LogLevel.WARNING, "Transfer",
                                                  "Could not stop Valve %s! %s. Attempt" % (valve, attempt+1))
                        if attempt == 2:
                            raise Exception("Valve %s not responding." % valve)

            for pump in self.pumps:
                for attempt in range(3):
                    try:
                        pump.stop()
                        break
                    except:
                        Logger.get_instance().log(LogLevel.WARNING, "Transfer",
                                                  "Could not stop Pump %s! %s. Attempt" % (pump, attempt+1))
                        if attempt == 2:
                            raise Exception("Pump %s not responding." % pump)

            Logger.get_instance().log(LogLevel.INFO, "Transfer", "All valves closed and all Pumps stopped")
            return 1

        except Exception as e:
            Logger.get_instance().log(LogLevel.ERROR, "Transfer", "Could not close valve or stop pump! "
                                                                  "Switching to Safe Mode! Error: %s" % e)
            self.__state_manager.safe_state_sig.value = True

    def start(self, ea, pump):
        """
        Opens a valve and starts pumping
        :return:
        """
        my_valve = self.valves[self.__valve_modes[ea][pump]]
        my_pump = self.pumps[pump]
        #if (my_valve is None) or (my_pump is None):
        #    Logger.get_instance().log(LogLevel.WARNING, "Transfer", "Path: EA " + str(ea) + ", Pump " + str(pump) +
        #                              " is not working. Trying to use Pump " + str(1-pump))
        #    my_valve = self.valves[self.__valve_modes[ea][1-pump]]
        #    my_pump = self.pumps[1-pump]
        try:
            for attempt in range(3):
                try:
                    my_valve.open()
                    break
                except:
                    Logger.get_instance().log(LogLevel.WARNING, "Transfer",
                                              "Could not start Valve %s! %s. Attempt" % (my_valve, attempt + 1))
                    if attempt == 2:
                        raise Exception("Valve %s not responding." % my_valve)
            for attempt in range(3):
                try:
                    my_pump.start()
                    break
                except:
                    Logger.get_instance().log(LogLevel.WARNING, "Transfer",
                                              "Could not start Pump %s! %s. Attempt" % (my_pump, attempt + 1))
                    if attempt == 2:
                        raise Exception("Pump %s not responding." % my_pump)
            return 1
        except Exception as e:
            Logger.get_instance().log(LogLevel.ERROR, "Transfer", "Could not Start pumping!"
                                                                  "Switching to Safe Mode! Error: %s" % e)
            self.__state_manager.safe_state_sig.value = True
            return 0


class ValvePump:

    def __init__(self):

        '''
        Pin 0,1,2 on Expander 1,2
        change Pins if wrong Valves
        :param expander1: Valve 1, Valve 2, Pump 1
        :param expander2: Valve 3, Vlave 4, Pump 2
        '''
        i2c = i2c_action()
        i2c.detect()

        if True:  # i2c_action.gpio_expander1 is True and i2c_action.gpio_expander1 is True:
            self.expander1 = PCA9557(address=i2c_action.Addr_exp1)  # defined i2c address
            self.expander2 = PCA9557(address=i2c_action.Addr_exp2)  # defined i2c address

            self.valve1 = Valve(self.expander1, i2c_action.Pin_exp1_valve1)
            self.valve2 = Valve(self.expander1, i2c_action.Pin_exp1_valve2)
            self.pump1 = Pump(self.expander1, i2c_action.Pin_exp1_pump)

            self.valve3 = Valve(self.expander2, i2c_action.Pin_exp2_valve3)
            self.valve4 = Valve(self.expander2, i2c_action.Pin_exp2_valve4)
            self.pump2 = Pump(self.expander2, i2c_action.Pin_exp2_pump)
            self.t = 5

        else:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Transfer", "GPIO Expander are not available")
            pass

    '''    
           T1              T2
            |               |
            |               |
           P1              P2
           |               |
           |------------   |
           |    -------x---|
           |    |      |   |
           V4   V3     V1  V2
           |    |      |   |
            EXP1        EXP2
    '''


    def close_stop(self):
        """
        closes and stops all valves and pumps
        :return:
        """
        try:
            Logger.get_instance().log(LogLevel.INFO, "Transfer", "All valves closed and all Pumps closing")

            self.valve1.close()
            self.valve2.close()
            self.valve3.close()
            self.valve4.close()
            self.pump1.stop()
            self.pump2.stop()
            Logger.get_instance().log(LogLevel.INFO, "Transfer", "All valves closed and all Pumps closing")

        except Exception as e:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Transfer", "close_stop() ended: %s" %e)

    def normal1(self, t):
        """
        Case Normal 1 for EA1 (T1,P1,V1)
        :param t: time, the pump should run and the valve should be opened for transfer
        :return:
        """
        try:
            Logger.get_instance().log(LogLevel.INFO, "Transfer", "Normal1 began")
            self.valve1.close()
            self.valve2.close()
            self.valve3.close()
            self.valve4.open()
            print("point 1")
            self.pump1.run_for(t)
            print("point2")
            self.pump2.stop()
            self.close_stop()
            Logger.get_instance().log(LogLevel.INFO, "Transfer", "Normal1 ended")

        except Exception as e:
            import traceback
            Logger.get_instance().log(LogLevel.EXCEPTION, "Transfer", "Normal1234 ended: %s" % traceback.format_exc())

    def normal2(self, t):
        """
        Case Normal 2 for EA2 (T2,P2,V2)
        :param t: time, the pump should run and the valve should be opened for transfer
        :return:
        """
        try:
            Logger.get_instance().log(LogLevel.INFO, "Transfer", "Normal2 began")

            self.valve1.close()
            self.valve2.open()
            self.valve3.close()
            self.valve4.close()
            self.pump2.run_for(t)
            self.pump1.stop()
            self.close_stop()
            Logger.get_instance().log(LogLevel.INFO, "Transfer", "Normal2 ended")

        except Exception as e:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Transfer", "normal2 ended: %s" % e)

    def cont1(self, t):
        """
        Case Contingency 1 for EA1(T1,P2,V3)
        :param t: time, the pump should run and the valve should be opened for transfer
        :return:
        """
        try:
            Logger.get_instance().log(LogLevel.INFO, "Transfer", "Cont1 began")

            self.valve1.close()
            self.valve2.close()
            self.valve3.open()
            self.valve4.close()
            self.pump1.stop()
            self.pump2.run_for(t)
            self.close_stop()
            Logger.get_instance().log(LogLevel.INFO, "Transfer", "Cont1 ended")

        except Exception as e:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Transfer", "cont1 ended: %s" % e)

    def cont2(self, t):
        """
        Case Contingency 1 for EA2 (T2,P1,V4)
        :param t: time, the pump should run and the valve should be opened for transfer
        :return:
        """
        try:
            Logger.get_instance().log(LogLevel.INFO, "Transfer", "Cont2 began")

            self.valve1.close()
            self.valve2.open()
            self.valve3.close()
            self.valve4.close()
            self.pump1.stop()
            self.pump2.run_for(t)
            self.close_stop()
            Logger.get_instance().log(LogLevel.INFO, "Transfer", "Cont2 ended")

        except Exception as e:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Transfer", "Cont2 ended: %s" % e)

    def safe_state(self):
        """
        Safe State (all pumps stopped and all valves closed)
        :return:
        """
        try:
            Logger.get_instance().log(LogLevel.INFO, "Transfer", "SAFE STATE initiated")

            self.valve1.close()
            self.valve2.close()
            self.valve3.close()
            self.valve4.close()
            self.pump1.stop()
            self.pump2.stop()
            Logger.get_instance().log(LogLevel.INFO, "Transfer", "SAFE STATE completed")

        except Exception as e:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Transfer", "safe_state() ended: %s" %e)

    def test_valve(self):
        import time
        print("open valve1?")
        self.valve1.open()
        time.sleep(20)
        print("open valve2?")
        self.valve1.close()
        self.valve2.open()
        time.sleep(20)
        print("open valve3?")
        self.valve2.close()
        self.valve3.open()
        time.sleep(20)
        print("open valve4?")
        self.valve3.close()
        self.valve4.open()
        time.sleep(20)
        print("close again?")
        self.valve4.close()


class Transfer:

    def __init__(self):
        pass

    def operation(self, status, t):
        """
        Transfer operation of combined Valve and Pumps
        :param status: (1-Normal1 for EA1, 2-Normal2 for EA2,
         3-Contingency1 for EA1, 4-Contingency2 for EA2, 0-close_stop)
        :param t: time of duration for running pump
        :return: none
        """

        i2c_action.gpio_expander1 = True
        i2c_action.gpio_expander2 = True
        if i2c_action.gpio_expander2 is True and i2c_action.gpio_expander1 is True:
            vp = ValvePump()
            if status == 1:
                vp.normal1(t=t)
                # Mechanik Ventil 1
            elif status == 2:
                vp.normal2(t=t)
                # Mechanik Ventil 2
            elif status == 3:
                vp.cont1(t=t)
                # Mechanik Ventil 3
            elif status == 4:
                vp.cont2(t=t)
                # Mechanik Ventil 4
            elif status == 0:
                vp.close_stop()
            elif status == -1:
                vp.safe_state()

        else:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Transfer", "GPIO Expander are not available")
            pass
