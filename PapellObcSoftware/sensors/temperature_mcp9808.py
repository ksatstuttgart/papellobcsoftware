'''##################################################
# PAPELL
# first created by Tobias Ott
# modified by Moritz Sauer
# modified by chris
# successfully tested Date 10.02.2018 by Tobias Ott
#
# Hardware level use of accelerometer
#
##################################################'''


from helpers.MCP9808 import MCP9808 as raw_temp
from state_manager.Logger import Logger


class Temperature:

    def __init__(self, address=0x18, **kwargs):
        """

        Initialize a Temperature Sensor on the specified I2C address and bus number.

        """
        try:
            if address is None:
                self.temp_sensor = raw_temp()
            else:
                self.temp_sensor = raw_temp(address, **kwargs)
            if self.temp_sensor.begin() is False:
                Logger.log(Logger.WARNING, "Could not initialize Temperature Sensor.")
        except Exception as e:
            self.temp_sensor = None
            Logger.log(Logger.ERROR, "Temperature sensor not found. %s" % str(e))

    def read(self):
        """
        :return: Returns the Temperature, in case of an error, returns None
        """
        try:
            return self.temp_sensor.readTempC()
        except Exception as e:
            Logger.log(Logger.ERROR, "Temperature sensor not found. %s" % str(e))
            return None
