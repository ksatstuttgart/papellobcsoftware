'''##################################################
# PAPELL
# first created by Tobias Ott
# modified by Moritz Sauer
# successfully tested Date 10.02.2018 by Tobias Ott
#
# Hardware level use of sound detector
#
##################################################'''


import math

import Adafruit_ADS1x15 as ADS1x15
from state_manager.Logger import Logger
from state_manager.initialcheck import i2c_action


class Sound:

    # Different usable resistors for R17 TODO
    RES1000K = 0
    RES470K = 1
    RES220K = 2
    RES100K = 3
    RES47K = 4
    RES22K = 5
    RES10K = 6
    RES4700 = 7
    RES2200 = 8

    def __init__(self, address=i2c_action.Addr_ADC_Sound, r3_mounted=True, r17_val=None):
        """Initialize a Sound Sensor ADC on the specified I2C address."""

        self.sound = ADS1x15.ADS1115(address)
        self.r3_mounted = r3_mounted
        self.r17_val = r17_val

    def read_raw(self):
        """
        Returns the raw data of the sound sensor as a list
        :return:
        """
        try:
            return [self.sound.read_adc(0, gain=1), self.sound.read_adc(1, gain=1), self.sound.read_adc(2, gain=1)]
        except:
            Logger.log(Logger.WARNING, "Could not read from sound sensor")

    def read_amp(self):
        """
        Returns the measured amplitude
        :return:
        """
        if self.r17_val is None:
            return 20*math.log(self.read_raw()[1], 10)

        return None # TODO

    def detect_sound(self):
        """
        Returns true if there is any sound
        :return:
        """

        if self.read_raw()[2] > 1000:
            return True

        return False
