'''##################################################
# PAPELL
# first created by Moritz Sauer
# successfully tested Date
#
# Class to detect the signal of the charger CHG_STAT1 Pin(6)
#
# Normal charging: Pin is high
# No charging: Pin is not defined to output
# Abnormal state(after interrupted charging) : Pin is blinking at 1Hz
#
##################################################'''

import RPi.GPIO as GPIO
from state_manager.Logger import *
from helpers.Singleton import Singleton


@Singleton
class Charger:

    def __init__(self):

        self._pin = 6
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self._pin, GPIO.IN)
        Logger.get_instance().log(LogLevel.VERBOSE, "Charger", "Charger initialised")

    def sample(self, sample_rate: int, timer: int, count: int, sample_cycles: int):

        """ has to be initialised with sample rate in Hz, starting values for timer and counter,
            and the amount of cycles, before decision
            :param sample_rate in Hz
            :param timer = 0
            :param count = 0
            :param sample_cycles measurement interval in sampling cycles

        """

        for i in range(sample_cycles):

            timer = timer + 1
            if GPIO.input(self._pin) == GPIO.HIGH:

                count = count + 1

            time.sleep(1 / int(sample_rate))

        if count/timer > 0.75:

            charging_flag = True
            idle_flag = False
            faulty_flag = False
            Logger.get_instance().log(LogLevel.INFO, "Charger", "Charger is on")

        elif 0.25 < count/timer < 0.75:

            charging_flag = False
            idle_flag = False
            faulty_flag = True
            Logger.get_instance().log(LogLevel.WARNING, "Charger", "Charger is in recovery state/has exceeded maximum charging time")

        elif count/timer < 0.1:

            charging_flag = False
            idle_flag = True
            faulty_flag = False
            Logger.get_instance().log(LogLevel.INFO, "Charger", "Charger is idle")

        else:

            Logger.get_instance().log(LogLevel.INFO, "Charger", "Charger is not giving proper signals")








