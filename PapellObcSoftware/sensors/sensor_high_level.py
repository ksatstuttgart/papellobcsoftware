'''##################################################
# PAPELL
# first created by Moritz Sauer
# modified by ...
# successfully tested Date 10.02.2018 by Tobias Ott
#
# Hardware level use of accelerometer
#
##################################################'''

from state_manager.Logger import Logger, LogLevel
from state_manager.initialcheck import i2c_action
from helpers.Database import QueuedDatabase
import time

try:
    from sensors.accelerometer_adxl335 import Accelerometer  # needs adc
    from sensors.current_sensor import Current
    from sensors.current_sensor import CurrentActuatorboard
    from sensors.magnetometer_mag3110 import LIS3MDL as Magnetometer
    from sensors.sounddetector_lmv324 import Sound  # needs adc!
    from sensors.temperature_mcp9808 import Temperature
    from helpers.i2c_switch import I2C_Switch
except Exception:
    Logger.get_instance().log(LogLevel.WARNING, "Sensors", "Import of Sensors did not work.")


class Sensor:
    def __init__(self):
        # init sensors
        self.sound = Sound()  # d(address=i2c_action.Addr_ADC_Sound)
        self.acc = Accelerometer(address=i2c_action.Addr_Acc)
        self.temp_sens = Temperature(address=i2c_action.Addr_TempSens_SensBoard)
        self.temp_obc = Temperature(address=i2c_action.Addr_TempSens_obc)

        # init magnetometers
        self.switch_mag = I2C_Switch(address=i2c_action.Addr_Switch_Mag)
        try:
            self.mag1 = Magnetometer(self.switch_mag, 0)
        except Exception:
            self.mag1 = False
            pass
        try:
            self.mag2 = Magnetometer(self.switch_mag, 1)
        except Exception:
            self.mag2 = False
            pass
        try:
            self.mag3 = Magnetometer(self.switch_mag, 2)
        except Exception:
            self.mag3 = False
            pass
        try:
            self.mag4 = Magnetometer(self.switch_mag, 3)
        except Exception:
            self.mag4 = False
            pass

        # init Current sensors
        self.current_act = CurrentActuatorboard(address=i2c_action.Addr_ADC_Curr_Act)
        self.current_ea = [[Current(address=i2c_action.Addr_ea1_adc1), Current(address=i2c_action.Addr_ea1_adc2)],
                           [Current(address=i2c_action.Addr_ea2_adc1), Current(address=i2c_action.Addr_ea2_adc2)]]

        # database
        #self.db = Database.get_instance()

    def get_mag(self, mag):
        if mag == False:
            return False
        else:
            try:
                return mag.get_mag()
            except:
                return False

    def data_full(self):
        # gives a set of Housekeeping data

        Logger.get_instance().log(LogLevel.INFO, "Sensors", "Taking Sensor data.")

        data = [time.time()]
        data2 = ["time"]
        # Data from Sensorboard

        # Temp sensors

        try:
            tempobc = self.temp_obc.read()
        except:
            tempobc = False

        if tempobc:
            data.append(str(tempobc))
            data2.append("temp_obc")

        try:
            tempsens = self.temp_sens.read()
        except:
            tempsens = False

        if tempsens:
            data.append(str(tempsens))
            data2.append("temp_sensor")



        # read from magnetometer ToDo: which magnet is on ea1/ea2

        m1 = self.get_mag(self.mag1)
        m2 = self.get_mag(self.mag2)
        m3 = self.get_mag(self.mag3)
        m4 = self.get_mag(self.mag4)

        if m1:
            data.append(str(m1))
            data2.append("mag1")

        if m2:
            data.append(str(m2))
            data2.append("mag2")

        if m3:
            data.append(str(m3))
            data2.append("mag3")

        if m4:
            data.append(str(m4))
            data2.append("mag4")

        # Soundsensor

        try:
           soundraw = self.sound.read_raw()
        except:
           soundraw = False

        if soundraw:
            data.append(str(soundraw))
            data2.append("sound")

        # Accelerometer

        try:
            acc = self.acc.read_raw()
        except:
            acc = False

        if acc:
            data.append(str(acc))
            data2.append("acc")

        # just debug things
        #print("temp: %s" % str(tempsens))
        #print("temp: %s" % str(tempobc))
        #print("mag 1: %s" % str(m1))
        #print("mag 2: %s" % str(m2))
        #print("mag 3: %s" % str(m3))
        #print("mag 4: %s" % str(m4))
        #print("sound: %s" % str(soundraw))
        #print("accel: %s" % str(acc))



        # Current Sensors EA1
        for i in range(8):
            try:
                data.append(self.current_ea[0][0].read(i))
                data2.append("cur_ea1_1%s" % str(i))
            except:
                pass
                #print("cant read cur_ea1_1%s" % str(i))

            try:
                    data.append(self.current_ea[0][1].read(i))
                    data2.append("cur_ea1_2%s" % str(i))
            except:
                pass
                #print("cant read cur_ea1_2%s" % str(i))


        # Current Sensors EA2
        for i in range(8):
            try:
                data.append(self.current_ea[1][0].read(i))
                data2.append("cur_ea2_1%s" % str(i))
            except:
                pass
                #print("cant read cur_ea2_1%s" % str(i))

            try:
                data.append(self.current_ea[1][1].read(i))
                data2.append("cur_ea2_2%s" % str(i))
            except:
                pass
                #print("cant read cur_ea2_2%s" % str(i))

        # Current Actuatorboard
        try:
            data.append(self.current_act.read(0))
            data2.append("cur_ac")
        except:
            pass
            #print("can't read actuator current  1")

        try:
            data.append(self.current_act.read(1))
            data2.append("cur_ac2")
        except:
            pass
            #print("can't read actuator current 2")

        QueuedDatabase.sensor_insert(data, data2)