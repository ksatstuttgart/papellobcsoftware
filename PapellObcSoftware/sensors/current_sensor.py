'''##################################################
# PAPELL
# first created by Daniel Galla
# modified by Tobias Ott
# successfully tested Date
#
# Transformation of ADC value to current value
#
##################################################'''

from helpers.AD799x import AD7998
from state_manager.initialcheck import i2c_action


class Current:

    # Specify different resistors for all the channels and adcs
    RESISTOR = {
        i2c_action.Addr_ea1_adc1: [ 0.1, 0.06,  0.1, 0.06,  0.1,  0.1, 0.06,  0.1],
        i2c_action.Addr_ea1_adc2: [0.06,  0.1,  0.1,  0.1,  0.1,  0.1, 0.06,  0.1],
        i2c_action.Addr_ea2_adc1: [ 0.1,  0.1, 0.06, 0.06,  0.1,  0.1,  0.1,  0.1],
        i2c_action.Addr_ea2_adc2: [ 0.1,  0.1,  0.1,  0.1,  0.1,  0.1,  0.1,  0.1]
    }

    # Initialization, address from ADC
    def __init__(self, address):
        self._i2c_address = address
        self.current_adc = AD7998(address)

    # Reading of ADC value, transformation into mA, with 0,1 Ohm
    def read(self, pin):
        current = (self.current_adc.analog_input(pin)*5 / (2**12)) / self.RESISTOR[self._i2c_address][pin] / 100 * 1000
        # current = input * 5V /(2^12)/Resistor/Gain * 1000 [for milliAmp]

        return current

"""
Current Sensors for Actuator Board
"""

import Adafruit_ADS1x15 as ADS1x15


class CurrentActuatorboard:

    def __init__(self, address):
        # Initialization, address from ADC
        self.current_adc = ADS1x15.ADS1115(address)

    def read(self, pin):
        # Reading of ADC value, transformation into mA, with 0,1 Ohm
        current = (self.current_adc.read_adc(pin, gain=1) * 8.4 / (2 ** 16)) / 0.1 / 50 * 1000
        return current


'''
if __name__ == "__main__":

    temp = Current

    print("zu")

    Current.read(1)

    print("end")
'''
