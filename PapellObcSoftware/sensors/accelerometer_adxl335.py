'''##################################################
# PAPELL
# first created by Tobias Ott
# modified by Moritz Sauer
# successfully tested Date 10.02.2018 by Tobias Ott
#
# Hardware level use of accelerometer
#
##################################################'''

import Adafruit_ADS1x15 as ADS1x15
from state_manager.Logger import Logger, LogLevel


class Accelerometer:

    def __init__(self, address=0x48):
        """Initialize a Accelerometer Sensor ADC on the specified I2C address."""

        self.acc = ADS1x15.ADS1115(address)
        self.cal_m = [0, 0, 0]
        self.cal_c = [0, 0, 0]

    def calibrate(self):            # TODO Unused, delete?
        """For Calibration of Sensor in G environment"""

        axis =['x', 'y', 'z']
        print("---Accelerometer calibration---")
        for i in range(0, 3):
            input("positive %s-Axis Down. Type sth. when ready:" % axis[i])
            A = self.read_raw()[i]
            input("negative %s-Axis Down. Type sth. when ready:" % axis[i])
            B = self.read_raw()[i]
            self.cal_m[i] = 2. / (A - B)
            self.cal_c[i] = 1 - (2. * A) / (A - B)

    def read_raw(self):
        return [self.acc.read_adc(0, gain=1), self.acc.read_adc(1, gain=1), self.acc.read_adc(2, gain=1)]

    def read(self):
        """
        :return: Returns the Acceleration in G, in case of an error, returns None
        """
        try:
            return [i*j+k for i, j, k in zip(self.read_raw(), self.cal_m, self.cal_c)]
        except:
            Logger.get_instance().log(LogLevel.WARNING, "Acceleration Sensor", "Acceleration sensor not found.")
            return None
