##################################################
# PAPELL
# first created by Robin Schweigert
# modified by Jan-Erik Brune
# successfully tested --
#
# This is the assembly for the overall magnet
# control.
#
##################################################
from helpers.ShiftRegister import *
from state_manager.Logger import *


class Magnets:

    EA_1 = 1
    EA_2 = 2

    def __init__(self, EA):
        self.magnets = []
        try:
            self.EA_aktiv = int(EA)
            if self.EA_aktiv == self.EA_1:
                self.SR = ShiftRegister(40, 8, 23, 25, 24)
                Logger.get_instance().log(LogLevel.INFO, "Magnets",
                                          "Shiftregister initialised with EA %s" % self.EA_aktiv)
            elif self.EA_aktiv == self.EA_2:
                self.SR = ShiftRegister(40, 7, 20, 12, 21)
                Logger.get_instance().log(LogLevel.INFO, "Magnets", "Shiftregister initialised")
            else:
                Logger.get_instance().log(LogLevel.EXCEPTION, "Magnets",
                                          "Shiftregister is not initialised due to wrong choosen EA")
            self.clear_all()
        except:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Magnets", "Shiftregister initialisation failed")

    def clear_all(self):
        """
        Switch off all magnets
        :return:
        """
        try:
            for i in range(40):
                self.SR.output(i, self.SR.LOW)
            self.magnets = []
            Logger.get_instance().log(LogLevel.INFO, "Magnets", "All magnets switched off")
            return 1
        except:
            Logger.get_instance().log(LogLevel.INFO, "Magnets", "All Magnet switch off failed")
            self.magnets = []
            return 0


    def switch_on(self, magnet: int):
        """
        Switch on one magnet
        :param magnet: magnet number, which should be turned on
        :return:
        """
        try:
            self.SR.output(self.number(magnet), self.SR.HIGH)
            Logger.get_instance().log(LogLevel.INFO, "Magnets", "Switched on magnet %s" % magnet)
        except:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Magnets", "Magnet switch on failed")
        self.magnets.append(self.number(magnet))

    def next(self, magnet: int):
        """
        Switch on next magnet
        :param magnet:
        :return:
        """
        self.clear_all()
        self.switch_on(self.number(magnet))

    def disable(self, magnet: int):
        """
        Switch off one magnet
        :param magnet: magnet number, which should be turned on
        :return:
        """
        try:
            self.SR.output(self.number(magnet), self.SR.LOW)
            Logger.get_instance().log(LogLevel.INFO, "Magnets", "Switched off magnet %s" % magnet)
        except:
            Logger.get_instance().log(LogLevel.EXCEPTION, "Magnets", "Magnet switch off failed")
        self.magnets.remove(self.number(magnet))

    def info(self):
        """
        Information on all magnets
        :return:
        """
        Logger.get_instance().log(LogLevel.INFO, "Magnets", "\nThis magnets are switched on \n" + str(self.magnets))

    def number(self, ea_number: int):
        """
        This method changes the EA Number into the belonging Shift Register Number.
        :param ea_number: The Number of the enumeration of all magnets by Operations Team
        :return: number for the Shift Register
        """

        mapping_ea1 = {1: 4, 2: 16, 3: 24, 4: 8, 5: 1, 6: 37, 7: 6, 8: 13, 9: 21, 10: 30, 11: 11, 12: 28, 13: 34, 14: 7,
                   15: 10, 16: 36, 17: 18, 18: 26, 19: 32, 20: 0, 21: 27, 22: 33, 23: 3, 24: 23, 25: 15, 26: 22, 27: 17,
                   28: 2, 29: 14, 30: 9, 31: 35, 32: 20, 33: 25, 34: 31, 35: 29, 36: 5, 37: 12}
        mapping_ea2 = {1: 5, 2: 11, 3: 0, 4: 1, 5: 2, 6: 29, 7: 31, 8: 35, 9: 33, 10: 21, 11: 23, 12: 25, 13: 27, 14: 6,
                   15: 8, 16: 9, 17: 10, 18: 4, 19: 7, 20: 26, 21: 24, 22: 22, 23: 20, 24: 34, 25: 32, 26: 30, 27: 28,
                   28: 3}
        if self.EA_aktiv == 1:
            # {EA Number: Shift Number}
            mapping = mapping_ea1
        elif self.EA_aktiv == 2:
            # {EA Number: Shift Number}
            mapping = mapping_ea2
        else:
            mapping = 0
            Logger.get_instance().log(LogLevel.EXCEPTION, "Magnets",
                                      "Shiftregister is not active due to wrong choosen EA")
        try:
            result = mapping[ea_number]
        except:
            result = 39
        return result
