import sys
try:
    from helpers.ShiftRegister import *
    from state_manager.Logger import Logger, LogLevel
except Exception as e:
    print("Imports failed %s" % str(e))


class TestSetup:

    def __init__(self):
        self.magnets = []
        try:
            self.SR = ShiftRegister(40, 6, 13, 19, 26)  # TODO define pins
            TestSetup.log("Shiftregister initialised")
        except Exception as e:
            TestSetup.log("Initialisation of shiftregister failed\n %s" % str(e))

    @staticmethod
    def log(msg):
        Logger.get_instance().log(LogLevel.INFO, "Magnets", msg)

    def clear_all(self):
        try:
            for i in range(40):
                self.SR.output(i, self.SR.LOW)
            TestSetup.log("switched all Magnets off")
        except Exception as e:
            TestSetup.log("Clearing failed %s " % str(e))
        self.magnets = []

    def switch_on(self, magnet: int):
        try:
            self.SR.output(magnet, self.SR.HIGH)
            TestSetup.log("Switched on magnet %s" % magnet)
        except Exception as e:
            TestSetup.log("Switch on failed %s " % str(e))
        self.magnets.append(magnet)

    def next(self, magnet: int):
        self.clear_all()
        self.switch_on(magnet)

    def switch_off(self, magnet: int):
        try:
            self.SR.output(magnet, self.SR.LOW)
            TestSetup.log("Switched off magnet %s" % magnet)
        except Exception as e:
            TestSetup.log("Switch off failed %s " % str(e))
        self.magnets.remove(magnet)

    def info(self):
        TestSetup.log("\nThis magnets are switched on \n" + str(self.magnets))


# if __name__ == '__main__':
    # Test = TestSetup()
