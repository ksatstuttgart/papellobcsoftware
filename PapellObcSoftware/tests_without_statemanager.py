##################################################
# PAPELL
# first created by Moritz Sauer
# modified by --
# successfully tested --
#
# Use this file to add any small testprogramms, that are run without statemanager
#
#
##################################################
from state_manager.Logger import Logger, LogLevel
from state_manager.initialcheck import i2c_action

from state_manager.initialcheck import i2c_action
from state_manager.Logger import Logger, LogLevel
from helpers.psu_powerline import Powerline
#from WIP_automatic_test import Test
import time
from helpers import *

import RPi.GPIO as GPIO


def alt_actuatorboard():

    from helpers.PCA9557 import PCA9557 as portexpander
    import time

    exp1 = portexpander(address=0x18)
    exp2 = portexpander(address=0x19)

    input("shall we start?")

    exp1.output(2, 1) # defines the step size
    exp1.output(3, 1) # defines the step size (see datasheet)

    input("start pump 1 and ventile?")
    exp1.output(1,1)
    exp1.output(7, 1) # makes Ventil1 open
    exp1.output(6, 1) # makes Ventil2 open
    input("turn off?")
    exp1.output(6, 0)
    exp1.output(7, 0)
    input("start motor?")
    exp1.output(4, 1) #enables motor to turn

    input("turn motor for 360 deg?")
    for i in range(100):
        exp1.output(5, 1)
        exp1.output(5, 0)
        time.sleep(.01)#longer time makes it turn slower

    input("Turning all off? (very important, press enter!!)")
    for i in range(9):
        exp1.output(i, 0)
        exp2.output(i, 0)


def check_actuatorboard():  # unsure if working, cut from intialcheck
    # check activity of actuatorboard with each component
    from helpers.PCA9557 import PCA9557 as portexpander
    import time

    gpio_actuatorboard = True

    if gpio_actuatorboard is True:
        exp1 = portexpander(address=0x18)
        exp2 = portexpander(address=0x19)

        exp1.output(1, 1)  # makes motor go
        time.sleep(.3)
        # ToDo: include Ampere measurement
        exp1.output(1, 0)  # stop motor

        exp1.output(7, 1)  # makes Ventil1 open
        exp1.output(6, 1)  # makes Ventil2 open

        time.sleep(.3)
        # ToDo: include Ampere measurement

        exp1.output(7, 0)  # makes Ventil1 close
        exp1.output(6, 0)  # makes Ventil2 close

        # start motor check
        exp1.output(2, 1)  # defines the step size
        exp1.output(3, 1)  # defines the step size (see datasheet)
        exp1.output(4, 1)  # enables motor to turn

        # turn motor for 180 deg
        for i in range(50):
            exp1.output(5, 1)
            exp1.output(5, 0)
            time.sleep(.01)  # longer time makes it turn slower


def actuatorboard():

    from helpers.PCA9557 import PCA9557 as portexpander
    from helpers.ADS1x15 import ADS1115
    import time

    exp = [portexpander(address=0x18),portexpander(address=0x19)]
    ads = ADS1115(address=0x4b)

    try:
        while True:
            hw = int(input("expander[0] or ads[1] or motor[2]?"))
            if hw == 0:
                num = int(input("Which expander?"))
                pin = int(input("Which pin?(0-7)"))
                state = int(input("On[1] or off[0]?"))
                exp[num].output(pin, state)
            elif hw == 1:
                for i in range(4):
                    print(ads.read_adc(i, gain=1))
            else:
                exp[1].output(4, 1)
                exp[1].output(3, 0)
                exp[1].output(2, 1)
                input("test")
                for i in range(1000):
                    print("Blobb")
                    exp[1].output(5, 1)
                    time.sleep(0.001)
                    print("Flopp")
                    exp[1].output(5, 0)
                    time.sleep(0.001)

    except Exception:
        for i in range(8):
            exp[0].output(i, 0)
            exp[1].output(i, 0)


def sensorboard(psu):

    from sensors.accelerometer_adxl335 import Accelerometer
    from sensors.magnetometer_mag3110 import LIS3MDL
    from sensors.sounddetector_lmv324 import Sound
    from sensors.temperature_mcp9808 import Temperature
    from helpers.i2c_switch import I2C_Switch
    import time

    psu_temp = psu
    for i in range(0, 5):
        psu_temp.BATTERIES[i].connect()
    psu_temp.connect_vext()
    psu_temp.disconnect_usb_power()

    switch = I2C_Switch(address=0x72)
    switch.openCH(0)
    mag = LIS3MDL(addr=0x1e)
    mag.enableLIS(temperature=False)
    acc = Accelerometer(address=0x48)
    sound = Sound(0x49)
    temp = Temperature(0x18)

    try:
        while True:
            print("Magnetometer: %s |    Acc: %s |" % (mag.get_mag(), acc.read_raw()))
            print("Sound: %s |    Temp: %s" % (sound.read_raw(), temp.read()))
            time.sleep(0.5)
    except:
        print("End")

    psu.disconnect_12v_powerline()
    psu.disconnect_vext()
    psu.disconnect_batteries()


def test_about_something():

    try:
        temporal_powerline = Powerline(5, 1)

        print("Strom in 5")

        time.sleep(5)

        temporal_powerline.connect()

    except Exception:

        print("could not connect 5V power")

    ea1()

    try:
        temporal_powerline.disconnect()
    except:
        print("5V poer could nt be turned off")


def test_Bat_Aktuatorboard():
    '''
    dieser Test ist mit der Flight Hardware: Aktuatorboard aber mit der Ground: OBC durchgeführt worden.
    :return:
    '''
    input("beginning?")
    from psu.PSUTest import PSUTest
    temp_psu = PSUTest()
    temp_psu.BATTERIES[4].connect()
    import RPi.GPIO as GPIO
    from state_manager.initialcheck import i2c_action
    GPIO.setmode(GPIO.BCM)

    try:
        input("Ladestrom ausschalten?")
        GPIO.setup(13, GPIO.OUT)
        GPIO.output(13, 1)
        input("Powerline 5V fur actuatorboard einschalten?")
        GPIO.setup(5, GPIO.OUT)
        GPIO.output(5, 0)
        input("adc")
        from smbus import SMBus
        i2c = SMBus(1)
        for pin in range(8):
            cmd = (1 << 7) | (pin >> 4)
            read = i2c.read_i2c_block_data(0x4b, cmd, 2)
            print("%s , %s" % (format(read[0], '008b'), format(read[1], '008b')))
        input("i2c")
        i2c = i2c_action()
        i2c.detect()
        input("Valve?")
        try:

            Logger.get_instance().log(LogLevel.INFO, "Test Valves", "beginning now with security being False.")
            from aktuators.transfer import Transfer
            transfer_spass = Transfer()
            transfer_spass.operation(1, 1)
        except:
            print("stop valve")
        input("close")
        GPIO.setup(5, GPIO.IN)
        input("close more")
        GPIO.output(13, 0)
        input("battery  disconnect")
        temp_psu.BATTERIES[4].disconnect()

    except:
        input("5V abschließen?")
        GPIO.setup(5, GPIO.IN)
        input("Ladestrom anschalten?")
        GPIO.setup(13, GPIO.OUT)
        input("ausschalten")
        GPIO.output(13, 0)
        input("battery ausschalten")
        temp_psu.BATTERIES[4].disconnect()


def Bat_aktivation():
    '''
    dieser Test ist mit der Flight Hardware: Aktuatorboard aber mit der Ground: OBC durchgeführt worden.
    :return:
    '''
    input("beginning?")
    from psu.PSUTest import PSUTest
    temp_psu = PSUTest()
    input("prepare bat")
    input("connect bat")
    temp_psu.BATTERIES[4].connect()
    input("import GPIO")
    import RPi.GPIO as GPIO
    from state_manager.initialcheck import i2c_action
    GPIO.setmode(GPIO.BCM)

    try:
        input("Ladestrom ausschalten?")
        GPIO.setup(13, GPIO.OUT)
        GPIO.output(13, 1)
        input("Powerline 5V fur actuatorboard einschalten?")
        GPIO.setup(5, GPIO.OUT)
        GPIO.output(5, 0)
    except:
        input("disable 5V")
        GPIO.setup(5, GPIO.IN)
        input("activate charging?")
        GPIO.setup(13, GPIO.OUT)
        GPIO.output(13, 0)
        time.sleep(3)
        input("Turn off Battery")
        temp_psu.BATTERIES[4].disconnect()


def old():
    """
    Tag 1: Pump Test
    from aktuators.pump import Pump

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(9, GPIO.OUT)
    time.sleep(1)
    GPIO.setup(13, GPIO.OUT)
    time.sleep(1)
    GPIO.output(9, GPIO.HIGH)
    time.sleep(1)
    GPIO.output(13, GPIO.HIGH)
    time.sleep(1)
    GPIO.setup(5, GPIO.OUT)
    time.sleep(1)
    GPIO.output(5, GPIO.LOW)
    time.sleep(1)
    GPIO.setup(26, GPIO.OUT)
    GPIO.output(26, GPIO.HIGH)
    time.sleep(1)

    pump = Pump(PCF8574, 1)

    pump.run_for(5)

    GPIO.output(26, GPIO.LOW)
    GPIO.output(9, GPIO.LOW)
    GPIO.output(13, GPIO.LOW)
    GPIO.setup(5, GPIO.IN)

    """
    #Tag 1: LED test

    #Ergebniss, 6/8 leds funktionieren

    GPIO.setmode(GPIO.BCM)
    from camera.lightning import Lightning

    GPIO.setup(21, GPIO.OUT)
    light = Lightning(20)

    GPIO.output(21, GPIO.HIGH)

    time.sleep(1)

    light.on()

    time.sleep(5)

    light.off()

    time.sleep(1)

    light.set(state_list=[1, 1, 1, 1, 0, 0, 0, 0])

    time.sleep(5)

    light.set(state_list=[0, 0, 0, 0, 0, 0, 0, 0])

    GPIO.output(21, GPIO.LOW)

    time.sleep(1)

    """
    # psu_menu = Test()
    #psu_menu = Test()

    # print("starting manual PSU control 1")
    # psu_menu.psu_test_1()
    # print("ending manual PSU control")
    # psu_menu.test_run_1(0,0,0,0,0,0,0,0,False,True)
    #psu_menu.test_run_1(0,0,0,0,0,0,0,0,False,True)
    ############################################

    Test().test_i2c()
    Test().test_database()

    # ea1()
    # Bat_aktivation()

    # Logger.get_instance().log(LogLevel.INFO, "Test i2cDetect", "beginning now")
    # test_i2c()
    # Logger.get_instance().log(LogLevel.INFO, "Test i2cDetect", "finished now")

    # test_database()

    # test_about_something()

    # actuatorboard()

    # Logger.get_instance().log(LogLevel.INFO, "Test Valves", "beginning now with security being False.")
    # from aktuators.transfer import Transfer
    # transfer_spass = Transfer()
    # transfer_spass.operation(1, 1)

    # from sensors.sensor_high_level import Sensor
    # Sensor().data_housekeeping()
    """


def robin_test_psu_no_need(psu):
    """funktioniert"""
    print("robin test starting")
    temp = psu
    input("connect bat")
    temp.BATTERIES[1].connect()
    input("connect 12V")
    temp.connect_12v_powerline()
    input("disconnect")
    temp.disconnect_vext()
    input("disconnect bat")
    temp.disconnect_batteries()
    print("robin test finished")


def robin_test_pump_no_need(psu):
    import time
    from helpers.PCA9557 import PCA9557
    print("robin test starting")
    temp = psu
    i2 = i2c_action()
    i2.detect()

    for i in range(0, 5):
         psu.BATTERIES[i].connect()
    time.sleep(0.1)
    input("start disconnection and start 5V")
    temp.disconnect_usb_power()
    temp.connect_12v_powerline()
    temp.connect_vext()
    input("start pumps")
    exp1 = PCA9557(address=0x18)
    exp2 = PCA9557(address=0x19)
    for i in range(0, 8):
        exp1.activate(i)
        exp2.activate(i)
    exp1.output(6, 1)
    time.sleep(1)
    exp1.output(7, 1)
    time.sleep(1)
    exp2.output(6, 1)
    time.sleep(1)
    exp2.output(7, 1)
    exp1.output(1, 1)
    exp2.output(1, 1)
    input("stop pumps")
    for i in range(0, 8):
        exp1.output(i, 0)
        exp2.output(i, 0)
    temp.disconnect_12v_powerline()
    temp.disconnect_vext()
    temp.disconnect_batteries()
    print("robin test finished")


def robin_test_motor(psu):
    """lauft noch nicht"""
    import time
    from helpers.PCA9557 import PCA9557
    from helpers.DRV8834 import DRV8834
    input("robin motor test starting")
    temp = psu
    for i in range(0, 5):
        temp.BATTERIES[i].connect()
    temp.connect_vext()
    temp.connect_12v_powerline()
    time.sleep(0.1)
    temp.disconnect_usb_power()
    input("start motor")
    exp1 = PCA9557(address=0x18)
    exp2 = PCA9557(address=0x19)
    mot1 = DRV8834(exp1)
    mot2 = DRV8834(exp2)
    for j in range(0, 2):
        print("first direction")
        mot1.set_dir(DRV8834.DIR_CCW)
        mot2.set_dir(DRV8834.DIR_CCW)
        for i in range(0, 20):
            mot1.step(1)
            mot2.step(1)
            time.sleep(0.5)
        print("other direction")
        mot1.set_dir(DRV8834.DIR_CW)
        mot2.set_dir(DRV8834.DIR_CW)
        for i in range(0, 20):
            mot1.step(1)
            mot2.step(1)
            time.sleep(0.5)
    temp.disconnect_12v_powerline()
    temp.disconnect_vext()
    temp.disconnect_batteries()
    print("robin motor test finished")


def robin_mot_deg_bad_bad_do_not_use(psu):
    "don't use"
    import time
    from helpers.PCA9557 import PCA9557
    from helpers.DRV8834 import DRV8834
    temp = psu
    for i in range(0, 5):
        temp.BATTERIES[i].connect()
    temp.connect_vext()
    temp.connect_12v_powerline()
    time.sleep(0.1)
    temp.disconnect_usb_power()
    input("start motor")
    exp1 = PCA9557(address=0x18)
    exp2 = PCA9557(address=0x19)
    mot1 = DRV8834(exp1)
    mot2 = DRV8834(exp2)
    mot1.set_dir(DRV8834.DIR_CCW)
    mot2.set_dir(DRV8834.DIR_CCW)
    for i in range(0, 15):
        mot1.step(1)
        mot2.step(1)
        time.sleep(0.5)
    temp.disconnect_12v_powerline()
    temp.disconnect_vext()
    temp.disconnect_batteries()
    print("robin motor test finished")


def robin_bat_test_not_needed_anymore(psu):
    import time
    from helpers.PCA9557 import PCA9557
    from helpers.DRV8834 import DRV8834
    temp = psu
    input("robin motor test starting")
    for i in range(0, 5):
        temp.BATTERIES[i].connect()
    temp.connect_12v_powerline()
    input("disable ext")
    temp.disconnect_usb_power()
    input("stop")
    temp.disconnect_12v_powerline()
    temp.disconnect_vext()
    temp.disconnect_batteries()
    print("robin motor test finished")


def robin_bat_read_no_need(psu):
    """no need"""
    temp = psu
    for i in range(0, 5):
        temp.BATTERIES[i].connect()
    temp.system_info()
    temp.disconnect_12v_powerline()
    temp.disconnect_vext()
    temp.disconnect_batteries()

def robin_load_noneed(psu):
    print("start loading")
    temp = psu
    for i in range(0, 5):
        temp.BATTERIES[i].connect()

def robin_check_discon_noneed(psu):
    print("robin test starting")
    temp = psu
    temp.BATTERIES[1].connect()
    temp.connect_12v_powerline()
    time.sleep(0.1)
    input("disable ext")
    temp.disconnect_usb_power()
    input("discon_1")
    input("discon_2")
    temp.disconnect_12v_powerline()
    temp.disconnect_vext()
    temp.disconnect_batteries()
    print("robin test finished")


def robin_led_test(psu):
    print("robin test starting")
    psu_temp = psu
    psu_temp.BATTERIES[1].connect()
    psu_temp.connect_vext()
    from helpers.LP55231 import LP55231
    temp = LP55231()
    input("led on")
    for i in range(0, 9):
        temp.SetChannelPWM(i, 255)
    input("led off")
    for i in range(0, 9):
        temp.SetChannelPWM(i, 0)
    psu_temp.disconnect_vext()
    psu_temp.disconnect_batteries()
    print("robin test finished")


def robin_ea2_test(psu):
    psu_temp = psu
    try:
        psu_temp = psu
        for i in range(0, 5):
            psu_temp.BATTERIES[i].connect()
        psu_temp.connect_vext()
        psu_temp.disconnect_usb_power()

        from helpers.ShiftRegister import ShiftRegister
        import sys
        import os

        sr = ShiftRegister(40, 7, 20, 12, 21)
        for i in range(0, 40):
            sr.output(i, ShiftRegister.LOW)
        while True:
            inp = input("enter magnet and state: ")
            if inp == "q":
                break
            elif len(inp.split(" ")):
                mag = int(inp.split(" ")[0])
                state = int(inp.split(" ")[1])
                if state == 1:
                    sr.output(mag, ShiftRegister.HIGH)
                    print("setting magnet %s to HIGH" % mag)
                else:
                    sr.output(mag, ShiftRegister.LOW)
                    print("setting magnet %s to LOW" % mag)
            else:
                print("please enter magnet and state or q to quit")

        print("powering off all magnets")
        for i in range(0, 40):
            sr.output(i, ShiftRegister.LOW)

    except:
        print("error")
    psu_temp.disconnect_vext()
    psu_temp.disconnect_batteries()
    print("robin test finished")


def robin_mag_test_notknown(psu):
    input("robin MGM Sensor starting")
    from helpers.i2c_switch import I2C_Switch
    from sensors.magnetometer_mag3110 import LIS3MDL
    # TODO search correct addresses
    switch = I2C_Switch(0x70)
    mag_0 = LIS3MDL(switch, 0)
    mag_1 = LIS3MDL(switch, 1)
    mag_2 = LIS3MDL(switch, 2)
    mag_3 = LIS3MDL(switch, 3)
    try:
        print(mag_0.get_mag())
    except Exception as e:
        print(e)
    print("robin test finished")


def moritz_test_sensors(psu):

    from sensors.magnetometer_mag3110 import LIS3MDL
    from sensors.accelerometer_adxl335 import Accelerometer
    from sensors.sounddetector_lmv324 import Sound
    from sensors.temperature_mcp9808 import Temperature
    from state_manager.Database import Database
    from helpers.i2c_switch import I2C_Switch
    from sensors.sensor_high_level import Sensor
    from sensors.current_sensor import Current, CurrentActuatorboard

    input("initialisation?")

    for i in range(0, 5):
         psu.BATTERIES[i].connect()
    time.sleep(0.1)
    input("start disconnection and start 5V")
    psu.disconnect_usb_power()
    psu.connect_12v_powerline()
    psu.connect_vext()

    try:

        collect = 1


        try:
            acc = Accelerometer()
        except:
            print("no acc")
        try:
            cur_ea_1 = Current(0x20)
        except:
            print("no current")

        try:
            cur_ea_2 = Current(0x23)
        except:
            print("no current")

        try:
            cur_ab = CurrentActuatorboard(0x4b)
        except:
            print("no current")

        from helpers.i2c_switch import I2C_Switch

        switch = I2C_Switch(0x70)

        try:
            mag_1 = LIS3MDL(switch, 1)
        except Exception as e:
            print("no 1 magnetometer: %s" % str(e))

        try:
            mag_2 = LIS3MDL(switch, 2)
        except:
            print("no  2 magnetometer")

        try:
            mag_3 = LIS3MDL(switch, 3)
        except:
            print("no 3 magnetometer")

        try:
            mag_4 = LIS3MDL(0x70, 4)
        except:
            print("no 4 magnetometer")

        try:
            mag_5 = LIS3MDL(0x70, 5)
        except:
            print("no 5 magnetometer")

        try:
            mag_6 = LIS3MDL(0x70, 1)
        except:
            print("no 6 magnetometer")


        try:
            mag_7 = LIS3MDL(0x70, 2)
        except:
            print("no 7 magnetometer")


        try:
            mag_8 = LIS3MDL(0x70, 3)
        except:
            print("no 8 magnetometer")



        try:
            mag_9 = LIS3MDL(0x70, 4)
        except:
            print("no 9  magnetometer")


        try:
            mag_10 = LIS3MDL(0x70, 5)
        except:
            print("no 10 magnetometer")


        try:
            sound = Sound()
        except:
            print("no sound")


        try:
            temp_1 = Temperature(0x1f)
        except:
            print("no magnetometer")

        try:
            temp_2 = Temperature(0x1b)
        except:
            print("no magnetometer")


    except:
        print("could not initialise")
        collect = 0

    try:
        while collect == 1:
            input("taking one data?")
            try:
                print("taking accelerometer data")
                print(acc.read())
                time.sleep(1)
            except:
                print("data of accelerometer is unavailable")
                time.sleep(1)

            try:

                mag_1.enableLIS()
                mag_2.enableLIS()
                mag_3.enableLIS()
                mag_4.enableLIS()
                mag_5.enableLIS()
                mag_6.enableLIS()
                mag_7.enableLIS()
                mag_8.enableLIS()
                mag_9.enableLIS()
                mag_10.enableLIS()

                input("taking current  data")
                print("ea_1")
                print(cur_ea_1.read(1))
                print(cur_ea_1.read(2))
                print(cur_ea_1.read(3))
                print(cur_ea_1.read(4))
                print(cur_ea_1.read(5))
                print(cur_ea_1.read(6))
                print(cur_ea_1.read(7))
                print(cur_ea_1.read(8))
                time.sleep(0.05)

                print("ea_2")
                print(cur_ea_2.read(1))
                print(cur_ea_2.read(2))
                print(cur_ea_2.read(3))
                print(cur_ea_2.read(4))
                print(cur_ea_2.read(5))
                print(cur_ea_2.read(6))
                print(cur_ea_2.read(7))
                print(cur_ea_2.read(8))
                time.sleep(1)

                print("ab")
                print(cur_ab.read(1))
                print(cur_ab.read(2))
                print(cur_ab.read(3))
                print(cur_ab.read(4))
                print(cur_ab.read(5))
                print(cur_ab.read(6))
                print(cur_ab.read(7))
                print(cur_ab.read(8))
                time.sleep(0.05)

            except:
                print("data of currentsensors is unavailable")
                time.sleep(1)
            try:
                input("taking magnet data")
                print("switch 1")
                print(mag_1.get_mag())
                print(mag_2.get_mag())
                print(mag_3.get_mag())
                print(mag_4.get_mag())
                print(mag_5.get_mag())
                print(mag_6.get_mag())
                print(mag_7.get_mag())
                print(mag_8.get_mag())
                print(mag_9.get_mag())
                print(mag_10.get_mag())

            except:
                print("data of magnetometer unavailable")
                time.sleep(1)
            try:
                input("taking sound data")
                print(sound.detect_sound())
            except:
                print("data of sound unavailable")
                time.sleep(1)
            try:

                input("taking temperature data")
                print("Sensorboard")
                print(temp_1.read())
                print("OBC")
                print(temp_2.read())
            except:
                print("data of temperature unavailable")
                time.sleep(1)

            ################

            print("completed 1 set")
            print("continue? yes[1], no[0]")
            collect = int(input(":"))
            print("ending data collection")

    except:
        print("major failure, ending")

    psu.disconnect_12v_powerline()
    psu.disconnect_vext()
    psu.disconnect_batteries()


def martin_i2c():
    input("i2c detection?")
    i2c = i2c_action()
    i2c.detect()


def martin_5V_enable():

    input("init Bat")
    print("5V test starting")
    print(" PSU Initialisation turns off Battery and powers over USB"
          "no charging the batteries.")
    from psu.PSUTest import PSUTest
    temp = PSUTest()
    input("connect bat")
    temp.BATTERIES[4].connect()
    temp.bat_info(temp.BATTERIES[4])

    if int(input("disconnect usb charging power? 0 for no")) != 0:
        temp.disconnect_usb_power()
        print("charging disabled.")
    input("enable 5V?")
    temp.connect_vext()
    temp.bat_info(temp.BATTERIES[4])
    if int(input("disconnect V extern? 0 for no")) != 0:
        temp.bat_info(temp.BATTERIES[4])
        temp.disconnect_vext()
        print("5V is disabled.")
    # input("disconnect bat")
    # temp.disconnect_batteries()
    print("Battery config finished")
    """
        dieser Test ist mit der Flight Hardware: Aktuatorboard aber mit der Ground: OBC durchgeführt worden.
        :return:"""


def martin_magnet_test():
    """does not work! with EA2"""
    input("start Magnet test?")
    from magnets.magnet_testsetup import TestSetup
    Shift = TestSetup()
    Shift.clear_all()
    try:
        for i in range(28):

            input("turn on nr %s?" % i)
            Shift.switch_on(i)
            input("turn off?")
            Shift.switch_off(i)
    except Exception as e:
        print(e)


def martin_ea2():
    from helpers.AD799x import AD7998
    from helpers.ShiftRegister import ShiftRegister
    import time

    shiftreg_ea1 = ShiftRegister(40, 8, 23, 25, 24)
    shiftreg = ShiftRegister(27, 7, 20, 12, 16)
    ad7998 = [AD7998(address=0x21), AD7998(address=0x22)]
    print("Test EA2 start")

    #temporal_powerline.connect()

    try:
        for pin in range(0,40):
            shiftreg.output(pin, ShiftRegister.LOW)
            shiftreg_ea1.output(pin,ShiftRegister.LOW)

        while True:
            item = int(input("Control adc[0] or shiftreg[1]?"))
            if item == 0:
                adc = int(input("Read from adc 0 or 1 ?"))
                for i in range(1, 9):
                    print("pin %s : %s" % (i, ad7998[adc].analog_input(i)))

            else:
                item2 = int(input("1 for program"))

                if item2 == 1:

                    for i in range(20):
                        print(i)
                        shiftreg.output(i, 1)
                        time.sleep(1)
                        shiftreg.output(i, 0)
                        time.sleep(1)
                else:
                    pin = int(input("Enter Shiftreg pin(0-39)"))
                    state = int(input("High[1] or low[0]?"))
                    shiftreg.output(pin, state)
    except:
        for pin in range(0, 40):
            shiftreg.output(pin, ShiftRegister.LOW)


def moritz_detection(psu):
    psu_temp = psu
    for i in range(0, 5):
        psu_temp.BATTERIES[i].connect()
    psu_temp.connect_vext()
    psu_temp.connect_usb_power()

    from sensors.charger_detection import Charger

    Charger.get_instance().sample(10, 1, 1, 5)
    input("still charging")


def moritz_1hz_signal(s):

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(23, GPIO.OUT)

    # 1Hz Signal
    for i in range(s):
        GPIO.output(23, GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output(23, GPIO.LOW)
        time.sleep(0.5)


def moritz_sample_test_nodono():


    from multiprocessing import Process


    # charger detection
    Process(target=moritz_1hz_signal, args=([50])).start()

    # detection
    Process(target=moritz_detection, args=()).start()


def robin_ea1_test(psu):
    psu_temp = psu
    try:
        psu_temp = psu
        for i in range(0, 5):
            psu_temp.BATTERIES[i].connect()
        psu_temp.disconnect_usb_power()
        psu_temp.connect_vext()
        from helpers.ShiftRegister import ShiftRegister
        import sys
        import os

        sr = ShiftRegister(40, 8, 23, 25, 24)
        for i in range(0, 40):
            sr.output(i, ShiftRegister.LOW)
        while True:
            inp = input("enter magnet and state: ")
            if inp == "q":
                break
            elif len(inp.split(" ")):
                mag = int(inp.split(" ")[0])
                state = int(inp.split(" ")[1])
                if state == 1:
                    sr.output(mag, ShiftRegister.HIGH)
                    print("setting magnet %s to HIGH" % mag)
                else:
                    sr.output(mag, ShiftRegister.LOW)
                    print("setting magnet %s to LOW" % mag)
            else:
                print("please enter magnet and state or q to quit")#
    except:
        pass


def martin_test_valve_motor(psu):
    from aktuators.transfer import ValvePump
    from state_manager.initialcheck import i2c_action

    for i in range(0, 5):
         psu.BATTERIES[i].connect()
    time.sleep(0.1)
    input("start disconnection and start 5V")
    psu.disconnect_usb_power()
    psu.connect_12v_powerline()
    psu.connect_vext()

    input("init Valve")
    vp = ValvePump()
    vp.close_stop()
    input("start version 1 for 2sec")
    vp.normal1(15)
    input("start alternative 2 for 2 sec")
    vp.cont1(15)
    input("start alternative 2 for 2 sec")
    vp.normal2(15)
    input("start alternative 2 for 2 sec")
    vp.cont2(15)
    vp.close_stop()

    psu.disconnect_12v_powerline()
    psu.disconnect_vext()
    psu.disconnect_batteries()


def robin_test_pump_manuel(psu):
    import time
    from helpers.PCA9557 import PCA9557
    print("robin test starting")
    temp = psu
    i2 = i2c_action()
    i2.detect()

    for i in range(0, 5):
         psu.BATTERIES[i].connect()
    time.sleep(0.1)
    input("start disconnection and start 5V")
    temp.disconnect_usb_power()
    temp.connect_12v_powerline()
    temp.connect_vext()
    input("start pumps")
    exp1 = PCA9557(address=0x18)
    exp2 = PCA9557(address=0x19)
    for i in range(0, 8):
        exp1.activate(i)
        exp2.activate(i)
    #exp1.output(6, 1)  #valve1
    time.sleep(1)
    #exp1.output(7, 1)  # Valve 2
    time.sleep(1)
    #exp2.output(6, 1)  # Valve 3
    time.sleep(1)
    #exp2.output(7, 1)  # Valve 4
    #exp1.output(1, 1)  # Pumpe 1
    exp2.output(1, 1)  # Pumpe 2
    input("stop pumps")
    for i in range(0, 8):
        exp1.output(i, 0)
        exp2.output(i, 0)
    temp.disconnect_12v_powerline()
    temp.disconnect_vext()
    temp.disconnect_batteries()
    print("robin test finished")


def tobias_sensors_test():
    from sensors.magnetometer_mag3110 import LIS3MDL
    from sensors.accelerometer_adxl335 import Accelerometer
    from sensors.sounddetector_lmv324 import Sound
    from sensors.temperature_mcp9808 import Temperature
    from helpers.i2c_switch import I2C_Switch
    import time

    # Sounddetector
    try:
        sound = Sound(address=0x49)
        print("Sound: %s " % sound.read_raw())
    except Exception as e:
        print("ERROR[Sounddetector]: %s" % e)

    time.sleep(1)
    # Accelerometer
    try:
        acc = Accelerometer(address=0x48)
        print("Accelerometer: %s" % acc.read_raw())
    except Exception as e:
        print("ERROR[Accelerometer]: %s" % e)

    time.sleep(1)
    # Temperature
    try:
        temp_psu = Temperature(address=0x1B)
        temp_sensor = Temperature(address=0x1F)

        print("Temperature PSU: %s" % temp_psu.read())
        print("Temperature Sensorboard: %s" % temp_sensor.read())
    except Exception as e:
        print("ERROR[Temperature]: %s" % e)

    time.sleep(1)
    # Magnetometer
    switch = I2C_Switch(address=0x70)
    mag = []
    for channel in range(4):
        try:
            mag.append(LIS3MDL(switch=switch, channel=channel))
            print("Magnetometer %s: %s" % (channel, mag[channel].get_mag()))
        except Exception as e:
            print("ERROR Magnetometer %s: %s" % (channel, e))

    switch.reset()
    mag = []
    for channel in range(4):
        try:
            mag.append(LIS3MDL(switch=switch, channel=channel))
            print("Magnetometer %s: %s" % (channel, mag[channel].get_mag()))
        except Exception as e:
            print("ERROR Magnetometer %s: %s" % (channel, e))



def tobias_current_sensor_test(psu):
    from state_manager.initialcheck import i2c_action
    from sensors.current_sensor import Current
    from sensors.current_sensor import CurrentActuatorboard


    for i in range(0, 5):
         psu.BATTERIES[i].connect()
    time.sleep(0.1)
    input("start disconnection and start 5V")
    psu.disconnect_usb_power()
    psu.connect_12v_powerline()
    psu.connect_vext()

    address = [i2c_action.Addr_ea1_adc1, i2c_action.Addr_ea1_adc2, i2c_action.Addr_ea2_adc1, i2c_action.Addr_ea2_adc2]
    # Current sensors on EA
    for i in range(4):
        try:
            current_1 = Current(address[i])
            print("Current Sensor [%s] on ea" % address[i])
            for i in range(8):
                print("Channel %s: %5.10f" % (i, current_1.read(i)))
        except Exception as e:
            print("Error Current Sensor [%s] on ea: %s" % (address[i], e))

    # Current sensor on Actuatorboard
    try:
        current_act = CurrentActuatorboard(i2c_action.Addr_ADC_Curr_Act)
        print("Current Sensor [%s] on act.board" % i2c_action.Addr_ADC_Curr_Act)
        for i in range(4):
            print("Channel %s: %5.10f" % (i, current_act.read(i)))
    except Exception as e:
        print("Error Current Sensor [%s] on act.board: %s" % (i2c_action.Addr_ADC_Curr_Act, e))

    psu.disconnect_12v_powerline()
    psu.disconnect_vext()
    psu.disconnect_batteries()


def martin_test_each_valve(psu):
    from aktuators.transfer import ValvePump
    from aktuators.valve import Valve
    from state_manager.initialcheck import i2c_action

    for i in range(0, 5):
         psu.BATTERIES[i].connect()
    time.sleep(0.1)
    input("start disconnection and start 5V")
    psu.disconnect_usb_power()
    psu.connect_12v_powerline()
    psu.connect_vext()

    input("init Valve")
    vp = ValvePump()
    vp.close_stop()

    vp.test_valve()

    psu.disconnect_12v_powerline()
    psu.disconnect_vext()
    psu.disconnect_batteries()


def martin_current_vllt(psu):
    from sensors.sensor_high_level import Sensor
    from state_manager.initialcheck import i2c_action

    for i in range(0, 5):
         psu.BATTERIES[i].connect()
    time.sleep(0.1)
    input("start disconnection and start 5V")
    psu.disconnect_usb_power()
    psu.connect_12v_powerline()
    psu.connect_vext() #
    # psu.connect_vext()

    input("Current sensoren?")
    for i in range(3):
        i2 = i2c_action()
        i2.detect()
        Sen = Sensor()
        Sen.data_full()
        input("try again?")

    psu.disconnect_12v_powerline()
    psu.disconnect_vext()
    psu.disconnect_batteries()


def martin_test_bat_disconnection_for_electronics(psu):

    for i in range(0, 5):
         psu.BATTERIES[i].connect()
    time.sleep(0.1)
    input("start disconnection and start 5V")
    psu.disconnect_usb_power()
    input("ready for durty stuff")

def final_LED_Camera_test():

    from camera.lightning import Lightning
    from camera.cameras import Camera
    import time

    light = Lightning(7)
    camera = Camera()


    print("Lights ON")
    time.sleep(1)
    light.on()
    input("OFF")
    time.sleep(1)
    print("taking picture")
    camera.picture()

    print("Lights OFF")


if __name__ == "__main__":

    from psu.psu import PSU
    from helpers.Database import QueuedDatabase
    queue = QueuedDatabase("Database/" + Logger.get_instance().new_beginning(".db", "Database/")+"_database")
    queue.start()
    temp = PSU(None)
    try:
        temp.enter_execution()
        """
        #from sensors.accelerometer_adxl335 import Accelerometer
        #import time
        #try:
        #    acc = Accelerometer(address=0x48)
        #    for i in range(60):
        #        print("Accelerometer: %s" % acc.read_raw())
        #        time.sleep(1)
        #except Exception as e:
        #    print("ERROR[Accelerometer]: %s" % e)
        #from state_manager.Houston_Test_March2018 import HoustonMarch
        #tobias_sensors_test()
        #hm = HoustonMarch
        print("los gehts")
        temp.enter_execution()
        time.sleep(2)
        #from aktuators.pump import Pump
        #from helpers.PCA9557 import PCA9557
        #from state_manager.Logger import LogLevel, Logger
        from state_manager.initialcheck import i2c_action
        i2c = i2c_action()

        #expander2 = PCA9557(address=i2c_action.Addr_exp2)  # defined i2c address
        #pump2 = Pump(expander2, i2c_action.Pin_exp2_pump)
        #print("achtung")
        #time.sleep(2)
        #print("ready set go...")
        #pump2.run_for(10)
        
        i2c.detect()
        #time.sleep(2.0)

        input("Transfer Class")
        from aktuators.transfer import Transfer
        trans = Transfer()
        input("Transfer Class")
        trans.operation(2, 260)
        
        input("Tobi 1umpen Class")
        from aktuators.transfer import PumpingSystem
        input("Tobi 2umpen Class")
        sysi = PumpingSystem()
        input("Tobi Pumpen Class")
        sysi.start(PumpingSystem.EA_1, PumpingSystem.PUMP_2)
        time.sleep(60)
        sysi.stop()
         
        #print("motortest")
        #ttt = 10 # time for motor to run
        #from aktuators.motor import motor
        #from helpers.PCA9557 import PCA9557
        #from helpers.DRV8834 import DRV8834
        #exp1 = PCA9557(0x18)
        #mot1 = DRV8834(exp1)
        #print("startversuch1")
        #time.sleep(1)d
        #mot1.set_dir(DRV8834.DIR_CCW)
        #for i in range(0, ttt*5):
            #mot1.step(1)
            #time.sleep(0.5)

        #momo = motor()
        #momo.start(9)
        #momo = motor()

        #temp.connect_usb_power()
        #time.sleep(2)
        #temp.disconnect_12v_powerline()
        #temp.disconnect_vext()
        #temp.connect_usb_power()
        #print("charging till death")
        #time.sleep(2.0)
        #temp.disconnect_12v_powerline()
        """
        print("los gehts")
        input("breakpoint")
        temp.psu.all_info()
        i2c = i2c_action()
        i2c.detect()
        from sensors.sensor_high_level import Sensor
        sen = Sensor()
        input("breakpoint")
        sen.data_full()
        raw_in = input("sensor?")
        if raw_in =
= "y":
            for i in range(0, 120):
                sen.data_full()
                time.sleep(2)
                #temp.psu.disconnect_vext()
        temp.exit_execution()

        temp.charging()
    except Exception as e:
        print(e)
    print("test finished")

    #temp.psu.disconnect_12v_powerline()
    #temp.psu.disconnect_vext()
    #temp.psu.connect_usb_power()



    #time.sleep(2.0)
    #temp.disconnect_usb_power()
    #for ttt in range(120):
    #    temp.all_info()
    #    time.sleep(2)
    #print("charging till death")
    #time.sleep(2.0)
    #temp.disconnect_12v_powerline()
    #tobias_current_sensor_test(temp)
