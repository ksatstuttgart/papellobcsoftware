'''##################################################
# PAPELL
# first created by Moritz Sauer
# modified by --
# successfully tested --
#
# This is a program that should allow us to
# comfortably test all the functions of the experiment
# while also running in the statemanager
# Should be able to:
#
#    -detecting of i2c devices(i2c_action())
#    -detect consumtion of power
#    -safe data from sensors
#    -display data from sensors
#    -run magnets testing program
#    -run pump/valve testing program
#    -
#
#
#
##################################################'''

from sensors.accelerometer_adxl335 import Accelerometer
from sensors.magnetometer_mag3110 import LIS3MDL
from sensors.sounddetector_lmv324 import Sound
from sensors.temperature_mcp9808 import Temperature
from helpers.i2c_switch import I2C_Switch
from state_manager.Logger import Logger, LogLevel
from state_manager.Database import Database
from aktuators.pump import Pump
import time
from aktuators.valve import Valve
from camera.lightning import Lightning
from camera.cameras import Camera
from helpers.DRV8834 import DRV8834
from psu.PSUTest import PSUTest
from helpers.psu_powerline import Powerline
from state_manager.initialcheck import i2c_action
import RPi.GPIO as GPIO
from helpers.PCF8574 import PCF8574


def print_sensorboard():

    try:
        switch = I2C_Switch(address=i2c_action.Addr_Switch1)
        switch.openCH(0)
        mag = LIS3MDL(addr=i2c_action.Addr_Mgm)
        mag.enableLIS(temperature=False)
        acc = Accelerometer(address=i2c_action.Addr_Acc)
        sound = Sound()
        temp = Temperature(i2c_action.Addr_TempSens_SensBoard)
        temp = Temperature(i2c_action.Addr_TempSens_obc)
        #TODO: insert current sensors

        while True:
            print("Magnetometer: %s |    Acc: %s |" % (mag.get_mag(), acc.read_raw()))
            print("Sound: %s |    Temp: %s" % (sound.read_raw(), temp.read()))
            time.sleep(0.5)
    except:
        print("End")


def database_sensorboard():

    switch = I2C_Switch(i2c_action.Addr_Switch1)  # ToDo: check witch switch! korrekt für address=0x72
    switch.openCH(0)

    mag = LIS3MDL(addr=i2c_action.Addr_Mgm)
    mag.enableLIS(temperature=False)
    acc = Accelerometer(address=i2c_action.Addr_Acc)
    sound = Sound(i2c_action.Addr_ADC_Sound)
    temp_obc = Temperature(i2c_action.Addr_TempSens_obc)
    temp_sensb = Temperature(i2c_action.Addr_TempSens_SensBoard)
    # TODO: insert current sensors

    # TODO: Database topics


    try:
        while True:

            db = Database.get_instance()

            # TODO: fill out the inserts with correct topic and values

            db.insert_value(db.Sens_ea1_magnet_1, 10)  # saves value 100 with the topic db.suppply_voltage_single

            db.insert_value(db.Sens_ea2_magnet_1, 41)
            db.insert_value()
            db.insert_value()
            db.insert_value()
            db.insert_value()
            db.insert_value()
            db.insert_value()
            time.sleep(0.5)

    except:

        print("End")


class Test:

    def __init__(self):
        pass

    def test_run_1(self, run_time_1: int, run_time_2: int, open_time_1: int,
                   open_time_2: int, open_time_3: int, open_time_4: int, steps_1: int,
                   steps_2: int, camera_on: bool, led_test: bool, ):

        """
        :param -pump1
        :param -pump2
        :param -valve1
        :param -valve2
        :param -valve3
        :param -valve4
        :param -servo1
        :param -servo2
        :param -LEDs
        :param -camera1
        :param -camera2
        :param -camera3

        """
        # i2c = i2c_action()

        expander1 = PCF8574(address=i2c_action.Addr_exp1)  # defined i2c address
        expander2 = PCF8574(address=i2c_action.Addr_exp2)  # defined i2c address

        valve1 = Valve(expander1, 1)
        valve2 = Valve(expander1, 2)
        pump1 = Pump(expander1, 0)

        valve3 = Valve(expander2, 1)
        valve4 = Valve(expander2, 2)
        pump2 = Pump(expander2, 0)

        '''    
             Tank1            Tank2
               |                |
               |                |
             Pump1            Pump2
               |                |
               |-------------   |
               |     -------x---|
               |     |      |   |
          Valve1  Valve3  Valve4  Valve2
               |  |            |  |
                EA1             EA2
        '''
        servo1 = DRV8834(1, 2, 3, 4)               # TODO: dir_pin, step_pin, m0_pin, m1_pin
        servo2 = DRV8834(1, 2, 3, 4)               # TODO: dir_pin, step_pin, m0_pin, m1_pin
        led = Lightning(8)
        camera1 = Camera()
        camera2 = Camera()  # TODO: create Camera Class
        camera3 = Camera()

        # pump part
        pump1.run_for(run_time_1)
        pump2.run_for(run_time_2)

        # valve part
        valve1.open_for(open_time_1)
        valve2.open_for(open_time_2)
        valve3.open_for(open_time_3)
        valve4.open_for(open_time_4)

        # servo part
        servo1.step(steps_1)
        servo2.step(steps_2)

        # camera part, all LEDs on
        if camera_on == True:
            led.on()
            camera1
            camera2     # TODO: create camera class
            camera3
            led.off()

        else:
            pass

        # only LEDs on, increasing by 1 per sec
        if led_test == True:

            GPIO.setmode(GPIO.BCM)

            GPIO.setup(21, GPIO.OUT)
            GPIO.output(21, GPIO.HIGH)
            time.sleep(1)

            # LED part
            led.set(state_list=[1, 0, 0, 0, 0, 0, 0, 0])
            time.sleep(1)
            led.set(state_list=[1, 1, 0, 0, 0, 0, 0, 0])
            time.sleep(1)
            led.set(state_list=[1, 1, 1, 0, 0, 0, 0, 0])
            time.sleep(1)
            led.set(state_list=[1, 1, 1, 1, 0, 0, 0, 0])
            time.sleep(1)
            led.set(state_list=[1, 1, 1, 1, 1, 0, 0, 0])
            time.sleep(1)
            led.set(state_list=[1, 1, 1, 1, 1, 1, 0, 0])
            time.sleep(1)
            led.set(state_list=[1, 1, 1, 1, 1, 1, 1, 0])
            time.sleep(1)
            led.set(state_list=[1, 1, 1, 1, 1, 1, 1, 1])
            time.sleep(1)

            GPIO.output(21, GPIO.LOW)

        else:
            pass

    def psu_test_1(self):

        # TODO: fg, ausmessen mit multimeter

        """
        electric test of

        pins to set,
        raspi strom immer an
        3v auch immer an
        vusb_disable macht strom von aussen weg, charger chip wird auch abgeschalten, default: low->an!!!
        vextern_enable, 5v + 12v, 12v muss extra enabled werden,
        12v_enable: default:low -> aus,


        :return:
        """

        """
        Test should do the following:
        -switch every battery of and on
        -change from charging to not charging
        -use different combinations of batteries
        
        
        """

        #temp_psu = PSUTest()

        import RPi.GPIO as GPIO

        GPIO.setmode(GPIO.BCM)

        print("this is te newest version")

        print("Select Battery to be turned on")

        for i in range(5):

            print("press smth. other than [1,2,3,4,5] to not select battery")

            print("which battery should be turned on?")

            battery = int(input(": "))

            GPIO.setup(9, GPIO.OUT)
            GPIO.setup(10, GPIO.OUT)
            GPIO.setup(11, GPIO.OUT)
            GPIO.setup(22, GPIO.OUT)
            GPIO.setup(27, GPIO.OUT)

            try:
                if battery ==0:

                    print("trying to connect battery 0")
                    #temp_psu.BATTERIES[0].connect()
                    GPIO.output(22, GPIO.HIGH)

                elif battery ==1:
                    print("trying to connect battery 1")
                    #temp_psu.BATTERIES[1].connect()
                    GPIO.output(10, GPIO.HIGH)

                elif battery ==2:
                    print("trying to connect battery 2")
                    #temp_psu.BATTERIES[2].connect()
                    GPIO.output(9, GPIO.HIGH)

                elif battery ==3:
                    print("trying to connect battery 3")
                    #temp_psu.BATTERIES[3].connect()
                    GPIO.output(27, GPIO.HIGH)

                elif battery ==4:
                    print("trying to connect battery 4")
                    #temp_psu.BATTERIES[4].connect()
                    GPIO.output(11, GPIO.HIGH)

                else:

                    print("input not found, therfore exiting this loop")
                    i = 5
                    pass

            except:

                print("input did not work")

        menu = True

        while menu == True:

            GPIO.setmode(GPIO.BCM)

            GPIO.setup(26, GPIO.OUT)


            print("which circuit should be connected/diconnected ? ")
            print("VEXT_EN(5v) = 0, VUSB_DIS(charging) = 1, 12V_EN(10v) = 2, end selection = 3 ")
            circuit_selection = int(input(":"))

            try:

                if circuit_selection == 0:

                    GPIO.output(5, GPIO.OUT)
                    print(" on[1] or off[0]?")
                    input_0 = int(input(":"))
                    #power_5v = Powerline(5, True, "power 5v")

                    if input_0 == 1:
                        #power_5v.connect()
                        GPIO.output(5, GPIO.LOW)

                    if input_0 == 0:
                        #power_5v.disconnect()
                        GPIO.output(5, GPIO.HIGH)


                if circuit_selection == 1:
                    print(" entladen der Battery[1] or weiter Laden uber USB[0]?")
                    input_1 = int(input(":"))
                    #power_usbv = Powerline(13, False, "VUSB_DIS")
                    GPIO.setup(13, GPIO.OUT)

                    if input_1 == 1:
                        #power_usbv.connect()
                        GPIO.output(13, GPIO.HIGH)

                    if input_1 == 0:
                        #power_usbv.disconnect()
                        GPIO.output(13, GPIO.LOW)

                if circuit_selection == 2:
                    print(" on[1] or off[0]?")
                    input_2 = int(input(":"))
                    #power_12v = Powerline(26, True, "power 5v")
                    GPIO.setup(26, GPIO.OUT)

                    if input_2 == 1:
                        #power_12v.connect()
                        GPIO.output(26, GPIO.HIGH)

                    if input_2 == 0:
                        #power_12v.disconnect()
                        GPIO.output(26, GPIO.LOW)

                if circuit_selection == 3:

                    menu = False

                else:
                    menu = False

            except:

                print("input did not work")

        print("Battery off selection:")

        for i in range(5):

            print("press smth. other than [1,2,3,4,5] to not select a battery")
            print("which battery should be turned off?")
            battery = int(input(":"))

            try:

                if battery == 0:

                    print("trying to disconnect battery 0")
                    #temp_psu.BATTERIES[0].disconnect()
                    GPIO.output(22, GPIO.LOW)

                elif battery == 1:
                    print("trying to disconnect battery 1")
                    #temp_psu.BATTERIES[1].disconnect()
                    GPIO.output(10, GPIO.LOW)

                elif battery == 2:
                    print("trying to disconnect battery 2")
                    #temp_psu.BATTERIES[2].disconnect()
                    GPIO.output(9, GPIO.LOW)

                elif battery == 3:
                    print("trying to disconnect battery 3")
                    #temp_psu.BATTERIES[3].disconnect()
                    GPIO.output(27, GPIO.LOW)

                elif battery == 4:
                    print("trying to disconnect battery 4")
                    #temp_psu.BATTERIES[4].disconnect()
                    GPIO.output(11, GPIO.LOW)

                else:
                    print("input not found, therfore exiting this loop")
                    i = 5
                    pass
            except:

                print("input did not work")

    def test_i2c(self):

        Logger.get_instance().log(LogLevel.INFO, "Test i2cDetect", "beginning now with i2c_action()")
        i2c = i2c_action()
        i2c.detect()
        print("gpio Expander is ", i2c.gpio_expander2)
        print("Actuatorboard is", i2c.gpio_actuatorboard)
        Logger.get_instance().log(LogLevel.INFO, "Test i2cDetect", "finished now")

    def test_database(self):

        Logger.get_instance().log(LogLevel.INFO, "Test Database", "beginning with database testing here")
        from state_manager.Database import Database

        db = Database.get_instance()

        db.insert_value(db.Sens_ea1_magnet_1, 10)  # saves value 100 with the topic db.suppply_voltage_single

        db.insert_value(db.Sens_ea1_magnet_1, 1050)

        # reads the last value from db with topic 1 or db.supply_voltage_single
        Logger.get_instance().log(LogLevel.INFO, "Database", db.get_last_value("1"))

        db.insert_value(db.Sens_ea1_magnet_2, 1050)

        # reads the last value from db with topic 1 or db.supply_voltage_single
        Logger.get_instance().log(LogLevel.INFO, "Database", db.get_last_value("102"))

        Logger.get_instance().log(LogLevel.INFO, "Database", db.get_last_value(db.Sens_ea1_magnet_1))
