'''##################################################
# PAPELL
# first created by Moritz Sauer
# modified by Robin Schweigert
# successfully tested Date
#
# This should check for the availability ov every single component
# and plot a list at the end, that will make it easy for non sotware members
# to verify, wether some component is available or not.
#
# at every component there has to be instanstiation, and method call
#
##################################################'''


# store first states
states = {}
states["fuelgauges"] = True
states["valve1"] = True
states["valve2"] = True
states["valve3"] = True
states["valve4"] = True
states["pump1"] = True
states["pump2"] = True
states["motor1"] = True
states["motor2"] = True
states["ea1"] = True
states["ea2"] = True
states["ea1_current"] = True
states["ea2_current"] = True
states["accelero"] = True
states["mag1"] = True
states["mag2"] = True
states["mag3"] = True
states["mag4"] = True
states["actuator_current"] = True
states["sensor_temp"] = True
states["obc_temp"] = True

temp_psu = None
from state_manager.initialcheck import i2c_action

try:
    from psu.PSUTest import PSUTest
    temp_psu = PSUTest()
    temp_psu.connect_batteries()
    temp_psu.connect_12v_powerline()
    temp_psu.connect_vext()
except Exception as e:
    print(e)
    states["fuelgauges"] = False

# fuelgauge
try:
    if temp_psu is not None:
        states["fuelgauges"] = temp_psu.check_status() and temp_psu.fg_value_check() and temp_psu.temp_check()
    else:
        states["fuelgauges"] = False
except Exception as e:
    print(e)
    states["fuelgauges"] = False
"""
# valve 1
try:
    from helpers.PCA9557 import PCA9557
    from state_manager.initialcheck import i2c_action
    from aktuators.valve import Valve
    expander_1 = PCA9557(i2c_action.gpio_expander1)
    valve_1 = Valve(expander_1, 7)
    valve_1.open_for(2)
except Exception as e:
    print(e)
    states["valve1"] = False

# vavle 2
try:
    from helpers.PCA9557 import PCA9557
    from state_manager.initialcheck import i2c_action
    from aktuators.valve import Valve

    expander_1 = PCA9557(i2c_action.gpio_expander1)
    valve_2 = Valve(expander_1, 6)
    valve_2.open_for(2)
except Exception as e:
    print(e)
    states["valve2"] = False
print("flag between Valve 2-3")
# valve 3
try:
    from helpers.PCA9557 import PCA9557
    from state_manager.initialcheck import i2c_action
    from aktuators.valve import Valve

    expander_2 = PCA9557(i2c_action.gpio_expander2)
    valve_3 = Valve(expander_2, 7)
    valve_3.open_for(2)
except Exception as e:
    print(e)
    states["valve3"] = False

# valve 4
try:
    from helpers.PCA9557 import PCA9557
    from state_manager.initialcheck import i2c_action
    from aktuators.valve import Valve

    expander_2 = PCA9557(i2c_action.gpio_expander1)
    valve_4 = Valve(expander_2, 6)
    valve_4.open_for(2)
except Exception as e:
    print(e)
    states["valve4"] = False

# pump 1
try:
    from helpers.PCA9557 import PCA9557
    from state_manager.initialcheck import i2c_action
    from aktuators.pump import Pump

    expander_1 = PCA9557(i2c_action.gpio_expander1)
    pump_1 = Pump(expander_1, 1)
    pump_1.run_for(2)
except Exception as e:
    print(e)
    states["pump1"] = False
"""
# pump 2
try:
    from helpers.PCA9557 import PCA9557
    from state_manager.initialcheck import i2c_action
    from aktuators.pump import Pump

    expander_2 = PCA9557(i2c_action.gpio_expander2)
    pump_2 = Pump(expander_2, 1)
    pump_2.run_for(2)
except Exception as e:
    print(e)
    states["pump2"] = False

print("flag between pump-servo")
# servo 1
try:
    from helpers.DRV8834 import DRV8834
    from helpers.PCA9557 import PCA9557

    expander_1 = PCA9557(i2c_action.gpio_expander1)
    motor_1 = DRV8834(expander_1)
    motor_1.set_dir(DRV8834.DIR_CW)
    motor_1.step(20)
    motor_1.set_dir(DRV8834.DIR_CCW)
    motor_1.step(20)
except Exception as e:
    print(e)
    states["motor1"] = False

# servo 2
try:
    from helpers.DRV8834 import DRV8834
    from helpers.PCA9557 import PCA9557

    expander_2 = PCA9557(i2c_action.gpio_expander2)
    motor_2 = DRV8834(expander_2)
    motor_2.set_dir(DRV8834.DIR_CW)
    motor_2.step(20)
    motor_2.set_dir(DRV8834.DIR_CCW)
    motor_2.step(20)
except Exception as e:
    print(e)
    states["motor2"] = False
print("flag between servo-ea")

# ea 1

try:
    from sensors.current_sensor import Current
    from state_manager.Database import Database
    from magnets.magnets import Magnets
    import time

    Magn = Magnets(1)
    current_ea1 = [Current(address=i2c_action.Addr_ea1_adc1), Current(address=i2c_action.Addr_ea1_adc2)]
    db = Database.get_instance()

    for j in range(1, 36):
        Magn.switch_on(j)
        time.sleep(2)
        for i in range(1, 8):
            db.insert_value(Database.Sens_ea1_current_1[i], current_ea1[0].read(i))
            db.get_last_value(Database.Sens_ea1_current_1[i])
            db.insert_value(Database.Sens_ea1_current_2[i], current_ea1[1].read(i))
            db.get_last_value(Database.Sens_ea1_current_2[i])
        time.sleep(2)
        Magn.disable(j)
        print("break before after magnet %s" % j)
except Exception as e:
    print(e)
    states["ea1"] = False

# ea 2
try:
    from sensors.current_sensor import Current

    current_ea2 = [Current(address=i2c_action.Addr_ea2_adc1), Current(address=i2c_action.Addr_ea2_adc2)]

    for i in range(1, 8):
        db.insert_value(Database.Sens_ea1_current_2[i], current_ea1[0].read(i))

    for j in range(1, 36):
        Magn.switch_on(j)
        time.sleep(2)
        for i in range(1, 8):
            db.insert_value(Database.Sens_ea2_current_1[i], current_ea2[0].read(i))
            db.get_last_value(Database.Sens_ea2_current_1[i])
            db.insert_value(Database.Sens_ea2_current_2[i], current_ea2[1].read(i))
            db.get_last_value(Database.Sens_ea2_current_2[i])
        time.sleep(2)
        Magn.disable(j)
        print("break before after magnet %s" % j)
except Exception as e:
    print(e)
    states["ea2"] = False

# current sensor ea 1
try:
    from sensors.current_sensor import Current
    temp_curent_1 = Current(i2c_action.Addr_ea1_adc1)
    temp_curent_2 = Current(i2c_action.Addr_ea1_adc2)

    temp_curent_1.read(2)  # ToDo: not ready
except Exception as e:
    print(e)
    states["ea1_current"] = False

# current sensor ea 2
try:
    pass
except Exception as e:
    print(e)
    states["ea2_current"] = False

# accelerometer
try:
    from sensors.accelerometer_adxl335 import Accelerometer
    import time
    temp_accel = Accelerometer()
    for i in range(0, 10):
        temp_accel.read()
        time.sleep(0.5)
except Exception as e:
    print(e)
    states["accelero"] = False

# magnetometer 1
try:
    from sensors.magnetometer_mag3110 import LIS3MDL
    from helpers.i2c_switch import I2C_Switch
    import time

    temp_switch = I2C_Switch()
    temp_mag = LIS3MDL(temp_switch, 0)
    for i in range(0, 10):
        temp_mag.get_mag()
except Exception as e:
    print(e)
    states["mag1"] = False

# magnetometer 2
try:
    from sensors.magnetometer_mag3110 import LIS3MDL
    from helpers.i2c_switch import I2C_Switch
    import time

    temp_switch = I2C_Switch()
    temp_mag = LIS3MDL(temp_switch, 1)
    for i in range(0, 10):
        temp_mag.get_mag()
except Exception as e:
    print(e)
    states["mag2"] = False

# magnetometer 3
try:
    from sensors.magnetometer_mag3110 import LIS3MDL
    from helpers.i2c_switch import I2C_Switch
    import time

    temp_switch = I2C_Switch()
    temp_mag = LIS3MDL(temp_switch, 2)
    for i in range(0, 10):
        temp_mag.get_mag()
except Exception as e:
    print(e)
    states["mag3"] = False

# magnetometer 4
try:
    from sensors.magnetometer_mag3110 import LIS3MDL
    from helpers.i2c_switch import I2C_Switch
    import time

    temp_switch = I2C_Switch()
    temp_mag = LIS3MDL(temp_switch, 3)
    for i in range(0, 10):
        temp_mag.get_mag()
except Exception as e:
    print(e)
    states["mag4"] = False

# current sensor actuator board
try:
    pass
except Exception as e:
    print(e)
    states["actuator_current"] = False

# temperature sensor sensorboard
try:
    from sensors.temperature_mcp9808 import Temperature

    temp_sensor = Temperature(address=i2c_action.Addr_TempSens_SensBoard)  # 0x1F)

    print("Temperature Sensorboard: %s" % temp_sensor)
except Exception as e:
    print(e)
    states["sensor_temp"] = False

# temperature sensor obc
try:
    from sensors.temperature_mcp9808 import Temperature

    temp_temp = Temperature(address=i2c_action.Addr_TempSens_obc)  # 0x1B)
    print("Temperature PSU: %s" % temp_temp)
except Exception as e:
    print(e)
    states["obc_temp"] = False


# final report:
for i in range(0, 5):
    print("")
print("Final Report:")
for element in states.keys():
    if states[element]:
        print("%s: working" % element)
    else:
        print("%s: broken" % element)

temp_psu.disconnect_12v_powerline()
temp_psu.disconnect_vext()
temp_psu.disconnect_batteries()
print("ende alles")