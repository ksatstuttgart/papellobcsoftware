#imports

from .SensorArray import SensorArray
from .Logger import Logger
from .MagnetArray import MagnetArray
from .Pump import Pump
from .Vents import Valve
from .BatTests import PSUTest

class Preset1():

    def __init__(self):
        self.valve = Valve()
        self.Pump = Pump()
        self.PSU = PSUTest()
        self.magnet_array = MagnetArray(1)

    def run():
        #Run pump 1 for 5 seconds
        Pump().time_pump(1,5)
        Pump().stop_pump(1)
        #see documentation for required inputs

        # open valve
        valve_status = input("Valve Status: ").split()
        valve_status = "vo"
        Safe_Mode = input("SafeMode: ")
        Safe_Mode = int(Safe_Mode)

        self.valve.valve_management(valve_status, Safe_Mode)
        #close valve
        Valve().valve.valve_close()

        #Run Array preset 1
        # MA1 Job:
        Logger.log(Logger.LOG, "Turning on one Magnet for Septembertest")
        self.magnet_array.shift_register.set_magnet(2,1)
