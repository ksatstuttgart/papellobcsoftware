from helpers.PCF8574 import PCF8574
from state_manager.Logger import Logger


#import RPi.GPIO as GPIO
# ToDo: port expander implementieren

class Valve:


    expander1 = PCF8574(address=0x38)  # define i2c address
    expander2 = PCF8574(address=0x39)  # define i2c address

    for i in range(0, 17):
        # expander.setup(expandernummer, status)
        expander1.setup(i, expander1.OUTPUT)
        expander2.setup(i, expander2.OUTPUT)

    def valve_io(self, valve, status):
        """
        port allocation with validation and GPIO setup and output
        :param valve: valve number (see in schematic)
        :param status: valve control
        :return:
        """
        if valve == "v1":
            pin = 22
            detect = True
        elif valve == "v2":
            pin = 23
            detect = True
        elif valve == "v3":
            pin = 24
            detect = True
        elif valve == "v4":
            pin = 25
            detect = True
        else:
            detect = False
            Logger.log(Logger.WARNING, "Valve: Unknown valve")
            print("Valve: Unknown valve")

        if detect is True:
            pin = int(pin)
            #GPIO.setmode(GPIO.BCM)
            #GPIO.setup(port, GPIO.OUT)
            if status == 1:
                #GPIO.OUTPUT(port, GPIO.HIGH)
                #Logger.log(Logger.LOG, "Status Valve %s: 1" % valve)
                expander.output(pin, status)
                print("Status Valve %s: 1" % valve)
            elif status == 0:
                #GPIO.OUTPUT(port, GPIO.LOW)
                #Logger.log(Logger.LOG, "Status Valve %s: 0" % valve)
                expander.output(pin, status)
                print("Status Valve %s: 0" % valve)
            else:
                #Logger.log(Logger.LOG, "Valve: Unknown Status Input")
                print("Valve: Unknown Status Input")



    #           T1              T2
    #           |               |
    #           |               |
    #           P1              P2
    #           |               |
    #           |------------   |
    #           |    -------x---|
    #           |    |      |   |
    #           V1   V3     V4  V2
    #           |    |      |   |
    #            EXP1        EXP2


    def valve_management(self, valve_status, Safe_Mode):  # function name, which exp. area, pump, tank

        """
        control of all valves
        :param valve_status: valve status
        :param Safe_Mode: SafeMode
        :return:
        """

        if valve_status == "n1" and Safe_Mode == 0:
            case = "norm_1"
        elif valve_status == "n2" and Safe_Mode == 0:
            case = "norm_2"
        elif valve_status == "c1" and Safe_Mode == 0:
            case = "cont_1"
        elif valve_status == "c2" and Safe_Mode == 0:
            case = "cont_2"
        elif valve_status == "close" and Safe_Mode == 0:
            case = "Close"
        elif Safe_Mode is 1:
            case = "Safe_Mode"
        else:
            case = "close"
            #Logger.log(Logger.LOG, "Valve: Unknown Input")
            print("Valve: Unknown Input")

        if case == "norm_1":  # normal 1: t1 --> p1 --> v1 --> e1
            try:
                self.valve_io("v1", 1)
                self.valve_io("v2", 0)
                self.valve_io("v3", 0)
                self.valve_io("v4", 0)
                #Logger.log(Logger.LOG, "Valve: Normal 1")
                print("Valve: Normal 1")
            except:
                #Logger.log(Logger.WARNING, "Valve Normal 1 failed")
                print("Valve Normal 1 failed")
        elif case == "norm_2":  # normal 2: t2 --> p2 --> v2 --> e2
            try:
                self.valve_io("v1", 0)
                self.valve_io("v2", 1)
                self.valve_io("v3", 0)
                self.valve_io("v4", 0)
                #Logger.log(Logger.LOG, "Valve: Normal 2")
                print("Valve: Normal 2")
            except:
                #Logger.log(Logger.WARNING, "Valve: Normal 2 failed")
                print("Valve: Normal 2 failed")
        elif case == "cont_1":  # contingency 1: t2 --> p2 --> v3 --> e1 (p1 failure)
            try:
                self.valve_io("v1", 0)
                self.valve_io("v2", 0)
                self.valve_io("v3", 1)
                self.valve_io("v4", 0)
                #Logger.log(Logger.LOG, "Valve: Contingency 1")
                print("Valve: Contingency 1")
            except:
                #Logger.log(Logger.WARNING, "Valve: Contingency 1 failed")
                print("Valve: Contingency 1 failed")
        elif case == "cont_2":  # contingency 2: t1 --> p1 --> v4 --> e2 (p2 failure)
            try:
                self.valve_io("v1", 0)
                self.valve_io("v2", 0)
                self.valve_io("v3", 0)
                self.valve_io("v4", 1)
                #Logger.log(Logger.LOG, "Valve: Contingency 2")
                print("Valve: Contingency 2")
            except:
                #Logger.log(Logger.WARNING, "Valve: Contingency 2 failed")
                print("Valve: Contingency 2 failed")
        elif case == "Close":
            try:
                self.valve_io("v1", 0)
                self.valve_io("v2", 0)
                self.valve_io("v3", 0)
                self.valve_io("v4", 0)
                #Logger.log(Logger.LOG, "Valve: Close")
                print("Valve: Close")
            except:
                #Logger.log(Logger.WARNING, "Valve: Close failed")
                print("Valve: Close failed")
        elif case == "Safe_Mode":
            try:
                self.valve_io("v1", 0)
                self.valve_io("v2", 0)
                self.valve_io("v3", 0)
                self.valve_io("v4", 0)
                #Logger.log(Logger.LOG, "Valve: Safe Mode")
                print("Valve: Safe Mode")
            except:
                #Logger.log(Logger.WARNING, "Valve: Safe Mode failed")
                print("Valve: Safe Mode failed")
        else:
            #Logger.log(Logger.LOG, "Case not found.")
            print("Case not found.")

    def __init__(self, Safe_Mode):

        #Logger.log(Logger.LOG, "Valve: Safe Mode is %s" % Safe_Mode)
        print("Valve: Safe Mode is %s" % Safe_Mode)

