##################################################
# PAPELL
# first created by Jan-Erik Brune
# modified by "name"
# successfully tested Date 21.12.2018 by Jan-Erik Brune
#
# case definitions for fluid transfer
#
##################################################

from aktuators.pump import Pump
from helpers.PCF8574 import PCF8574
from aktuators.valve import Valve


class ValvePump:

    def __init__(self):
        '''
        Pin 0,1,2 on Expander 1,2 
        change Pins if wrong Valves
        :param expander1:
        :param expander2:
        '''
        self.expander1 = PCF8574(address=0x38)
        self.expander2 = PCF8574(address=0x39)

        #for i in range(17):
        #    self.expander1.setup(i, PCF8574.OUT)
        #    self.expander2.setup(i, PCF5874.OUT)

        self.valve1 = Valve(self.expander1, 1)
        self.valve2 = Valve(self.expander1, 2)
        self.valve3 = Valve(self.expander2, 1)
        self.valve4 = Valve(self.expander2, 2)
        self.pump1 = Pump(self.expander1, 0)
        self.pump2 = Pump(self.expander2, 0)
        self.t = 5
    '''    
           T1              T2
            |               |
            |               |
           P1              P2
           |               |
           |------------   |
           |    -------x---|
           |    |      |   |
           V1   V3     V4  V2
           |    |      |   |
            EXP1        EXP2
    '''

    def close_stop(self):
        try:
            #Logger.get_instance().log(LogLevel.INFO, "Transfer", "All valves closed and all Pumps closing")
            print("Transfer: All valves closed and all Pumps closing")

            self.valve1.close()
            self.valve2.close()
            self.valve3.close()
            self.valve4.close()
            self.pump1.stop()
            self.pump2.stop()
            #Logger.get_instance().log(LogLevel.INFO, "Transfer", "All valves closed and all Pumps closing")
            print("Transfer: All Valves closed and all Pumps stopped")

        except:
            print("Transfer: CloseStop canceled")

    def normal1(self,t):
        try:
            print("Transfer: Normal1 began")

            self.valve1.open()
            self.valve2.close()
            self.valve3.close()
            self.valve4.close()
            self.pump1.run_for(t)
            self.pump2.stop()
            self.close_stop()
            print("Transfer: Normal1 ended")

        except:
            print("Transfer: Normal1 canceled")

    def normal2(self,t):
        try:
            print("Transfer: Normal2 began")

            self.valve1.close()
            self.valve2.open()
            self.valve3.close()
            self.valve4.close()
            self.pump1.stop()
            self.pump2.run_for(t)
            self.close_stop()
            print("Transfer: Normal2 ended")

        except:
            print("Transfer: Normal2 canceled")

    def cont1(self,t):
        try:
            print("Transfer: Cont1 began")

            self.valve1.close()
            self.valve2.close()
            self.valve3.open()
            self.valve4.close()
            self.pump1.stop()
            self.pump2.run_for(t)
            self.close_stop()
            print("Transfer: Cont1 ended")

        except:
            print("Transfer: Cont1 canceled")

    def cont2(self,t):
        try:
            print("Transfer: Cont2 began")

            self.valve1.close()
            self.valve2.close()
            self.valve3.open()
            self.valve4.close()
            self.pump1.stop()
            self.pump2.run_for(t)
            self.close_stop()
            print("Transfer: Cont2 ended")

        except:
            print("Transfer: Cont2 canceled")

    def safe_state(self):
        try:
            print("Transfer: SAFE STATE initiated")

            self.valve1.close()
            self.valve2.close()
            self.valve3.close()
            self.valve4.close()
            self.pump1.stop()
            self.pump2.stop()
            print("Transfer: SAFE STATE completed")

        except:
            print("Transfer: unable to activate SAFE STATE")
