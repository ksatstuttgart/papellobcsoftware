'''##################################################
# PAPELL
# first created by Tobias Ott
# modified by Moritz Sauer
# successfully tested "Date xx.xx.xxxx" by "name"
#
# Hardware level use of accelerometer
#
##################################################'''

import math

import Adafruit_ADS1x15 as ADS1x15 #TODO check if this is required

from state_manager.Logger import Logger

class Accelerometer:

    def __init__(self, address=0x48):
        """Initialize a Accelerometer Sensor ADC on the specified I2C address."""

        self.acc = ADS1x15.ADS1115(address)
        self.cal_m = [0, 0, 0]
        self.cal_c = [0, 0, 0]

    def calibrate(self):
        """For Calibration of Sensor in G environment"""

        axis =['x', 'y', 'z']
        print("---Accelerometer calibration---")
        for i in range(0, 3):
            input("positive %s-Axis Down. Type sth. when ready:" % axis[i])
            A = self.read_raw()[i]
            input("negative %s-Axis Down. Type sth. when ready:" % axis[i])
            B = self.read_raw()[i]
            self.cal_m[i] = 2. / (A - B)
            self.cal_c[i] = 1 - (2. * A) / (A - B)

    def read_raw(self):
        return [self.acc.read_adc(0, gain=1), self.acc.read_adc(1, gain=1), self.acc.read_adc(2, gain=1)]

    def read(self):
        """
        :return: Returns the Acceleration in G, in case of an error, returns None
        """
        try:
            return [i*j+k for i, j, k in zip(self.read_raw(), self.cal_m, self.cal_c)]
        except:
            Logger.log(Logger.ERROR, "Acceleration sensor not found.")
            return None


class Sound:

    # Different usable resistors for R17 TODO
    RES1000K = 0
    RES470K = 1
    RES220K = 2
    RES100K = 3
    RES47K = 4
    RES22K = 5
    RES10K = 6
    RES4700 = 7
    RES2200 = 8

    def __init__(self, address=0x48, r3_mounted=True, r17_val=None):
        """Initialize a Sound Sensor ADC on the specified I2C address."""

        self.sound = ADS1x15.ADS1115(address)
        self.r3_mounted = r3_mounted
        self.r17_val = r17_val

    def read_raw(self):
        """
        Returns the raw data of the sound sensor as a list
        :return:
        """
        try:
            return [self.sound.read_adc(0, gain=1), self.sound.read_adc(1, gain=1), self.sound.read_adc(2, gain=1)]
        except:
            Logger.log(Logger.WARNING, "Could not read from sound sensor")

    def read_amp(self):
        """
        Returns the measured amplitude
        :return:
        """
        if self.r17_val is None:
            return 20*math.log(self.read_raw()[1], 10)

        return None # TODO

    def detect_sound(self):
        """
        Returns true if there is any sound
        :return:
        """

        if self.sound.read_raw()[2] > 1000:
            return True

        return False
