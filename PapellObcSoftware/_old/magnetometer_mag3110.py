'''##################################################
# PAPELL
# first created by Tobias Ott
# modified by Moritz Sauer
# successfully tested "Date xx.xx.xxxx" by "name"
#
# Hardware level use of accelerometer
#
##################################################'''

from helpers.LIS3MDL import LIS3MDL as raw_mag
from state_manager.Logger import Logger


class Magnetometer:

    def __init__(self, address=0x1e, bus_id=1):
        """
        Initialize a Magnetometer on the specified I2C address and bus id.
        """
        self.address = address
        try:
            self.mag_sensor = raw_mag(bus_id, address)
            self.mag_sensor.enableLIS()
        except:
            self.mag_sensor = None
            Logger.log(Logger.ERROR, "Magnetometer at address %s not found." % address)

    def read_raw(self):
        """
        :return: Returns a 3-dimensional vector (list) of raw magnetometer data or
        in case of an error, returns None
        """
        try:
            return self.mag_sensor.getMagnetometerRaw()
        except:
            Logger.log(Logger.ERROR, "Magnetometer at address %s not found." % self.address)
            return None

    def read(self):
        """
        :return: Returns a 3-dimensional vector (list) of magnetometer data or
        in case of an error, returns None
        """
        try:
            return self.mag_sensor.get_mag()
        except:
            Logger.log(Logger.ERROR, "Magnetometer at address %s not found." % self.address)
            return None