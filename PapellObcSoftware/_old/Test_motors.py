'''##################################################
# PAPELL
# first created by Martin Siedorf
# modified by chris
# successfully tested :
#
#
#
##################################################'''


import time

import helpers.PCF8574 as expand

expander = expand(address=0x38)  # i2c address
expander2 = expand(address=0x39) # i2c address

for i in range(0, 16):
    """set all expander to output. (don't know why, just do it"""
    expander.setup(i, expander.OUTPUT)
    expander2.setup(i, expander.OUTPUT)

while True:
    for i in range(0, 8):
        expander.output(i, i == cur)
    cur = (cur + 1) % 8
    time.sleep(0.2)

