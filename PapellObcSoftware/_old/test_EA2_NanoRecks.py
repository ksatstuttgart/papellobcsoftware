import time

from helpers.ShiftRegister import *
from state_manager.Logger import Logger, LogLevel


class NanoRacksTest:

    def __init__(self):
        pass

    def move_single_magnets(self):
        shiftreg = ShiftRegister(40, 6, 13, 19, 26)
        for i in range(40):
            shiftreg.output(i, ShiftRegister.LOW)
        input("press to start")
        try:
            for i in range(40):
                shiftreg.output(i, ShiftRegister.HIGH)
                input("next: %s" % i)
                shiftreg.output(i, ShiftRegister.LOW)
        except Exception as e:
            Logger.get_instance().log(LogLevel.ERROR, "StateManager", "Exception occured: " + str(e))
            shiftreg.clear_all()

    def all_on(self):
        shiftreg = ShiftRegister(40, 6, 13, 19, 26)
        for i in range(40):
            shiftreg.output(i, ShiftRegister.HIGH)
        input("press to stop")
        for i in range(40):
            shiftreg.output(i, ShiftRegister.LOW)

    def twelve_on(self):
        shiftreg = ShiftRegister(40, 6, 13, 19, 26)
        for i in range(40):
            shiftreg.output(i, ShiftRegister.LOW)
        for i in range(23):
            shiftreg.output(i, ShiftRegister.HIGH)
        input("press to stop")
        for i in range(40):
            shiftreg.output(i, ShiftRegister.LOW)

    def increasing_on(self):
        shiftreg = ShiftRegister(40, 6, 13, 19, 26)
        for i in range(40):
            shiftreg.output(i, ShiftRegister.LOW)
        for i in range(40):
            input("currently: %s" % i)
            shiftreg.output(i, ShiftRegister.HIGH)
        input("press to stop")
        for i in range(40):
            shiftreg.output(i, ShiftRegister.LOW)



if __name__ == '__main__':

    test = NanoRacksTest()

    #test.move_single_magnets()
    #test.all_on()
    #test.twelve_on()
    test.increasing_on()

