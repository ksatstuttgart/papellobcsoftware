from helpers.BMP280 import BMP280 as raw_pres
from helpers.LIS3MDL import LIS3MDL as raw_mag
from helpers.MCP9808 import MCP9808 as raw_temp
from state_manager.Logger import Logger


class Temperature:

    def __init__(self, address=0x18, **kwargs):
        """

        Initialize a Temperature Sensor on the specified I2C address and bus number.

        """
        try:
            if address is None:
                self.temp_sensor = raw_temp()
            else:
                self.temp_sensor = raw_temp(address, **kwargs)
            if self.temp_sensor.begin() is False:
                Logger.log(Logger.WARNING, "Could not initialize Temperature Sensor.")
        except:
            self.temp_sensor = None
            Logger.log(Logger.ERROR, "Temperature sensor not found.")

    def read(self):
        """
        :return: Returns the Temperature, in case of an error, returns None
        """
        try:
            return self.temp_sensor.readTempC()
        except:
            Logger.log(Logger.ERROR, "Temperature sensor not found.")
            return None


class Magnetometer:

    def __init__(self, address=0x1e, bus_id=1):
        """
        Initialize a Magnetometer on the specified I2C address and bus id.
        """
        self.address = address
        try:
            self.mag_sensor = raw_mag(bus_id, address)
            self.mag_sensor.enableLIS()
        except:
            self.mag_sensor = None
            Logger.log(Logger.ERROR, "Magnetometer at address %s not found." % address)

    def read_raw(self):
        """
        :return: Returns a 3-dimensional vector (list) of raw magnetometer data or
        in case of an error, returns None
        """
        try:
            return self.mag_sensor.getMagnetometerRaw()
        except:
            Logger.log(Logger.ERROR, "Magnetometer at address %s not found." % self.address)
            return None

    def read(self):
        """
        :return: Returns a 3-dimensional vector (list) of magnetometer data or
        in case of an error, returns None
        """
        try:
            return self.mag_sensor.get_mag()
        except:
            Logger.log(Logger.ERROR, "Magnetometer at address %s not found." % self.address)
            return None


class Pressure:

    def __init__(self, address=None, **kwargs):
        """
        Initialize a Pressure Sensor on the specified I2C address.
        """
        try:
            if address is None:
                self.pressure_sensor = raw_pres()
            else:
                self.pressure_sensor = raw_pres(adress=address, kwargs=kwargs)
        except:
            self.pressure_sensor = None
            Logger.log(Logger.ERROR, "Temperature sensor not found.")

    def read(self):
        """
        :return: Returns the Pressure, in case of an error, returns None
        """
        try:
            return self.pressure_sensor.read_pressure()
        except:
            Logger.log(Logger.ERROR, "Temperature sensor not found.")
            return None
