##################################################
# PAPELL
# first created by "Jan-Erik Brune"
# modified by "name"
# successfully tested "Date 21.01.2018" by "Jan-Erik Brune"
#
# outdated version of Transfer.py
#
##################################################

from old.Valves_OLD import Valve


class Transfer:

    def operation(self, status, Safe_Mode):
        '''
        combination of valves and pumps
        :param status:
        :param Safe_Mode:
        :return:
        '''
        valve = Valve(Safe_Mode)
        try:
            if status == 'n1' and Safe_Mode == 0:
                pin = 1
            elif status == 'n2' and Safe_Mode == 0:
                pin = 2
            elif status == 'c1' and Safe_Mode == 0:
                pin = 2
            elif status == 'c2' and Safe_Mode == 0:
                pin = 1

            if Safe_Mode == 1:
                pump1 = Pump(1)
                pump2 = Pump(2)
                pump1.stop()
                pump2.stop()
                valve.valve_management(status, Safe_Mode)
            else:
                pump = Pump(pin)
                pump.stop()
                pump = Pump(pin)
                t = 2
                valve.valve_management(status, Safe_Mode)
                pump.run_for(t)
                status = 'close'
                valve.valve_management(status, Safe_Mode)
        except:
            pump1 = Pump(1)
            pump2 = Pump(2)
            pump1.stop()
            pump2.stop()
            print("Input not recognized")
            valve.valve_management(status, 0)

if __name__ == '__main__':
    SafeMode = 0
    status = 'c5'

    transfer = Transfer()
    transfer.operation(status, SafeMode)
    #Logger.log(Logger.INFO, "DONE")
    #Logger.log(Logger.INFO, "Used Current for Charging" % (psu._Battery.getCurrent()))