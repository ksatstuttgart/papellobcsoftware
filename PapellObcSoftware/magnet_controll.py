##################################################
# PAPELL
# first created by Robin Schweigert
# modified by --
# successfully tested --
#
# This class implements a low level magnet testing
# function. For this purpose every magnet can be
# controlled individually.
#
##################################################
from helpers.ShiftRegister import ShiftRegister
import sys
import os
import time


# old
#sr = ShiftRegister(40, 6, 13, 19, 26)

# ea 1
sr = ShiftRegister(40, 8, 23, 25, 24)

# ea 2
#sr = ShiftRegister(40, 7, 20, 12, 21)
time.sleep(1)
for i in range(0, 40):
    sr.output(i, ShiftRegister.LOW)
while True:
    inp = input("enter magnet and state: ")
    if inp == "q":
        break
    elif inp == "a":
        for i in range(0, 40):
            sr.output(i, 1)
    elif inp == "o":
        for i in range(0, 40):
            sr.output(i, 0)
    elif len(inp.split(" ")):
        mag = int(inp.split(" ")[0])
        state = int(inp.split(" ")[1])
        if state == 1:
            sr.output(mag, ShiftRegister.HIGH)
            print("setting magnet %s to HIGH" % mag)
        else:
            sr.output(mag, ShiftRegister.LOW)
            print("setting magnet %s to LOW" % mag)
    else:
        print("please enter magnet and state or q to quit")

print("powering off all magnets")
for i in range(0, 40):
    sr.output(i, ShiftRegister.LOW)
