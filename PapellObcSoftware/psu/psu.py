#!/usr/bin/env python3
# -*- coding: utf-8 -*-
##################################################
# PAPELL
# first created by "chris"
# successfully tested "Date xx.xx.xxxx" by "name"
#
# PSU
#
##################################################

#####                           ##            #####                ###########        ##                           ##                                       ##
##      ###                  ####          ##     ###           ###########       ##                           ##                                       ##
##         ###             ##    ##        ##         ###       ##                              ##                            ##                                       ##
##      ###              ##       ##       ##      ###          ##                              ##                            ##                                       ##
####                     #######        ####                   ###########      ##                            ##                                      ##
##                        ##            ##       ##                        ##                              ##                            ##                                      ##
##                      ##               ##      ##                        ##                              ##                            ##
##                    ##                 ##      ##                        ###########      ###########    ###########              ##
##                   ##                   ##     ##                        ###########      ###########    ###########              ##

from psu import PSUTest as PSUT
from multiprocessing import Process, Value
import time
import config.configurable_variables as cv
import numpy as np
from helpers.Database import QueuedDatabase




class PSU:
    active_check_loop = Value("b", True)
    active_log_loop = Value("b", True)

    def __init__(self, state_manager):
        self._state_manager = state_manager
        self.psu = PSUT.PSUTest(self._state_manager)
        self.log_framerate = cv.PSU_LOGGING_FRAMERATE
        self.checking_framerate = cv.PSU_CHECKING_FRAMERATE
        self.prio2 = cv.PSU_CHECKING_prio2
        self.prio3 = cv.PSU_CHECKING_prio3
        self.cprio2 = 0
        self.cprio3 = 0
        self.CHARGING = False
        self.broken_battery_flag = 0
        #self.queue = QueuedDatabase("Database/Database_of_psu")
        #self.queue.start()

        self.start_logging()


        self.initialize()

        self.start_checking()


    def SOC(self):
        """State of Charge
        """
        soc = []
        for i in range(len(self.psu.BATTERIES)):
            for bat in self.psu.BATTERIES:
                if bat.get_state_of_charge() != -1:
                    soc.append(bat.get_state_of_charge())
        return np.mean(soc)
    #maybe voltage check ? additional security, but additional risks for false negatives

    def start_logging(self):
        PSU.active_log_loop.value = True
        self.logging_thread = Process(target=self.logging_loop, args=([self.log_framerate]))
        self._state_manager.threads.append(self.logging_thread)
        self.logging_thread.start()

    def stop_logging(self):
        self.psu.info_log("", "stopped database logging.")
        PSU.active_log_loop.value = False
        self.logging_thread.join(10)
        try:
            self._state_manager.remove(self.logging_thread)
        except Exception as e:
            self.psu.info_log("", "cannot remove Logging loop")

    def start_checking(self):
        PSU.active_check_loop.value = False
        self.checking_thread = Process(target=self.checking_loop, args=([self.checking_framerate]))
        self._state_manager.threads.append(self.checking_thread)
        self.checking_thread.start()

    def stop_checking(self):
        self.psu.info_log("", "stopped checking loop.")
        PSU.active_check_loop.value = False
        self.checking_thread.join(10)
        try:
            self._state_manager.remove(self.checking_thread)
        except Exception as e:
            self.psu.info_log("", "cannot remove checkingloop")

    def current_time(self):
        """
        :returns the current time in milliseconds
        """
        return int(round(time.time() * 1000))

    def logging_loop(self, framerate): # aus der config auch die checks
        self.psu.info_log("", "started database logging")
        self.active_log_loop = 1
        while PSU.active_log_loop.value:
            self.last_execution = self.current_time()
            # execution block
            # self.check()
            self.log()
            # end of execution block
            # checking execution time
            if self.current_time() - self.last_execution < 1 / framerate * 1000:
                time.sleep(((1 / framerate * 1000) - (self.current_time() - self.last_execution)) / 1000)
            else:
                self.psu.warn_log("", "Can't reach target framerate, current framerate: %s" % (
                                              1 / (self.current_time() - self.last_execution) * 1000))

    def checking_loop(self, framerate):
        """
        checks if any values are out of range #ToDo
        :param framerate:
        :return:
        """
        self.psu.info_log("", "started checking loop")
        self.active_check_loop = 1
        while PSU.active_check_loop.value:
            self.last_execution = self.current_time()
            # execution block
            # self.check()
            self.check()
            # end of execution block
            # checking execution time
            if self.current_time() - self.last_execution < 1 / framerate * 1000:
                time.sleep(((1 / framerate * 1000) - (self.current_time() - self.last_execution)) / 1000)
            else:
                self.psu.warn_log("", "Can't reach target framerate, current framerate: %s" % (
                        1 / (self.current_time() - self.last_execution) * 1000))

    def repair_battery(self):
        pass
    # needs to be cleared with team leaders (possibly state manager try and counter or self repair (but state manager waits anyway with safemode)
    # problems with reenabling batteries, different voltages...

    def log(self):
        db_data_names = ["time", "mode", "SOC", ]
        data = [time.time(), 1, self.SOC()]

        self.cprio2 = self.cprio2 + 1
        self.cprio3 = self.cprio3 + 1

        for battery in self.psu.BATTERIES:

            data.append(str(battery.get_voltage()))
            db_data_names.append(str(battery.__str__() + "_Voltage"))

            data.append(str(battery.get_current()))
            db_data_names.append(str(battery.__str__() + "_Current"))



            if self.cprio2 == self.prio2:
                data.append(str(battery.get_temperature()))
                db_data_names.append(str(battery.__str__() + "_Temperature"))

                data.append(str(battery.get_internal_temperature()))
                db_data_names.append(str(battery.__str__() + "_internal_Temperature"))

                data.append(str(battery.get_state_of_charge()))
                db_data_names.append(str(battery.__str__() + "_SOC"))


            if self.cprio3 == 1:
                data.append(str(battery.get_capacity()))
                db_data_names.append(str(battery.__str__() + "_current_Capacity"))
            elif self.cprio3 == 2:
                data.append(str(battery.get_full_capacity()))
                db_data_names.append(str(battery.__str__() + "_full_capacity"))
            elif self.cprio3 == 3:
                data.append(str(battery.get_power()))
                db_data_names.append(str(battery.__str__() + "_Power_Usage"))
            elif self.cprio3 == 4:
                data.append(str(battery.get_flagsa()))
                db_data_names.append(str(battery.__str__() + "_Flags_A"))
            elif self.cprio3 == 5:
                data.append(str(battery.get_flagsb()))
                db_data_names.append(str(battery.__str__() + "_Flags_B"))
            elif self.cprio3 == 6:
                data.append(str(battery.get_max_error()))
                db_data_names.append(str(battery.__str__() + "_MaxError"))
            elif self.cprio3 == 7:
                data.append(str(battery.get_state_of_health()))
                db_data_names.append(str(battery.__str__() + "_StateOfHealth"))
            elif self.cprio3 == 8:
                data.append(str(battery.get_avg_time_to_empty()))
                db_data_names.append(str(battery.__str__() + "_Avg_Time_to_Empty"))
            elif self.cprio3 == 9:
                data.append(str(battery.get_avg_time_to_full()))
                db_data_names.append(str(battery.__str__() + "_Time_to_full"))
            elif self.cprio3 == 10:
                data.append(str(battery.get_learned_status()))
                db_data_names.append(str(battery.__str__() + "_Learned_Status"))
            elif self.cprio3 == 11:
                data.append(str(battery.get_cycle_count()))
                db_data_names.append(str(battery.__str__() + "_Cycle_Count"))


        if self.cprio3 == self.prio3:
            self.cprio3 = 0
        if self.cprio2 == self.prio2:
            self.cprio2 = 0

        #return data
        QueuedDatabase.psu_insert(data, db_data_names)
        return 1

    def check(self):
        """
        check running all available PSU check funktions available
        :return:
        """
        self.psu.check_status()
        self.psu.fg_value_check()
        for battery in self.psu.BATTERIES:
            def max_three(func):
                for i in range(3):
                    _temp = func()
                    if _temp != -1 and _temp != -273.25:
                        return _temp
                return  func()

            data = {
                "current": max_three(battery.get_current),
                "voltage": max_three(battery.get_voltage),
                "temp": max_three(battery.get_temperature),
                "soc": max_three(battery.get_state_of_charge)
            }

            self.psu.temp_check(battery, data["temp"])
            self.psu.ocp_check(battery, data["current"])
            self.psu.ovp_check(battery, data["voltage"])
            self.psu.uvp_check(battery, data["voltage"])
            if not self.CHARGING:
                self.psu.SOC_check(battery, data["soc"])
        # self.psu.uvp_check()
        pass  # checks if all values are okay

    def enter_execution(self):
        self.psu.info_log("STATE CHANGE", "Changing to execution")
        self.check()
        time.sleep(1)
        self.psu.system_info()
        if self.psu.connect_batteries():
            time.sleep(1)
            self.psu.disconnect_usb_power()
            time.sleep(1)
            self.psu.connect_vext()
            time.sleep(1)
            self.psu.connect_12v_powerline()
            time.sleep(1)
            return 1
        self.psu.error_log("PSU", "Changing to execution not possible")
        return 0

    def exit_execution(self):
        self.psu.info_log("STATE CHANGE", "Ending execution")
        self.psu.disconnect_12v_powerline()
        self.psu.disconnect_vext()
        time.sleep(0.5)

    def charging(self):  # save mode triggern
        # StateManager.StateManager.safe_state_sig = Value(bool, True)

        self.exit_execution()
        time.sleep(1)
        if self.psu.connect_batteries():
            time.sleep(1)
            self.psu.connect_usb_power()
            self.CHARGING = True
        else:
            self.dif_voltage_charge()
            self.CHARGING = True

    def dif_voltage_charge(self):
        #ToDo
        # needs to be cleared with team leaders --- possibly a non priority functions
        pass

    def initialize(self):
        """
        has to be done with the initialization of the OBC
        :return:
        """
        self.psu.info_log("PSU", "Doing Alex Routine for initializing Batteries shortly and shortly enable 5V für i2c fun.")
        time.sleep(1)
        if self.psu.connect_batteries():
            time.sleep(1)

            self.psu.disconnect_usb_power()
            time.sleep(1)

            self.psu.connect_vext()
            time.sleep(1)

            self.psu.disconnect_vext()
            time.sleep(1)

            self.psu.connect_usb_power()
            time.sleep(1)

            self.psu.disconnect_batteries()
            time.sleep(1)
            return 1
        else:
            return -1






