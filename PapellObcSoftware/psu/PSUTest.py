
# -*- coding: utf-8 -*-
##################################################
# PAPELL
# first created by "chris"
# successfully tested "Date xx.xx.xxxx" by "name"
#
# PSU Testing and Developing Class
#
##################################################


from helpers.i2c_switch import I2C_Switch

from helpers.psu_powerline import Powerline
from helpers.psu_battery import Battery
from multiprocessing import Value
from state_manager.Logger import Logger, LogLevel
from config import configurable_variables as cv
from numpy import array, mean, max, min
import time


class PSUTest(object):
    """Class for psu testing purposes"""

    def __init__(self, state_manager):
        self._state_manager = state_manager
        Logger.get_instance().log(LogLevel.INFO, "PSU", "Initializing PSU")
        switch = I2C_Switch(0x72)

        self.batpins = [
            ["BAT0", 22, 5],
            ["BAT1", 10, 4],
            ["BAT2", 9, 2],
            ["BAT3", 27, 1],
            ["BAT4", 11, 0],
            ]

        self.circpins = [
            [5, 1, "VEXT_EN"],
            [26, 0, "12V_EN"],
            [13, 0, "VUSB_DIS"]
        ]


        self.POWERLINES = [Powerline(pin, default, name) for pin, default, name in self.circpins]
        time.sleep(1.0)
        self.BATTERIES = [Battery(switch, nbr, pin, name) for name, pin, nbr in self.batpins]
        self.check_status()
        self.PACK1 = []
        self.PACK2 = []

    @staticmethod
    def info_log(kind, msg):
        Logger.get_instance().log(LogLevel.INFO, str("PSU" + str(kind)), str(msg))

    @staticmethod
    def warn_log(kind, msg):
        Logger.get_instance().log(LogLevel.WARNING, str("PSU" + str(kind)), str(msg))

    @staticmethod
    def error_log(kind, msg):
        Logger.get_instance().log(LogLevel.ERROR, str("PSU" + str(kind)), str(msg))

    def all_info(self):
        for i in range(len(self.BATTERIES)):
            self.bat_info(self.BATTERIES[i])

    def system_info(self):
        infos = []
        for battery in self.BATTERIES:
            infos.append(battery.info())
        infos = array(infos)
        PSUTest.info_log("", "Fancy Information about PSU" )
        PSUTest.info_log("", "System contains of %i Batteries(%s)" % (len(self.BATTERIES),
                                                                      str([str(str(bat) + ": " + str(bat.connect_status()) ) for bat in self.BATTERIES])))
        PSUTest.info_log("", "Battery Charging Status:........ %s%%" % mean(infos[:, 0]))
        PSUTest.info_log("", "Battery Minimum Chg Status:..... %s%%" % mean(infos[:, 0]))
        PSUTest.info_log("", "Battery Highest Voltage......... %smV" % max(infos[:, 1]))
        PSUTest.info_log("", "Battery Mean Voltage:........... %smV" % mean(infos[:, 1]))
        PSUTest.info_log("", "Battery Lowest Voltage:......... %smV" % min(infos[:, 1]))
        PSUTest.info_log("", "Current Current Usage:.......... %smAh" % str(-1*(sum(infos[:, 2]))))
        PSUTest.info_log("", "Current Power Usage:............ %smW" % str(-1*sum(infos[:, 3])))
        PSUTest.info_log("", "Battery Current Capacity:....... %smAh" % mean(infos[:, 4]))
        PSUTest.info_log("", "Battery Full Capacity:.......... %smAh" % mean(infos[:, 5]))
        PSUTest.info_log("", "Battery highest Temperature:.... %sC" % max(infos[:, 6]))
        PSUTest.info_log("", "FuelGauge highest Internal Temp: %sC" % max(infos[:, 7]))


    def powerline_connect(self, powerline: Powerline):
        """connects Powerline"""
        powerline.connect()
        print("Circuit %s disconnected" % powerline)

    def powerline_disconnect(self, powerline: Powerline):
        """disconnects Powerline"""
        powerline.disconnect()
        print("Powerline %s connected" % powerline)

    def bat_info(self, battery: Battery):
        """returns information about battery"""
        PSUTest.info_log(battery, "Fancy Information about Battery: " + str(battery))
        PSUTest.info_log(battery, "Battery connected %s " % str(battery.connect_status()))
        PSUTest.info_log(battery, "Battery Charging Status:... %s%%" % (battery.get_state_of_charge()))  ##
        PSUTest.info_log(battery, "Battery Voltage:........... %smV" % (battery.get_voltage()))  ##
        PSUTest.info_log(battery, "Battery Current Capacity:.. %smAh" % (battery.get_capacity()))  ###
        PSUTest.info_log(battery, "Battery Full Capacity:..... %smAh" % (battery.get_full_capacity())) ###
        PSUTest.info_log(battery, "Battery Design Capacity:... %smAh" % (battery.get_design_capacity()))
        PSUTest.info_log(battery, "Battery Temperature:....... %sC" % (battery.get_temperature())) ###
        PSUTest.info_log(battery, "FuelGauge Internal Temp:... %sC" % (battery.get_internal_temperature())) ###
        PSUTest.info_log(battery, "Current Power Usage:....... %smW" % (str(-int(battery.get_power()))))  ##
        PSUTest.info_log(battery, "Current Current Usage:..... %smAh" % (str(-int(battery.get_current())))) ##
        PSUTest.info_log(battery, "Battery flags A: %s" % str(battery.get_flagsa()))
        PSUTest.info_log(battery, "Battery flags B: %s" % str(battery.get_flagsb()))
        PSUTest.info_log(battery, "Battery Max Error:........ %s%%" % (battery.get_max_error()))
        PSUTest.info_log(battery, "Battery State of Health:.. %s%%" % (battery.get_state_of_health()))
        PSUTest.info_log(battery, "Average Time to Empty:.... %smin" % (battery.get_power()))
        PSUTest.info_log(battery, "Current Time to full:..... %smin" % (battery.get_power()))
        PSUTest.info_log(battery, "Learned Status: %s" % str(battery.get_learned_status()))
        PSUTest.info_log(battery, "Cycle Count: ..............%s" % battery.get_cycle_count())



    def disconnect_usb_power(self):
        self.POWERLINES[2].connect()
        self.info_log("CIRCUIT", "USB Power disconnected")

    def connect_usb_power(self):
        self.POWERLINES[2].disconnect()
        self.info_log("CIRCUIT", "USB Power reconnected")

    def connect_12v_powerline(self):
        self.info_log("CIRCUIT", "Connect 12V Circuits")
        self.connect_vext()
        self.POWERLINES[1].connect()

    def disconnect_12v_powerline(self):
        self.info_log("CIRCUIT", "Disconnect 12V Circuits")
        self.POWERLINES[1].disconnect()

    def connect_vext(self):
        self.info_log("CIRCUIT", "Connect external Circuits")
        self.POWERLINES[0].connect()

    def disconnect_vext(self):
        self.info_log("CIRCUIT", "Disconnect external Circuits")
        self.POWERLINES[0].disconnect()

    def connect_batteries(self):
        """disconnects Battery"""
        if self.charging_v_dif_check():
            for battery in self.BATTERIES:
                battery.connect()
            return 1
        else:
            PSUTest.error_log("", "Can't connect Batteries")
            return 0

    def disconnect_batteries(self):
        """disconnects Battery"""
        self.connect_usb_power()
        time.sleep(1.0)
        for battery in self.BATTERIES:
            battery.disconnect()

    def disconnect_broken(self, Bat : Battery):
        """disconnects a battery if broken, if no more than 2 Batteries are operating goes to safe mdoe"""

        self.disconnect_12v_powerline()
        self.disconnect_vext()
        self.disconnect_batteries()
        #Bat.broken()
        #i = 0
        #for bat in self.BATTERIES:
            #if bat.working():
                #i=i + 1
        #if i >= 3:
            #Bat.disconnect()
        #else:
        self._state_manager.safe_state_sig.value = True

##checks

    def check_status(self):
        state = True
        for battery in self.BATTERIES:
            if battery.dead():
                PSUTest.info_log(battery, "Battery dead, will be removed!")
                self.BATTERIES.remove(battery)
                state = False
        return state

    def fg_value_check(self):
        state = True
        texts = ["State of Charge", "Voltage", "Current", "Power", "Capacity", "Full Capacity", "Temperature", "Internal Temperature"]
        limits = [cv.SOC_LIMITS, cv.VOLTAGE_LIMITS, cv.CURRENT_LIMITS, cv.POWER_LIMITS, cv.CAPACITY_LIMITS, cv.FULL_CAPACITY_LIMITS, cv.TEMP_LIMITS, cv.TEMP_LIMITS]
        for bat in self.BATTERIES:
            for info, text, limit in zip(bat.info(), texts, limits):
                if not limit[0] <= info <= limit[1]:
                    PSUTest.warn_log(bat, str("%s out of limits %s" % (text, info)))
                    state = False
        return state

    def temp_check(self, battery, value):  # ToDo: Bat Packets need to be defined to shut them all down
        state = True
        for battery in self.BATTERIES:
            temp = battery.get_temperature()
            if temp >= battery.temp_critical:
                PSUTest.error_log(battery, "Battery to hot (%s) will be disconnected, and be broken" % temp)
                if battery in self.PACK1:
                    for bat in self.PACK1:
                        self.disconnect_broken(bat)
                if battery in self.PACK2:
                    for bat in self.PACK2:
                        self.disconnect_broken(bat)
                state = False
            if temp >= battery.temp_threshold:
                PSUTest.warn_log(battery, "Battery hot ... %s" % battery.get_temperature())
        return state

    def ovp_check(self, battery, value):
        state = True
        if int(value) >= cv.OVP_VOLTAGE_LIMITS[0]:
            PSUTest.warn_log(battery, "Battery voltage high ... %s" % str(value))
        if int(value) >= cv.OVP_VOLTAGE_LIMITS[1]:
            PSUTest.error_log(battery, "Battery voltage to high: %s, will be disconnected and be broken" % str(value))
            self.disconnect_broken(battery)
            state = False
        return state

    def ocp_check(self, battery, value):
        state = True
        if int(value) >= cv.OVC_CURRENT_LIMITS[0]:
            PSUTest.warn_log(battery, "Battery current high ... %s" % str(value))
        if int(value) >= cv.OVC_CURRENT_LIMITS[1]:
            PSUTest.error_log(battery, "Battery current to high: %s, will be disconnected and be broken" % str(value))
            self.disconnect_broken(battery)
            state = False
        return state

    def uvp_check(self, battery, value):
        state = True
        if int(value) <= cv.UVP_VOLTAGE_LIMITS[0]:
            PSUTest.warn_log(battery, "Battery voltage low ... %s" % str(value))
        if int(value) <= cv.UVP_VOLTAGE_LIMITS[1]:
            PSUTest.error_log(battery, "Battery voltage to low: %s, will enter safemode" % str(value))
            self._state_manager.safe_state_sig.value = True
            state = False
        return state

    def SOC_check(self, battery, value):
        state = True
        if int(value) <= cv.SOC_CHECK_LIMITS[0]:
            PSUTest.warn_log(battery, "Battery state of charge low ... %s" % str(value))
        if int(value) <= cv.SOC_CHECK_LIMITS[1]:
            PSUTest.error_log(battery, "Battery state of charge to low: %s, will enter safemode" % str(value))
            self._state_manager.safe_state_sig.value = True
            state = False
        return state

    def charging_v_dif_check(self):
        state = True
        voltages = []
        for bat in self.BATTERIES:
            voltages.append(bat.get_voltage())
        maxdif = max(voltages)-min(voltages)
        if maxdif >= cv.CHARGING_MAX_VOLTAGE_DIFF:
            PSUTest.error_log("", "Battery voltage difference to high %s" % str(maxdif))
            state = False
        return state



## Abfrage SOC -<  in PSU BATTERIES_FULL
## Abfrage laden -<  in PUS CHARGING

## Ladestand (weniger als ?? 30%) dann -> Warnung [stop states]
## Ladestand (weniger als ?? 20%) dann -> Error [in save state]
##
##
## OVP bei mehr als warunng --> , batterien abschalten bis kleiner 3 -> save state
##
## OCP bei mehr als warnung --> , batterien abschalten bis kleiner 3 -> save state
##

