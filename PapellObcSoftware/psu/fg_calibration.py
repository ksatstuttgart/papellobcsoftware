import smbus
import time


"""
How to use:
open python3 -i fg_calibration.py

repeat:
    current() -> gibt dir den Messwert
    open_cali_config() -> öffnet das ihr programmieren dürftet
    change mit wr()
    new checksum wr_check() ist negative addition
    close_config()
    
"""


def ftol(byte: int):
    liste = []
    liste.append(1) if byte & 1 > 0 else liste.append(0)
    liste.append(1) if byte & 2 > 0 else liste.append(0)
    liste.append(1) if byte & 4 > 0 else liste.append(0)
    liste.append(1) if byte & 8 > 0 else liste.append(0)
    liste.append(1) if byte & 16 > 0 else liste.append(0)
    liste.append(1) if byte & 32 > 0 else liste.append(0)
    liste.append(1) if byte & 64 > 0 else liste.append(0)
    liste.append(1) if byte & 128 > 0 else liste.append(0)
    return liste


class FG_Calib:
    def __init__(self):

        self.bus = smbus.SMBus(1)

        self.CHANNEL = [0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80]
        self.all_changes = []
        self.temp_check = 0
        self.check_after_changes = 0

        self.changes = []

    def openCH(self, channel):

        """opens on channel of the switch, closes all other"""
        self.bus.write_byte_data(0x72, 0x00, self.CHANNEL[channel])


    def open_all(self):
        """opens all channels of the switch"""
        self.bus.write_byte_data(0x72, 0x00, 0xff)


    def rd(self, reg):
        """reads a byte"""
        return str(self.bus.read_byte_data(0x55, reg))


    def wr(self, reg, val):
        """writes a byte"""
        self.bus.write_byte_data(0x55, reg, val)

    def wrr(self, reg, val):
        old = self.bus.read_byte_data(0x55, reg)
        change = {'reg': reg, 'old': old, 'new': val}
        self.wr(reg, val)
        self.changes.append(change)
        print(change)
        time.sleep(0.5)

    def wr_check(self, val=1000):
        """writes byte to checksum register, checksum ist negative addition"""
        try:
            if val == 1000:
                val = self.check_after_changes
                self.all_changes.append([change for change in self.changes])
                self.changes = []
            self.bus.write_byte_data(0x55, 0x60, val)
        except OSError as e:
            print("Write failed, probably wrong checksum(%s)? %s" % (str(val), str(e)))




    def open_current_sense_config(self):  # ToDo: we need to write the right sense resistor value in here, but we don't know the "kodierung" of this 4byte float
        """opens calibration configuration"""
        self.bus.write_word_data(0x55, 0x00, 0x0414)
        self.bus.write_word_data(0x55, 0x00, 0x3672)
        self.bus.write_byte_data(0x55, 0x61, 0x00)
        self.bus.write_byte_data(0x55, 0x3e, 0x68)
        self.bus.write_byte_data(0x55, 0x3f, 0x00)
        self.temp_check = self.rd(0x60)
        print("Current checksum %s" % self.bus.read_byte_data(0x55, 0x60))
        print("Current 1: %s , 2: %s, 3: %s, 4: %s" % (self.rd(0x40), self.rd(0x41), self.rd(0x42), self.rd(0x43)))

    def change_current_sense(self, which: int, dif: int):
        if 0 <= which <= 3:
            self.open_current_sense_config()
            val = self.rd(0x40+which)
            self.wrr(0x40+int(which), int(val)+int(dif))
            self.calc_check_from_changes()
            time.sleep(1)
            self.wr_check()
            self.close_config()


    def open_config(self, subclass, offset=0):
        print("opening config at subclass %s" % subclass)
        self.bus.write_word_data(0x55, 0x00, 0x0414)
        self.bus.write_word_data(0x55, 0x00, 0x3672)
        self.bus.write_byte_data(0x55, 0x61, 0x00)
        self.bus.write_byte_data(0x55, 0x3e, subclass)
        if offset == 0:
            self.bus.write_byte_data(0x55, 0x3f, 0)
        else:
            self.bus.write_byte_data(0x55, 0x3f, 1)
        self.temp_check = int(self.rd(0x60))
        print("current checksum %s" % self.temp_check)
        time.sleep(0.5)

    def open_calibration_data_config(self):
        self.open_config(48)

    def calc_check(self, old_checksum, old_value, new_value):
        temp = (255 - old_checksum - old_value) % 256
        checksum_after_changes = 255 - (temp + new_value) % 256
        print("calculated new checksum %s old: %s" % (checksum_after_changes, old_checksum))
        return checksum_after_changes

    def calc_check_from_changes(self):
        temp_check = self.temp_check
        for change in self.changes:
            temp_check = self.calc_check(int(temp_check), int(change['old']), int(change['new']))
        self.check_after_changes = temp_check
        return temp_check

    def current(self):
        while 1:
            print(self.bus.read_word_data(0x55, 0x10))
            time.sleep(1)

    def close_config(self):
        time.sleep(1)
        self.bus.write_word_data(0x55, 0x00, 0x0020)
        time.sleep(1)
        #self.bus.write_word_data(0x55, 0x00, 0x0041)
        print("config closed")


    def custom_print(self, liste, div):
        counter = 0
        for l in liste:
            print(str(hex(int(l))), end="\t")
            counter = counter +1
            if counter % div == 0:
                print()
        print()

    def dump_conf(self):
        dump = []
        for i in range(64, 96):
            temp = self.rd(i)
            dump.append(temp)
        self.custom_print(dump, 10)
        return dump

    def conc(self, a, b):
        return (int(a) << 8) + int(b)

    def h2(self,a,b):
        a = int(a)
        b = int(b)
        return str(str(self.conc(a, b)) + " [" + hex(a) +", "+ hex(b) + " ]")

    def read_config_data(self):
        dump = self.dump_conf()
        print("Design Capacity\t: %s" % self.h2(dump[11], dump[12]))
        print("Design Power\t: %s" % self.h2(dump[13], dump[14]))

    def new_design_capacity(self, new_value):
        print("write new design capacity: %s" % new_value)
        hexd = hex(int(new_value))[2:]
        self.open_calibration_data_config()
        time.sleep(0.5)
        self.wrr(0x4b, int(hexd[:-2], 16))
        time.sleep(0.5)
        self.wrr(0x4c, int(hexd[-2:], 16))
        self.calc_check_from_changes()
        time.sleep(1)
        self.wr_check()
        self.all_changes.append(self.changes)
        self.changes = []
        self.close_config()

    def new_design_power(self, new_value):
        print("write new design power: %s" % new_value)
        hexd = hex(int(new_value))[2:]
        self.open_calibration_data_config()
        time.sleep(0.5)
        self.wrr(0x4d, int(hexd[:-2], 16))
        time.sleep(0.5)
        self.wrr(0x4e, int(hexd[-2:], 16))
        self.calc_check_from_changes()
        time.sleep(1)
        self.wr_check()
        self.all_changes.append(self.changes)
        self.changes = []
        self.close_config()

    def thermistor(self, extern=1):
        assert 0<=extern<=1
        self.open_config(64)
        time.sleep(0.5)
        old = int(self.rd(0x41))
        self.wrr(0x41, old | extern) if extern == 1 else self.wrr(0x41, old & 0xfe)
        time.sleep(0.5)
        self.calc_check_from_changes()
        time.sleep(1)
        self.wr_check()
        self.changes = []
        self.close_config()

    def clear_changes(self):
        self.changes = []

    def get_flags(self, reg):
        print(ftol(int(self.rd(reg))))




if __name__ == "__main__":
    import bq34z100g1 as bq
    bat = bq.bq34z100g1()
    fg = FG_Calib()
