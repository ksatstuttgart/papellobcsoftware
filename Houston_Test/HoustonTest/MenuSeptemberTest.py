# imports
from modules.SensorArray import SensorArray
from modules.Logger import Logger
from modules.MagnetArray import MagnetArray
from modules.Pump import Pump
from modules.Vents import Valve
from modules.BatTests import PSUTest
from modules.StepperMotor import  StepperMotor
import modules.shift as shift
import time
import RPi.GPIO as GPIO
import atexit
import random
import multiprocessing


Safe_Mode = 1


# SuperClassForModes
class Mode:
    def __init__(self, run):
        self.rum = run


# ClassForDefaultMode
class DefaultMode(Mode):

    def __init__(self, run):
        super().__init__(run)
        self.valve = Valve()
        self.Pump = Pump()
        self.PSU = PSUTest()
        self.magnet_array = MagnetArray(1)
        self.stepper_motor_1 = StepperMotor(40, 14, 26, 15, 18)
        self.sensors = SensorArray()
        self.pump_1_thread = None
        self.pump_2_thread = None

    def music(self):
        """funny function due to night shift
        makes sounds by using the pump"""
        self.Pump.start_pump(1)
        viertel=1.5
        for i in range(4):
            self.Pump.time_pump(2,viertel)
            time.sleep(0.25)
            self.Pump.time_pump(2,viertel)
            time.sleep(0.25)
            for i in range(3):
                self.Pump.time_pump(2,viertel/3*2)
                time.sleep(0.05)
        self.Pump.stop_pump(1)


   
    def run(self):

        PlaceholderChargingStatus = self.PSU._Battery.getStateOfCharge()
        PlaceholderRemainingOperationTime = 2

        temperature = self.sensors.get_temperature()
        #magnetfield = self.sensors.get_mag

        user_input_operation = 0
        # loop for user input
        while user_input_operation != "quit":
            # TODO define charging variables
            print("\n######BREAK######################BREAK##################")
            print("Options:")
            print("Enter 's' to go to Safe Mode ")
            print('%s%% Charge Status' % (PlaceholderChargingStatus))
            print("Enter 'vo' to open valve")
            print("Enter 'vc' to close valve")
            print("Enter 'pump' to pump specified volume")
            print("Enter 'pumps' to stop pump")
            print("Enter 'moves' to move servo specified parameter")
            print("Enter 'stops' to stop servo")
            print("Enter 'array_1' to run array preset 1")
            print("Enter 'array_2' to run array preset 2")
            print("Enter 'array_3' to run array preset 3")
            print("Enter 'cpreset_1' to run combinded preset 1")
            print("Enter 'cpreset_2' to run combinded preset 2")
            print("Enter 'cpreset_3' to run combinded preset 3")
            print("Enter 'battstatus' for batterystatus")
            print("Enter 'sensors' to see sensors")    # TODO write better text
            print("Enter 'quit' to quit program")

            try:
                user_input_operation = input(": ")
            except KeyboardInterrupt:
                #SafeMode(1).stop()
                break

            # valve open:
            if user_input_operation == "vo":

                #see documentation for required inputs

                # valve_status = input("Valve Status: (").split()
                valve_status = "vo"
                # Safe_Mode = input("SafeMode: (0=close or 1=open)")
                Safe_Mode = 0 # int(Safe_Mode)

                self.valve.valve_management(valve_status, Safe_Mode)

            # valve close:
            elif user_input_operation == "vc":
                self.valve.valve_close()

            # Pump activation:
            elif user_input_operation == "pump":
                print("Enter '1' to run pump 1")
                print("Enter '2' to run pump 2")
                print("Enter 'all' to run both pumps")
                pumpno = input(": ")

                print("Enter the time(seconds) the pump(s) will be turned on")
                pumpt = input(": ")

                # try for the wanted pump activation
                try:
                    p = Pump()
                    if pumpno == "1":
                        if self.pump_1_thread != None and self.pump_1_thread.is_alive():
                            self.pump_1_thread.terminate()
                        self.pump_1_thread = multiprocessing.Process(target=p.time_pump, args=(1, float(pumpt)))
                        self.pump_1_thread.start()
                    elif pumpno == "2":
                        if self.pump_2_thread != None and self.pump_2_thread.is_alive():
                            self.pump_2_thread.terminate()
                        self.pump_2_thread = multiprocessing.Process(target=p.time_pump, args=(2, float(pumpt)))
                        self.pump_2_thread.start()
                    elif pumpno == "all":
                        if self.pump_1_thread != None and self.pump_1_thread.is_alive():
                            self.pump_1_thread.terminate()
                        self.pump_1_thread = multiprocessing.Process(target=p.time_pump, args=(1, float(pumpt)))
                        self.pump_1_thread.start()
                        if self.pump_2_thread != None and self.pump_2_thread.is_alive():
                            self.pump_2_thread.terminate()
                        self.pump_2_thread = multiprocessing.Process(target=p.time_pump, args=(2, float(pumpt)))
                        self.pump_2_thread.start()
                    else:
                        print("Unknown pump input")
                except ValueError:
                    print("time input is not a number")
                #self.pumps.pump_operation()

            # make all pumps stop
            elif user_input_operation == "pumps":
                
                if self.pump_1_thread != None:
                     self.pump_1_thread.terminate()
                     self.pump_1_thread = None
                if self.pump_2_thread != None:
                     self.pump_2_thread.terminate()
                     self.pump_2_thread = None
                Pump().stop_pump(1)
                Pump().stop_pump(2)

            # make servo turn
            elif user_input_operation == "moves":
                self.steps = 200
                print("moving servo %s steps " %self.steps)
                self.stepper_motor_1.step(self.steps)
                # TODO insert servo

            # stop servos
            elif user_input_operation == "stops":
                print("stopping servo")
                # TODO insert servo

            # use pumps for some seconds
            elif user_input_operation == "cpreset_1":
                print("running preset 1")

                self.Pump.time_pump(1, 5)
                self.Pump.time_pump(2, 2)
                # TODO insert combination

            # use pumps and then the one magnet
            elif user_input_operation == "cpreset_2":
                self.steps=100
                self.Pump.time_pump(1,3)
                Logger.log(Logger.LOG, "Pumping on Pump 1 for 3 seconds")
                self.stepper_motor_1.step(self.steps)
                Logger.log(Logger.LOG, "Moving Servo for %s steps"%(self.steps))
                shift.digitalWrite(2,1)
                Logger.log(Logger.LOG, "Turning on one Magnet on for 5 seonds ")
                time.sleep(5)
                shift.digitalWrite(2,0)
                Logger.log(Logger.LOG, "cpreset_2 finished")
                # TODO insert combination

            # activate some magnets and then activate the pumps
            elif user_input_operation == "cpreset_3":
                # self.music()
                shift.digitalWrite(5,1)
                shift.digitalWrite(7,1)
                Logger.log(Logger.LOG, "Turning on two Magnet on for 3 seonds ")
                time.sleep(3)
                shift.digitalWrite(5,0)
                shift.digitalWrite(7,0)
                Logger.log(Logger.LOG, "Doing some Sound Engineering with Pumps")
                for i in range(1,5):
                    self.Pump.time_pump(1,random.random()+0.5)
                    self.Pump.time_pump(2,random.random()+0.5)

                print("finished preset 3")
                # TODO insert combination

            # activate two magnets
            elif user_input_operation == "array_1":
                # MA1 Job:
                Logger.log(Logger.LOG, "Turning on one Magnet on for 5 seonds ")
                # self.magnet_array.shift_register.set_magnet(2,1)
                shift.digitalWrite(2, 1)
                time.sleep(5) # time in Seconds
                shift.digitalWrite(2, 0)

            # activate several magnets, each for some time
            elif user_input_operation == "array_2":
                Logger.log(Logger.LOG, "Horizontal movement with magnets is done")
                # Way=            [[  1. magnet], [ 2. magnet]]; turned on simultanious
                path_horizontal = [[[1, 1, 3000]],
                                   [[1, 1, 3000], [1, 2, 3000]],
                                   [[1, 2, 3000]],
                                   [[1, 2, 3000], [1, 3, 3000]],
                                   [[1, 3, 3000]],
                                   [[1, 3, 3000], [1, 4, 3000]],
                                   [[1, 5, 3000]],
                                   [[1, 6, 3000]],
                                   [[1, 7, 3000]],
                                   [[1, 8, 3000]],
                                   [[1, 9, 3000]],
                                   [[1, 10, 3000]],
                                   [[1, 11, 3000]],
                                   [[1, 12, 3000]],
                                   [[1, 13, 3000]],
                                   [[1, 4, 3000]]]
                self.magnet_array.move_it(path_horizontal, 16)

            # activate several magnets by reading the config file config_ma.csv
            elif user_input_operation == "array_3":
                # MA3 Job:
                # Test read config
                my_config = self.magnet_array.read_config("config_ma.csv")
                # Logger.log(Logger.LOG, my_config)
                my_path = self.magnet_array.list_to_path(my_config)
                Logger.log(Logger.VERBOSE, my_path)
                # Test move_it with the (my_path, lines of the config.csv)
                self.magnet_array.move_it(my_path, 3)

            # read Battery status
            elif user_input_operation == "battstatus":
                #TODO ausgabe noch schreiben (keine echte ausgabe!)
                self.PSU.BatInfo()
                #print(temperature)

            # test sensors
            elif user_input_operation == "sensors":
                self.sensors.test()
                print()

            # stop the program
            elif user_input_operation == "quit":
                print("quitting program")
                GPIO.cleanup()
                break

            # activate Safe Mode
            elif user_input_operation == "s" or user_input_operation == "S":
                safe_mode = SafeMode(1, self)
                safe_mode.run()

            else:
                print("Unknown input")
                print("resetting program")


# ClassForSafeMode
class SafeMode(Mode):

    def __init__(self, run, default):
        super().__init__(run)
        self.valve = Valve()
        self.pumps = Pump()
        self.sensors = SensorArray()
        self.magnet_array = MagnetArray(1)
        self.default = default

    def stop(self):
        """Function to stop all components of the experiment and close everything"""
        temperature = self.sensors.get_temperature()

        print("\nNow running Safe-Mode")
        print(temperature)

        if self.default.pump_1_thread != None and self.default.pump_1_thread.is_active():
            self.default.pump_1_thread.terminate()
            self.default.pump_1_thread = None

        if self.default.pump_2_thread != None and self.default.pump_2_thread.is_active():
            self.default.pump_2_thread.terminate()
            self.default.pump_2_thread = None

        self.magnet_array.all_off()  # stop magnets
        Pump().stop_pump(1)
        Pump().stop_pump(2)

        valve_status = "close"
        Safe_Mode = 1
        Valve().valve_management(valve_status, Safe_Mode)

        # TODO stop servos, stop preset

        print("\nEntered Safe-Mode, all operations halted")
        print("Program will now terminate")

    def run(self):
        """what safemode does.
        Including basic interface output and additional stopping and turning of the experiment"""
        temperature = self.sensors.get_temperature()

        print("\nNow running Safe-Mode")
        print(temperature)

        self.magnet_array.all_off()  # stop magnets
        Pump().stop_pump(1)
        Pump().stop_pump(2)

        valve_status = "close"
        Safe_Mode = 1
        Valve().valve_management(valve_status, Safe_Mode)

        # TODO stop servos, stop preset

        print("\nEntered Safe-Mode, all operations halted")
        print("Enter 'OP' to continue operations")
        print("Enter 'quit' to quit program")

        # something that warns about resuming operations if some value is still critical
        user_input_safe_mode = input(": ")

        if user_input_safe_mode == "quit" or user_input_safe_mode == "QUIT":
            print("shutting down")
            exit()
        elif user_input_safe_mode == "OP":
            print("resuming Operations")

            defaultmode = DefaultMode(1)

            defaultmode.run()

        else:
            print("Unknown input")

# ClassForModeDecisions
def on_exit(default):
    SafeMode(1, default).run()

if __name__ == '__main__':
    # Start program in default Mode
    defaultmode = DefaultMode(1)
    atexit.register(on_exit, default=defaultmode)
    defaultmode.run()
