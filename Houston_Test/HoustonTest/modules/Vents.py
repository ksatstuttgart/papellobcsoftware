from modules.Logger import Logger

import RPi.GPIO as GPIO

class Valve:



    def __init__(self):

        self.Safe_Mode = 0
        # Logger.log(Logger.LOG, "Safe Mode is: %s \n" % Safe_Mode)
        # self.port1 = 5

    def valve_close(self):
        port = 5
        # for port in range(22, 27):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(port, GPIO.OUT)
        GPIO.output(port, GPIO.LOW)
        Logger.log(Logger.LOG, "Any valve is closed")

    def valve_io(self, valve, status):
        if valve == "v1":
            port = 5 # TODO only active pin
            detect = True
        # elif valve == "v2":
        #     port = 23
        #     detect = True
        # elif valve == "v3":
        #     port = 24
        #     detect = True
        # elif valve == "v4":
        #     port = 25
        #     detect = True
        # elif valve == "v5":
        #     port = 26
        #     detect = True
        # elif valve == "v6":
        #     port = 27
        #     detect = True
        else:
            detect = False
            Logger.log(Logger.LOG, "Unknown valve")

        if detect is True:
            port = int(port)
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(port, GPIO.OUT)
            if status == 1:
                GPIO.output(port, GPIO.HIGH)
                Logger.log(Logger.LOG, "Valve: %s; Status: Open" % valve)
            elif status == 0:
                GPIO.output(port, GPIO.LOW)
                if self.Safe_Mode == 0:
                    Logger.log(Logger.LOG, "Valve: %s; Status: Close" % valve)
            else:
                Logger.log(Logger.LOG, "unknown status input")



    #           T1              T2
    #           |               |
    #           V1              V2
    #           |               |
    #           |---------------|
    #           |               |
    #           P1              P2
    #           |               |
    #           V3              V4
    #           |               |
    #           |---------------|
    #           |               |
    #           V5              V6
    #           |               |
    #           EXP1            EXP2


    # Program for valve management
    def valve_management(self, valve_status, safe_state):  # function name, which exp. area, pump, tank

        state = [0, 0, 0, 0, 0, 0]
        self.Safe_Mode = safe_state
        if valve_status == "norm_1" and self.Safe_Mode == 0:
            case = "norm_1"
        elif valve_status == "norm_2" and self.Safe_Mode == 0:
            case = "norm_2"
        elif valve_status == "cont_1" and self.Safe_Mode == 0:
            case = "cont_1"
        elif valve_status == "cont_2" and self.Safe_Mode == 0:
            case = "cont_2"
        elif valve_status == "cont_3" and self.Safe_Mode == 0:
            case = "cont_3"
        elif valve_status == "cont_4" and self.Safe_Mode == 0:
            case = "cont_4"
        elif valve_status == "close" and self.Safe_Mode == 0:
            case = "close"
        elif valve_status == "vo"  and self.Safe_Mode == 0:
            case = "vo"
        elif valve_status == "vc"  and self.Safe_Mode == 0:
            case = "vc"
        elif self.Safe_Mode is 1:
            case = "Safe_Mode"
        else:
            case = "close"
            Logger.log(Logger.LOG, "Unknown Input")

        if case == "norm_1":  # normal 1: t1 --> v1 --> p1 --> v3 --> v5 --> e1
            self.valve_io("v1", 1)
            self.valve_io("v2", 0)
            self.valve_io("v3", 1)
            self.valve_io("v4", 0)
            self.valve_io("v5", 1)
            self.valve_io("v6", 0)
            Logger.log(Logger.LOG, "Case: Normal 1")
        elif case == "norm_2":  # normal 2: t2 --> v2 --> p2 --> v4 --> v6 --> e2
            self.valve_io("v1", 0)
            self.valve_io("v2", 1)
            self.valve_io("v3", 0)
            self.valve_io("v4", 1)
            self.valve_io("v5", 0)
            self.valve_io("v6", 1)
            Logger.log(Logger.LOG, "Case: Normal 2")
        elif case == "cont_1":  # contingency 1: t1 --> v1 --> p2 --> v4 --> v5 --> e1 (p1 failure)
            self.valve_io("v1", 1)
            self.valve_io("v2", 0)
            self.valve_io("v3", 0)
            self.valve_io("v4", 1)
            self.valve_io("v5", 1)
            self.valve_io("v6", 0)
            Logger.log(Logger.LOG, "Case: Contingency 1")
        elif case == "cont_2":  # contingency 2: t2 --> v2 --> p1 --> v3 --> v6 --> e2 (p2 failure)
            self.valve_io("v1", 0)
            self.valve_io("v2", 1)
            self.valve_io("v3", 1)
            self.valve_io("v4", 0)
            self.valve_io("v5", 0)
            self.valve_io("v6", 1)
            Logger.log(Logger.LOG, "Case: Contingency 2")
        elif case == "cont_3":  # contingency 3: t2 --> v2 --> p1 --> v3 --> v5 --> e1 (t1 failure)
            self.valve_io("v1", 0)
            self.valve_io("v2", 1)
            self.valve_io("v3", 1)
            self.valve_io("v4", 0)
            self.valve_io("v5", 1)
            self.valve_io("v6", 0)
            Logger.log(Logger.LOG, "Case: Contingency 3")
        elif case == "cont_4":  # contingency 4: t1 --> v1 --> p2 --> v4 --> v6 --> e2 (t2 failure)
            self.valve_io("v1", 1)
            self.valve_io("v2", 0)
            self.valve_io("v3", 0)
            self.valve_io("v4", 1)
            self.valve_io("v5", 0)
            self.valve_io("v6", 1)
            Logger.log(Logger.LOG, "Case: Contingency 4")
        elif case == "close":
            self.valve_io("v1", 0)
            # self.valve_io("v2", 0)
            # self.valve_io("v3", 0)
            # self.valve_io("v4", 0)
            # self.valve_io("v5", 0)
            # self.valve_io("v6", 0)
            Logger.log(Logger.LOG, "Case: Close. Valve closed")
        elif case == "Safe_Mode":
            self.valve_io("v1", 0)
            # self.valve_io("v2", 0)
            # self.valve_io("v3", 0)
            # self.valve_io("v4", 0)
            # self.valve_io("v5", 0)
            # self.valve_io("v6", 0)
            Logger.log(Logger.LOG, "Safe Mode: Valve closed")
        elif case == "vo":
            self.valve_io("v1", 1)
            # self.valve_io("v2", 1)
            # self.valve_io("v3", 1)
            # self.valve_io("v4", 1)
            # self.valve_io("v5", 1)
            # self.valve_io("v6", 1)
            Logger.log(Logger.LOG, "Valve opened")
        elif case == "vc":
            self.valve_io("v1", 0)
            # self.valve_io("v2", 0)
            # self.valve_io("v3", 0)
            # self.valve_io("v4", 0)
            # self.valve_io("v5", 0)
            # self.valve_io("v6", 0)
            Logger.log(Logger.LOG, "Valve closed")
        else:
            Logger.log(Logger.LOG, "Case not found.")





""""if __name__ == '__main__':
    valve_status = input("Valve Status: ")
    Safe_Mode = input("SafeMode: ")
    Safe_Mode = int(Safe_Mode)

    valve = Valve()

    valve.valve_management(valve_status, Safe_Mode)"""""


