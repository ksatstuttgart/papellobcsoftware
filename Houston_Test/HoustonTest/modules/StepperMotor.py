import RPi.GPIO as GPIO
import time


class StepperMotor:

    def current_time_micros(self):
        return int(round(time.time() * 1000000))

    def __init__(self, number_of_steps, pin_a_enbl, pin_a_phase, pin_b_enbl, pin_b_phase):
        self.step_number = 0
        self.direction = 0
        self.last_step_time = 0
        self.number_of_steps = number_of_steps

        self.step_delay = 60 * 1000 * 1000 / self.number_of_steps / 120

        self.pins = [pin_a_enbl, pin_a_phase, pin_b_enbl, pin_b_phase]
        self.sequence = [[1, 1, 1, 0],
                         [1, 0, 1, 0],
                         [1, 0, 1, 1],
                         [1, 1, 1, 1]]

        GPIO.setmode(GPIO.BCM)

        for pin in self.pins:
            GPIO.setup(pin, GPIO.OUT)

    def set_speed(self, speed):
        self.step_delay = 60 * 1000 * 1000 / self.number_of_steps / speed

    def step(self, steps):
        steps_left = abs(steps)

        if steps > 0:
            self.direction = 1
        if steps < 0:
            self.direction = 0

        while steps_left > 0:
            now = self.current_time_micros()

            if float(now - self.last_step_time) >= self.step_delay:
                self.last_step_time = now
                if self.direction == 1:
                    self.step_number += 1
                    if self.step_number == self.number_of_steps:
                        self.step_number = 0
                else:
                    if self.step_number == 0:
                        self.step_number = self.number_of_steps
                    self.step_number -= 1
                steps_left -= 1

                for i in range(len(self.pins)):
                    GPIO.output(self.pins[i], self.sequence[self.step_number % 4][i])

        for pin in self.pins:
            GPIO.output(pin, GPIO.LOW)
