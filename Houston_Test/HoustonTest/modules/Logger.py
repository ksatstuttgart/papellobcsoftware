import time
import datetime


class Logger:

    # All the possible Log levels
    ERROR = "Error"
    WARNING = "Warning"
    EXCEPTION = "Exception"
    LOG = "Log"
    VERBOSE = "Verbose"

    @staticmethod
    def log(log_level, msg):
        '''
        if not verbose, the message is printed in the console
        TODO saves the message in the log files
        :param log_level:
        :param msg: Message to be saved as string
        :return:
        '''
        if log_level != "Verbose":
            print("[" + str(log_level) + "][" + str(Logger.get_current_time_str()) + "]: " + str(msg))

    @staticmethod
    def get_current_time_millis():
        '''
        :return: Returns the time in milliseconds since the epoch as an Integer
        '''
        return int(round(time.time() * 1000))

    @staticmethod
    def get_current_time_str():
        '''
        :return: Returns the current date and time as [yyyy-mm-dd hh:mm:ss]
        '''
        return str(datetime.datetime.now()).split(".")[0]