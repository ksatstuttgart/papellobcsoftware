import RPi.GPIO as GPIO
import time

"""GPIO.setmode(GPIO.BCM)

GPIO.setup(29, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)
GPIO.setup(28, GPIO.OUT)

GPIO.output(29, GPIO.LOW)
GPIO.output(27, GPIO.LOW)
GPIO.output(28, GPIO.LOW)"""

GPIO.setmode(GPIO.BCM)

GPIO.setup(17, GPIO.OUT)

GPIO.output(17, GPIO.LOW)

time.sleep(5)

GPIO.output(17, GPIO.HIGH)

time.sleep(5)

GPIO.output(17, GPIO.LOW)

time.sleep(5)

GPIO.output(17, GPIO.HIGH)