#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#from modules.helper.battery import Battery
from modules.helpers.bq27441g1 import bq27441g1
from modules.Logger import Logger
import RPi.GPIO as GPIO

class PSUTest(object):
    """Class for PSU testing purposes"""
    
    def __init__(self):
        self._BATGAUGE = 0x55  # input BatteryGauge Adresses here
        self._Battery=bq27441g1(0x55, 1)
        # Mosfet vor charger
        # Mosfet zur Last

    def BatInfo(self):

        #gets Battery Number as 0,1,2,3,4
        Logger.log(Logger.LOG, "____________________________________")
        Logger.log(Logger.LOG, "Battery Charging Status:... %s%%" %(self._Battery.getStateOfCharge()))
        Logger.log(Logger.LOG, "Battery Voltage:........... %smV" %(self._Battery.getVoltage()))
        Logger.log(Logger.LOG, "Battery Current Capacity:.. %smAh" %(self._Battery.getCapacity()))
        Logger.log(Logger.LOG, "Battery Full Capacity:..... %smAh" %(self._Battery.getFullCapacity()))
        Logger.log(Logger.LOG, "Battery Design Capacity:... %smAh" %(self._Battery.getDesignCapacity()))
        Logger.log(Logger.LOG, "Charger Temperature:....... %sC"%(round(self._Battery.getTemperature()*0.1-273.15,2)))
        Logger.log(Logger.LOG, "Charger Internal Temp:..... %sC"%(round(self._Battery.getIntTemperature()*0.1-273.15,2)))
        Logger.log(Logger.LOG, "Used Power for Charging:... %smW"%(self._Battery.getPower()))
        Logger.log(Logger.LOG, "Used Current for Charging:. %smAh"%(self._Battery.getCurrent()))


class Circuit(object):
    """Class for Connecting and Disconnecting Circuits"""

    def __init__(self, pin):
        self._PIN = pin
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self._PIN, GPIO.OUT)

    def on(self):
        GPIO.output(self._PIN, GPIO.HIGH)

    def off(self):
        GPIO.output(self._PIN, GPIO.LOW)


if __name__== "__main__":
    import time
    PSU = PSUTest()
    while 1:
        print("____________________________________")
        print("Battery Charging Status:... %s%%" %(PSU._Battery.getStateOfCharge()))
        print("Battery Voltage:........... %smV" %(PSU._Battery.getVoltage()))
        print("Battery Current Capacity:.. %smAh" %(PSU._Battery.getCapacity()))
        print("Battery Full Capacity:..... %smAh" %(PSU._Battery.getFullCapacity()))
        print("Battery Design Capacity:... %smAh" %(PSU._Battery.getDesignCapacity()))
        print("Charger Temperature:....... %sC"%(round(PSU._Battery.getTemperature()*0.1-273.15,2)))
        print("Charger Internal Temp:..... %sC"%(round(PSU._Battery.getIntTemperature()*0.1-273.15,2)))
        print("Used Power for Charging:... %smW"%(PSU._Battery.getPower()))
        print("Used Current for Charging:. %smAh"%(PSU._Battery.getCurrent()))
        time.sleep(4)
