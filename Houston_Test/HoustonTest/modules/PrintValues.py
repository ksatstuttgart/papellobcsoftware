import time
from sensors.helpers.MCP9808 import MCP9808 as raw_temp
from sensors.helpers.Magnetometer import LIS3MDL as raw_mag

temp_sensor = raw_temp()
temp_sensor.begin()

mag_sensor = raw_mag()
mag_sensor.enableLIS()

while True:
    temp = temp_sensor.readTempC()
    mag_raw = mag_sensor.getMagnetometerRaw()
    mag = mag_sensor.get_mag()
    mag_temp = mag_sensor.getLISTemperatureCelsius(False)
    print("temp: %s" % (temp))
    print("mag temp: %s" % (mag_temp))
    print("mag raw: %s" % (mag_raw))
    print("mag: %s" % mag)
    time.sleep(0.05)
