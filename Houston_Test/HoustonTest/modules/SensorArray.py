from modules.Logger import Logger
from modules.helpers.MCP9808 import MCP9808 as raw_temp
from modules.helpers.Magnetometer import LIS3MDL as raw_mag
import time


class SensorArray:

    def __init__(self):
        try:
            self.temp_sensor = raw_temp()
            self.temp_sensor.begin()
        except:
            self.temp_sensor = None
            Logger.log(Logger.ERROR, "Temperature sensor not found.")

        try:
            self.mag_sensor_0 = raw_mag(addr=0x1e)
            self.mag_sensor_0.enableLIS()
        except:
            self.mag_sensor_0 = None
            Logger.log(Logger.ERROR, "Magnetometer at address 0x1e not found.")

        try:
            self.mag_sensor_1 = raw_mag(addr=0x1c)
            self.mag_sensor_1.enableLIS()
        except:
            self.mag_sensor_1 = None
            Logger.log(Logger.ERROR, "Magnetometer at address 0x1c not found.")

    def get_temperature(self):
        try:
            return self.temp_sensor.readTempC()
        except:
            Logger.log(Logger.ERROR, "Temperature sensor not found.")
            return -1

    def get_raw_mag(self, sensor=0):
        if sensor == 0:
            try:
                return self.mag_sensor_0.getMagnetometerRaw()
            except:
                Logger.log(Logger.ERROR, "Magnetometer at address 0x1e not found.")
                return -1
        else:
            try:
                return self.mag_sensor_1.getMagnetometerRaw()
            except:
                Logger.log(Logger.ERROR, "Magnetometer at address 0x1e not found.")
                return -1

    def get_mag(self, sensor=0):
        if sensor == 0:
            try:
                return self.mag_sensor_0.get_mag()
            except:
                Logger.log(Logger.ERROR, "Magnetometer at address 0x1e not found.")
                return -1
        else:
            try:
                return self.mag_sensor_1.get_mag()
            except:
                Logger.log(Logger.ERROR, "Magnetometer at address 0x1c not found.")
                return -1

    def test(self):
        for i in range(0, 10):
            Logger.log(Logger.LOG, "temp: %s" % self.get_temperature())
            Logger.log(Logger.LOG, "mag 0: %s" % self.get_mag(0))
            Logger.log(Logger.LOG, "mag 1: %s" % self.get_mag(1))
            time.sleep(1)
