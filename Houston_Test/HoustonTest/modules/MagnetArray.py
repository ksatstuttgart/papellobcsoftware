# -*- coding: utf-8 -*-
# !/usr/bin/python
import time
from modules.Logger import Logger
from modules.ShiftRegister import ShiftRegister
import modules.shift as shift


class MagnetArray:

    def __init__(self, status):
        self.status = status
        # TODO Pins for data, clk, stb, clr
        # self.shift_register = ShiftRegister(40, 1, 2, 3, 4)

    def current_time_millis(self):
        return int(round(time.time() * 1000))

    def all_off(self):
        """
        Turns all Magnets in the MA off.
        :return:
        """
        Logger.log(Logger.LOG, "Turning the MagnetArray off.")
        # self.shift_register.clear_all()
        for nu in range(0, 10):
            #shift.digitalWrite(nu,0)
            Logger.log(Logger.LOG, "turning Magnet "+str(nu)+"off")


    def read_config(self, name):
        """
        this method reads config_m.csv file and makes an list of it
        each line of the file is new part of the list
        Lines with "+" in the beginning are comments
        list[line]
        :param name: File name of a .csv to transfer into String list
        :return: pathes [List] (each line is one Path)
        """
        with open(name, "r") as f:
            # Writes config data in String: config_list
            Logger.log(Logger.LOG, "Name of the file: " + str(f.name))
            content = f.read().splitlines()
            config_list = []
            for c in content:
                neu = c.replace("(", "").replace("/", ";").replace(")", "").split(";")
                if "+" in neu[0]:
                    Logger.log(Logger.VERBOSE, c)

                elif neu[0] != "+":
                    config_list.append(neu)
        return config_list

    def list_to_path(self, config_list):
        """
        makes a List out of the list of strings from the config_m.csv file
        parts are (xpos,ypos, deltaTime)
        all in ints
        :param: config_name: Name of the config file name to transfer in MagnetArray
        :return: List in ints: list[pathroutes[pathpoints[circuit/magnet,dt]]]
        """
        command_list =[]
        for l in config_list:
            # Loop over each pathroutes
            i = 0
            commandpath = []
            while i < len(l):
                # Loop over all path Point
                command = []
                for j in range(0, 3):
                    # Loop for one path Point(circuit, magnet,duration)
                    command.append(int(l[i+j]))
                commandpath.append(command)
                i = i + 3
            command_list.append(commandpath)
        return command_list

    def move_it(self, command_list, num_of_path):
        """
        uses command_list (List with points of MA and time to turn the Magnets on, in wanted order
        :param command_list: List of ints: list[pathroutes[pathpoints[circuit, magnet, dt]]]
        :param num_of_path: Path in int to be run, starting from Path 1
        :return:
        """
        if 0 <= num_of_path-1 :
            # Loop of one Path Route
            for path in command_list:
                Logger.log(Logger.LOG, path)
                millis_delay = 0
                for pathpoint in path:
                    if pathpoint[2] > millis_delay:
                        millis_delay = pathpoint[2]
                    shift.digitalWrite(pathpoint[1] - 1, 1)
                    print(pathpoint[1], " ein")
                    Logger.log(Logger.LOG,
                           "Magnet " + str(pathpoint[1]) + " is on for " + str(pathpoint[2]) + " milliseconds")
                millis_start = self.current_time_millis()

                while self.current_time_millis() <= millis_start + millis_delay:
                    for pathpoint in path:
                        if self.current_time_millis() == millis_start + pathpoint[2]:
                            shift.digitalWrite(pathpoint[1]-1, 0)
                    time.sleep(0.0001)

            Logger.log(Logger.LOG, "_____End of this path_____")
        else:
            Logger.log(Logger.ERROR, "No " + str(num_of_path) + ". Path found (Total: " + str(len(command_list)) + ")")


# if __name__ == '__main__':

    # magnet_array = MagnetArray(1)

    # Test methods
    # print("_____Test read_config_____\n")
    # my_config = magnet_array.read_config("config_ma.csv")
    # print(my_config)

    # print("\n\n_____Test list_to_path_____\n")
    # my_path = magnet_array.list_to_path(my_config)
    # print(my_path)

    # print("\n\n_____Test move_it_____\n")
    # magnet_array.move_it(my_path, 1)

    # print("\n\n_____Test all_off_____\n")
    # magnet_array.all_off()

    # print("\n\n_____Test Exception: Invalid Magnet_____\n")
    # magnet_array.shift_register.set_magnet(-2, 1)

    # print("\n\n_____Test Exception: Invalid State_____\n")
    # magnet_array.shift_register.set_magnet(4, 2)

    # TODO Job for Septembertest:
    # MA1 Job:
    # Logger.log(Logger.LOG,"Turning on one Magnet for Septembertest")
    # magnet_array = MagnetArray(1)
    # magnet_array.shift_register.set_magnet(2,1)

    # MA2 Job:
    # Logger.log(Logger.LOG,"Horizontal movement with magnets is done")
    # magnet_array = MagnetArray(1)
    # Weg               1.          2. Magn;    next Time               ; and last time
    # path_horiontal = [[[1,1,3000], [1,2,4000]], [[1,2,1000], [1,3,5000]], [[1,3,3000], [1,4,3000]]]
    # magnet_array.move_it(path_horiontal, 1)

    # MA3 Job:
    # magnet_array = MagnetArray(1)
    # Test read config
    # my_config = magnet_array.read_config("config_ma.csv")
    # Logger.log(Logger.LOG, my_config)
    # Test list_to_path
    # my_path = magnet_array.list_to_path(my_config)
    # Logger.log(Logger.LOG,my_path)
    # Test move_it
    # magnet_array.move_it(my_path, 1)
    # Test all_off
    # magnet_array.all_off()


    # read Config file:
    #print("los:")
    #my_config_list = magArray.read_config("config_ma.csv")
    #print(my_config_list)
    #MA_list = magArray.list_to_path(config_list)
    #magArray.move_it(MA_list)

    #Logger.log(Logger.VERBOSE, "asdf")

    # Koordinates: xxyy are xx circuit number
    #                        yy Magnet number
    #
