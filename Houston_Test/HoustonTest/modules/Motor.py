# git clone https: // github.com / pololu / drv8835 - motor - driver - rpi.git
# cd drv8835 - motor - driver - rpi
# sudo python setup.py install

import time
from itertools import cycle
from pololu_drv8835_rpi import motors, MAX_SPEED

# TODO
# drvpin 7 = pipin 12
# drvpin 8 = pipin 10
# drvpin 9 = pipin 37
# drvpin 10 = pipin 8

class Motor:

    pin_actuator = 1 # needs to be updated in the test

    def actuator_maxspeed(self, direction, speed=100):  # speed in percentage
        if 0 <= speed <= 100:
            if direction == "right":
                motors.setSpeeds(480 / 100 * speed, 480 / 100 * speed)
            elif direction == "left":
                motors.setSpeeds(-480 / 100 * speed, -480 / 100 * speed)
            elif direction == "stop":
                motors.setSpeeds(0, 0)
            else:
                raise Exception("Error: input only right, left or stop with a speed percentage")
        else:
            raise Exception("motor speed out of range: %s" % speed)
