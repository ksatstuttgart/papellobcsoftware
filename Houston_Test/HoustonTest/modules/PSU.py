#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from helper.battery import Battery
from helper.circuit import Circuit
#from helper.tempsensor import TempSensor


class PSU:
    def __init__(self):
        #initialise a Battery like this   | Battery([adress of IOExpander, pin of gpioa], adress of [FuelGauge, switch, nbr])
        self.Bat0 = Battery([0x20,0],[0x68,0x70,0])
        
        self.BATTERIES=[self.Bat0]
        #initialise a Circuit like this  | Circuit([adress of IOExpander, pin of gpiob])
        self.Circ33 = Circuit([0x20,0])
        
        
        ###Variables###
        self.Bat_Threshhold = 350
        
    def check(self):
        for Bat in self.Batteries:
            if Bat.getTemperature() > self.Bat_Threshhold : Bat.Disconnect
        
        
        
    def safe_mode():
        pass
    
    def charge_mode():
        bat = Battery()
        for bat in BATTERIES:
            if bat.