from default.Logger import Logger

import time
import RPi.GPIO as GPIO

"""
This Code can:  (a) execute flawlessly
                (b) activate and deactivate all necessary pins (2 in total)
                (c) successfully control PWM frequency & duty cyle, i. e. voltage
It cannot yet:  (d) be proven to work with an actual pump
                (e) show the desired characteristics of PWM with the LED I had at home
                    (to be precise, the PWM could blink the LED with a low frequency set
                    and showed variing lengths of the blink time correspoding to the set
                    duty cycle, but could not properly dim the LED with frequencies >20 Hz (presumably reaction time of LED to large)

!!!BEFORE ACTUAL TESTING WITH PUMP!!!: Adjust PWMAmplitude_Frequency & PWMAmp_DutyCycle to target according to manual (values are included in comments).
                                        Otherwise, Pump might not work as intended.

It should work, tho. :)
"""                

class Pump:

    OperationTime = 10 #pump operation time in s !! Please adjust to desired number before delivery !!
    PWMAmplitude_Frequency = 10 #PWM Frequency in Hz (target: 100000) (LED test frequency: 10)
    PWMAmp_DutyCycle = 1 #Voltage = 3.3V * PWMAmp_DutyCycle/100 (target area: 10.6 - 39.3)
    # Pin allocation
    PWMAmp_Pin = 13 #Physical Pin 33
    SHUTDOWN_Pin = 16 #Physical Pin 19
    # Initialisation of GPIO pins (apparently *must* happen outside of functions)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(PWMAmp_Pin, GPIO.OUT)
    GPIO.setup(SHUTDOWN_Pin, GPIO.OUT)


    
    p = GPIO.PWM(PWMAmp_Pin, PWMAmplitude_Frequency)
    """Clock Setup"""
    """ Activate only if internal Clock is -not- used. Please use internal clock if possible! Remember to de-comment lines in functions below"""
    #CLOCK_Pin = 12 #Physical Pin 32. Pump needs BOTH and therefore ALL PWM pins if clock is activated
    #GPIO.setup(CLOCK_Pin, GPIO.OUT)
    #CLOCK_Frequency = 400 #PWM Frequency in Hz (target Area: <400) equals 4*pump operation frequency
    #CLOCK_DutyCycle = 100 #Can be 100% AFAIK. Further testing necessary ...
    #q = GPIO.PWM(CLOCK_Pin, CLOCK_Frequency)

    # Starts Pump
    def start_pump(self):
        
        # Sample GPIO Out code

        
        self.p.start(self.PWMAmp_DutyCycle)
        self.p.ChangeDutyCycle(self.PWMAmp_DutyCycle)
        GPIO.output(self.SHUTDOWN_Pin, GPIO.HIGH)
        Logger.log(Logger.LOG, "Pump has started")
        #self.q.start(self.CLOCK_DutyCycle)
        #self.q.ChangeDutyCycle(self.CLOCK_DutyCycle)

    # Stops Pump
    def stop_pump(self):
        #self.q.ChangeDutyCycle(0)
        self.p.ChangeDutyCycle(0)  # Shutdown PWM "Amplitude"
        GPIO.output(self.SHUTDOWN_Pin, GPIO.LOW)  # Shutdown "Shutdown"
        GPIO.cleanup()
        Logger.log(Logger.LOG, "Pump has stopped")
        exit()

    # Runs Pump
    def pump_operation(self):
        self.start_pump()
        Logger.log(Logger.LOG, "Pump operates for " + str(self.OperationTime) + " seconds")
        time.sleep(self.OperationTime)
        self.stop_pump()

# *actually* runs Pump
PumpFunction = Pump()
PumpFunction.pump_operation()
