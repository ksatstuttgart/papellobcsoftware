import multiprocessing, time


def dosome(num):
    while True:
        print(num)
        time.sleep(1)

z1 = multiprocessing.Process(target=dosome, args=("1"))
z2 = multiprocessing.Process(target=dosome, args=("2"))


z1.start()
time.sleep(2)
z2.start()

time.sleep(2)
print("stopping")
z2.terminate()
time.sleep(2)
print("quit")
z1.terminate()
while True:
    print("3")
    time.sleep(1)