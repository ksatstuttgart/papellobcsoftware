import threading
import time


class MyThread(threading.Thread):
    Ergebnis = [0, 1]

    def __init__(self, tid, name):
        super(MyThread, self).__init__()
        self.tid = tid
        self.name = name

    def run(self):
        for i in range(20):
            lock_me.acquire()                           # Alle anderen Threads bleiben angehalten
            zahl = MyThread.Ergebnis[len(MyThread.Ergebnis) - 2] + MyThread.Ergebnis[len(MyThread.Ergebnis) - 1]
            MyThread.Ergebnis.append(zahl)
            lock_me.release()                           # Weiter gehts mit den anderen


lock_me = threading.Lock()                          # Locks definieren, mit der andere Threads angehalten werden

t1 = MyThread(1, "Fred")
t2 = MyThread(2, "Otto")
t3 = MyThread(3, "Peter")

t1.start()
t2.start()
t3.start()


# läuft erst weiter, wenn t1 bzw. t2 fertig ist
t1.join()
t2.join()
t3.join()
# ... oder
if t1.isAlive():
    time.sleep(1)

print(MyThread.Ergebnis[61])
