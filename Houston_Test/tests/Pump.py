
import time
#from default.Logger import Logger
import RPi.GPIO as GPIO


class Pump:

    def pump_io(self, pump):
        if pump == 1:
            port = 21
            detect = True
        elif pump == 2:
            port = 22
            detect = True
        else:
            #Logger.log(Logger.LOG, 'Status Pump: Unknown Pump Input')
            detect = False

        if detect == 0:
            port = int(port)
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(port, GPIO.OUT)

    def start_pump(self, pump):
        #Logger.log(Logger.LOG, "Status Pump %s : started" % pump)
        if pump == 1:
            port = 21
            detect = True
        elif pump == 2:
            port = 22
            detect = True
        else:
            #Logger.log(Logger.LOG, 'Status Pump: Unknown Pump Input')
            detect = False

        if detect is True:
            port = int(port)
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(port, GPIO.OUT)
        #GPIO.setup(port, GPIO.OUT)
        GPIO.output(port, GPIO.HIGH)

    def stop_pump(self, pump):
        #Logger.log(Logger.LOG, "Status Pump %s : stopped" % pump)
        if pump == 1:
            port = 21
            detect = True
        elif pump == 2:
            port = 22
            detect = True
        else:
            #Logger.log(Logger.LOG, 'Status Pump: Unknown Pump Input')
            detect = False

        if detect is True:
            port = int(port)
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(port, GPIO.OUT)

        GPIO.output(port, GPIO.LOW)  # Shutdown "Shutdown"
        GPIO.cleanup()
0
    def time_pump(self, pump, t):
        print("Pump ", pump, "started")
        self.start_pump(pump)
        #Logger.log(Logger.LOG, "Status Pump %s : run for %s sec" % (pump, t))
        print("Pump", pump, "run for", t, "sec")
        time.sleep(int(t))
        print("Pump", pump, "stopped")
        self.stop_pump(pump)

    def pump_management(self, pump_command, Safe_Mode):
        try:
            if pump_command == 'st1' and Safe_Mode == 0:
                    print('pump_1 started ')
                    self.start_pump(1)
            elif pump_command == 'st2' and Safe_Mode == 0:
                    print('pump_2 started')
                    self.start_pump(2)
            elif pump_command == 'sp1' and Safe_Mode == 0:
                    print('pump_1 stopped')
                    self.stop_pump(1)
            elif pump_command == 'sp2' and Safe_Mode == 0:
                    print('pump_2 stopped')
                    self.stop_pump(2)
            elif pump_command == 'tp1' and Safe_Mode == 0:
 0           		t = input("requested time: ")
                    self.time_pump(1, t)
            elif pump_command == 'tp2' and Safe_Mode == 0:
                    t = input("requested time: ")
                    self.time_pump(2, t)
            elif Safe_Mode == 1:
                    print('Status SafeMode:', Safe_Mode)
                    self.stop_pump(1)
                    self.stop_pump(2)
                    #Logger.log(Logger.LOG, "Status SafeMode: %s" % Safe_Mode)
            else:
                    self.stop_pump(1)
                    self.stop_pump(2)
                    #Logger.log(Logger.LOG, "Status Pumps : unknown pump input")
                    #Logger.log(Logger.LOG, "Status SafeMode: %s" % Safe_Mode)
                    exit()
        except:
            self.stop_pump(1)
            self.stop_pump(2)


if __name__ == '__main__':
    pump_command = input("Pump Command: ")
    #pump_command = 'time_pump_1' #!!!change required (input not operational, Traceback)!!!#
    Safe_Mode = input("SafeMode: ")
    Safe_Mode = int(Safe_Mode)

  0  pump = Pump()

    pump.pump_management(pump_command, Safe_Mode)
