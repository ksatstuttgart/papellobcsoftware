# -*- coding: utf-8 -*-
# !/usr/bin/python
import time
from modules.Logger import Logger
from modules.ShiftRegister import ShiftRegister


class MagnetArray:

    def __init__(self, status):
        self.status = status
        # TODO Pins for data, clk, stb, clr
        self.shift_register = ShiftRegister(40, 1, 2, 3, 4)

    def all_off(self):
        """
        Turns all Magnets in the MA off.
        :return:
        """
        Logger.log(Logger.LOG, "Turning the MagnetArray off.")
        self.shift_register.clear_all()

    def read_config(self, name):
        """
        this method reads config_m.csv file and makes an list of it
        each line of the file is new part of the list
        Lines with "+" in the beginning are comments
        list[line]
        :param name: File name of a .csv to transfer into String list
        :return: pathes [List] (each line is one Path)
        """
        with open(name, "r") as f:
            # Writes config data in String: config_list
            Logger.log(Logger.LOG, "Name of the file: " + str(f.name))
            content = f.read().splitlines()
            config_list = []
            for c in content:
                neu = c.replace("(", "").replace("/", ";").replace(")", "").split(";")
                if "+" in neu[0]:
                    Logger.log(Logger.LOG,c)
                elif neu[0] != "+":
                    config_list.append(neu)
        return config_list

    def list_to_path(self, config_list):
        """
        makes a List out of the list of strings from the config_m.csv file
        parts are (xpos,ypos, deltaTime)
        all in ints
        :param: config_name: Name of the config file name to transfer in MagnetArray
        :return: List in ints: list[pathroutes[pathpoints[circuit/magnet,dt]]]
        """
        command_list =[]
        for l in config_list:
            # Loop over each pathroutes
            i = 0
            commandpath = []
            while i < len(l):
                # Loop over all path Point
                command = []
                for j in range(0, 3):
                    # Loop for one path Point(circuit, magnet,duration)
                    command.append(int(l[i+j]))
                commandpath.append(command)
                i = i + 3
            command_list.append(commandpath)
        return command_list

    def move_it(self, command_list, path):
        """
        uses command_list (List with points of MA and time to turn the Magnets on, in wanted order
        :param command_list: List of ints: list[pathroutes[pathpoints[circuit/magnet,dt]]]
        :param path: Path in int to be run, starting from Path 1
        :return:
        """
        if 0 <= path-1 <= len(command_list):
            # Loop of one Path Route
            Logger.log(Logger.LOG, command_list[path-1])
            for pathpoint in command_list[path-1]:
                # Loop for each Path points
                Logger.log(Logger.LOG,
                           "Magnetpoint " + str(pathpoint[1]) + " is on for " + str(pathpoint[2]) + " milliseconds")
                self.shift_register.set_magnet(pathpoint[1], ShiftRegister.HIGH)
                time.sleep(pathpoint[2] / 1000)
                self.shift_register.set_magnet(pathpoint[1], ShiftRegister.LOW)
            Logger.log(Logger.LOG, "_____End of this path_____")
        else:
            Logger.log(Logger.ERROR, "No " + str(path) + ". Path found (Total: " + str(len(command_list)) + ")")


# if __name__ == '__main__':

    # magnet_array = MagnetArray(1)

    # Test methods
    # print("_____Test read_config_____\n")
    # my_config = magnet_array.read_config("config_ma.csv")
    # print(my_config)

    # print("\n\n_____Test list_to_path_____\n")
    # my_path = magnet_array.list_to_path(my_config)
    # print(my_path)

    # print("\n\n_____Test move_it_____\n")
    # magnet_array.move_it(my_path, 1)

    # print("\n\n_____Test all_off_____\n")
    # magnet_array.all_off()

    # print("\n\n_____Test Exception: Invalid Magnet_____\n")
    # magnet_array.shift_register.set_magnet(-2, 1)

    # print("\n\n_____Test Exception: Invalid State_____\n")
    # magnet_array.shift_register.set_magnet(4, 2)


    # TODO Job for Septembertest:
    # MA1 Job:
    # Logger.log(Logger.LOG,"Turning on one Magnet for Septembertest")
    # magnet_array = MagnetArray(1)
    # magnet_array.shift_register.set_magnet(2,1)

    # MA2 Job:
    # Logger.log(Logger.LOG,"Horizontal movement with magnets is done")
    # magnet_array = MagnetArray(1)
    # Weg               1.          2. Magn;    next Time               ; and last time
    # path_horiontal = [[[1,1,3000], [1,2,3000]], [[1,2,3000], [1,3,3000]], [[1,3,3000], [1,4,3000]]]
    # magnet_array.move_it(path_horiontal, 1)

    # MA3 Job:
    # magnet_array = MagnetArray(1)
    # Test read config
    # my_config = magnet_array.read_config("config_ma.csv")
    # Logger.log(Logger.LOG, my_config)
    # Test list_to_path
    # my_path = magnet_array.list_to_path(my_config)
    # Logger.log(Logger.LOG,my_path)
    # Test move_it
    # magnet_array.move_it(my_path, 1)
    # Test all_off
    # magnet_array.all_off()

    # Koordinates: xxyy are xx circuit number
    #                        yy Magnet number
