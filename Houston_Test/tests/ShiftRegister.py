
# import RPi.GPIO as GPIO
from default.Logger import Logger


class ShiftRegister:

    HIGH = 1
    LOW = 0

    """This transforms the actuation of the magnetarray via shift register"""
    def __init__(self, num_of_pins, pin_data, pin_clk, pin_stb, pin_clr):
        self.num_of_pins = num_of_pins
        self.pin_data = pin_data
        self.pin_clk = pin_clk
        self.pin_stb = pin_stb
        self.pin_clr = pin_clr

        self.register = []

        for i in range(self.num_of_pins):
            self.register.append(0)

        #GPIO.output(pin_clr, GPIO.HIGH)
        #GPIO.output(pin_stb, GPIO.HIGH)

    def shift_data(self):
        """
        Shifts the states of the pins stored in the List register into the storage
        :return:
        """
        for i in range(self.num_of_pins-1, -1, -1):
            Logger.log(Logger.VERBOSE, "Shifting to latches")
            #GPIO.output(self.pin_data, self.register[i])

            #GPIO.output(self.clock_pin, GPIO.HIGH)
            #GPIO.output(self.clock_pin, GPIO.LOW)

        #GPIO.output(self.pin_stb, GPIO.LOW)
        #GPIO.output(self.pin_stb, GPIO.HIGH)

    def set_pin(self, pin, state):
        """
        Shifts the data into the register and sets a pin on/off
        :param pin: pin to be set on/off
        :param state: State of the pin (on = 1, off = 0)
        :param duration: time of the activation of one magnet in mili sec
        :return:
        """
        self.register[pin] = state
        self.shift_data()

    def set_pins(self, pins, state):
        # TODO Unused
        """
        Shifts the data into the register and sets a number of pins on/off
        :param pins: List of pins to be set on/off
        :param state: state of the pins (on = 1, off = 0)
        :return:
        """
        for pin in pins:
            self.set_pin(pin, state)

        self.shift_data()

    def clear_all(self):
        """
        Sets all pins off
        :return:
        """
        self.register = [0] * self.num_of_pins

        #GPIO.output(self.pin_clr, GPIO.LOW)
        #GPIO.output(self.pin_clr, GPIO.HIGH)
        Logger.log(Logger.LOG, "----All Magnets off----")

    def magnet_to_shift(self, magnet):
        """
        Returns the Pin the magnet is connected to
        Note: Pin P1 is 0, P2 is 1, ...
        :param magnet: Number of the magnet (eg. 24)
        :return: Returns the Pin of the Shift Register to be changed
        """
        if magnet <= 9:
            return 9-magnet
        elif magnet <= 19:
            return 9 + 19-magnet
        elif magnet <= 28:
            return 20 + 28-magnet
        else:   # if magnet <= 37
            return 29 + 37 - magnet

    def set_magnet(self, magnet, state):
        """
        this method is able to directly set the state of a single magnet at pos magnet
        :param magnet: Postion of Magnet as int on the magnet array (01, ..., 37)
        :param state: state of the magnet, on/off
        :return:
        """
        # TODO Möglicherweise zusätzlich Schaltkreisnummer schreiben
        magnet_out = "{0:b}".format(magnet)
        for i in range(0, 6 - len(magnet_out)):
            magnet_out = "0" + magnet_out
        if (state == 0 or state == 1) and 37 >= magnet >= 0:
            self.set_pin(self.magnet_to_shift(magnet), state)
            Logger.log(Logger.LOG, "Magnet: Position: " + str(magnet_out) + "|State: " + "{0:b}".format(state))
        else:
            Logger.log(Logger.ERROR, "Invalid State [" + str(state) + "] or Magnet position [" + str(magnet_out) + "]")


