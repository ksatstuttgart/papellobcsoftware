#!/bin/bash

from datetime import datetime
from time import sleep
## import RPi.GPIO as GPIO

def time():
    now = str(datetime.now())
    print(now)


def readconfig():
     """
     this method reads the FerroFluid path from the "config_ma.csv" and makes a list with one line = one path
     the informations from the file are pos x, y and the time of activation [msec] (x,y,deltaT)
     :return: pathes [List] (each line is one Path)
     """
     with open("test2.csv", "r") as f:
         print("Name of the file: ", f.name)
         content = f.read().splitlines()
         pathes = []
         for c in content:
             neu = c.replace("(","").replace(")","").split(";")
             pathes.append(neu)
             if "+" in neu[0]:
                 print(c)
             elif neu[0] != "+":
                 pathes.append(neu)
     return pathes
# Pumpe
def pump():
    #Pump Operation Time & other parameters
    t = 10
    PWMAmp_Pin = 25
    PWMAmp_Freq = 50
    PWMAmp_DutyCycle = 50
    
    SHUTDOWN_Pin = 10
    ## Sample GPIO Out code
    GPIO.setmode(GPIO.BCM)
    # GPIO.setup(CLOCK, GPIO.OUT)
    GPIO.setup(PWMAmp_Pin, GPIO.OUT)
    GPIO.setup(SHUTDOWN_Pin, GPIO.OUT)

    p = GPIO.PWM(PWMAmp_Pin, PWMAmplitude_Frequency)
    p.start
    p.ChangeDutyCycle(PWMAmp_DutyCycle)
    GPIO.output(SHUTDOWN_Pin, GPIO.HIGH)
    time()
    sleep(t)
    time()
    p.ChangeDutyCycle(0) # Shutdown PWM "Amplitude"
    GPIO.output(SHUTDOWN_Pin, GPIO.LOW) # Shutdown "Shutdown"
    GPIO.cleanup()
    exit()
    

def main():
    pump()
    
main()
