#imports

from modules.SensorArray import SensorArray
from modules.Logger import Logger
from modules.MagnetArray import MagnetArray
from modules.Pump import Pump
from modules.Vents import Valve

#TODO import PSU, servos


#TODO for test only

sensor_value = 1
value_threshold = 2
PlaceholderChargingStatus = 1
PlaceholderRemainingOperationTime = 2



#used variables:
# case, Valves



#SuperClassForModes

class Mode:
    def __init__(self, run):
        self.rum = run


#ClassForDefaultMode

class DefaultMode(Mode):

    def __init__(self, run):
        super().__init__(run)
        self.valve = Valve()
        self.pumps = Pump()
        self.magnet_array = MagnetArray(1)

    def run(self):

        print("Enter 'S' to go to Safe Mode ")

        #TODO define charging variables

        print('%s loaded, remaining operation time before charging: %s' % (
        PlaceholderChargingStatus, PlaceholderRemainingOperationTime))
        print("Options:")
        print("Enter 'vo' to open valve")
        print("Enter 'vc' to close valve")
        print("Enter 'pump' to pump specified volume")
        print("Enter 'pumps' to stop pump")
        print("Enter 'moves' to move servo specified parameter")
        print("Enter 'stops' to stop servo")
        print("Enter 'cpreset_1' to run combinded preset 1")
        print("Enter 'cpreset_2' to run combinded preset 2")
        print("Enter 'cpreset_3' to run combinded preset 3")
        print("Enter 'array_1' to run array preset 1")
        print("Enter 'array_2' to run array preset 2")
        print("Enter 'array_3' to run array preset 3")
        print("Enter 'quit' to quit program")
        print("Enter 'batt' to see charging status")

        user_input_operation = input(": ")

        if user_input_operation == "vo":

            # see documentation for required inputs

            valve_status = input("Valve Status: ").split()
            for i in range(0, len(valve_status)):
                valve_status[i] = int(valve_status[i])
            Safe_Mode = input("SafeMode: ")
            Safe_Mode = int(Safe_Mode)

            self.valve.valve_management(valve_status, Safe_Mode)

        elif user_input_operation == "vc":

            self.valve.valve_close()

        elif user_input_operation == "pump":

            self.pumps.pump_operation()

        elif user_input_operation == "pumps":

            self.pumps.stop_pump()

        elif user_input_operation == "moves":

            print("moving servo")
            # TODO insert servo

        elif user_input_operation == "stops":

            print("stopping servo")
            # TODO insert servo

        elif user_input_operation == "cpreset_1":

            print("running preset 1")
            # TODO insert combination

        elif user_input_operation == "cpreset_2":

            print("running preset 2")
            # TODO insert combination

        elif user_input_operation == "cpreset_3":

            print("running preset 3")
            # TODO insert combination

        elif user_input_operation == "array_1":

            # MA1 Job:
            Logger.log(Logger.LOG, "Turning on one Magnet for Septembertest")
            self.magnet_array.shift_register.set_magnet(2,1)

        elif user_input_operation == "array_2":

            Logger.log(Logger.LOG, "Horizontal movement with magnets is done")
            # Weg               1.          2. Magn;    next Time               ; and last time
            path_horizontal = [[[1, 1, 3000], [1, 2, 3000]], [[1, 2, 3000], [1, 3, 3000]], [[1, 3, 3000], [1, 4, 3000]]]
            self.magnet_array.move_it(path_horizontal, 1)

        elif user_input_operation == "array_3":

            # MA3 Job:
            # Test read config
            my_config = self.magnet_array.read_config("config_ma.csv")
            Logger.log(Logger.LOG, my_config)
            # Test list_to_path
            my_path = self.magnet_array.list_to_path(my_config)
            Logger.log(Logger.LOG, my_path)
            # Test move_it
            self.magnet_array.move_it(my_path, 1)

        elif user_input_operation == "batt":

            print('%s loaded, remaining operation time before charging: %s' % (
                PlaceholderChargingStatus, PlaceholderRemainingOperationTime))

        elif user_input_operation == "quit":

            print("quitting program")

        else:
            print("Unknown input")
            print("quitting program")

#ClassForSafeMode

class SafeMode(Mode):

    def __init__(self, run):
        super().__init__(run)
        self.valve = Valve()
        self.pumps = Pump()

    magnet_array = MagnetArray(1)

    def run(self):

        print("now running Safe-Mode")

        self.magnet_array.all_off()  # stop magnets
        self.pumps.stop_pump() # stop pumps
        self.valve.valve_close() # close valves

        #TODO stop servos, stop preset

        print("entered Safe-Mode, all operations halted")
        print("Enter 'OP' to continue operations")
        print("Enter 'quit' to quit program")

        # something that warns about resuming operations if some value is still critical

        user_input_safe_mode = input(": ")

        if user_input_safe_mode == "quit":
            print("shutting down")
        elif user_input_safe_mode == "OP":
            print("resuming Operations")
        else:
            print("Unknown input")

# ClassForModeDecisions


#if __name__ == '__main__' and sensor_value <= value_threshold:
default_mode = DefaultMode(1)
default_mode.run()


def pumpvariables():

    # Pump Operation Time & other parameters
    t = 10
    PWMAmplitude_Frequency = 50
    PWMAmp_DutyCycle = 50
    #Pin allocation
    PWMAmp_Pin = 25
    SHUTDOWN_Pin = 10

def startpump():
    ## Sample GPIO Out code
    GPIO.setmode(GPIO.BCM)
    #GPIO.setup(CLOCK_Pin, GPIO.OUT) """ operate Clock with internal Clock """
    GPIO.setup(PWMAmp_Pin, GPIO.OUT)
    GPIO.setup(SHUTDOWN_Pin, GPIO.OUT)

    p = GPIO.PWM(PWMAmp_Pin, PWMAmplitude_Frequency)
    p.start
    p.ChangeDutyCycle(PWMAmp_DutyCycle)
    GPIO.output(SHUTDOWN_Pin, GPIO.HIGH)


def stoppump():
    p.ChangeDutyCycle(0)  # Shutdown PWM "Amplitude"
    GPIO.output(SHUTDOWN_Pin, GPIO.LOW)  # Shutdown "Shutdown"
    GPIO.cleanup()
    exit()


def pumpoperation(t):
    startpump()
    sleep(t)
    stoppump()

pumpoperation(t=10)
