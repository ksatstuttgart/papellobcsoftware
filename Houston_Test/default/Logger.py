import time
import datetime
import os


class Logger:

    SAFE_TO_FILE = True

    # All the possible Log levels
    ERROR = "Error"
    WARNING = "Warning"
    EXCEPTION = "Exception"
    LOG = "Log"
    VERBOSE = "Verbose"

    @staticmethod
    def log(log_level, msg):
        '''
        if not verbose, the message is printed in the console and
        if supposed to, it is saved in the main logfile "Logs/MainLogs.log"
        and the specific log level logfile              "Logs/" + log_level + "Logs.log"
        :param log_level:
        :param msg: Message to be saved as string
        :return:
        '''
        if log_level != "Verbose":
            if Logger.SAFE_TO_FILE:
                if not os.path.isdir("Logs"):
                    os.mkdir("Logs")
                f_all = open("Logs/MainLogs.log", "a")
                f_all.write("[" + str(log_level) + "][" + str(Logger.get_current_time_str()) + "]: " + str(msg) + "\n")
                f_all.close()
                f_sep = open("Logs/" + log_level + "Logs.log", "a")
                f_sep.write("[" + str(log_level) + "][" + str(Logger.get_current_time_str()) + "]: " + str(msg) + "\n")
                f_sep.close()

            print("[" + str(log_level) + "][" + str(Logger.get_current_time_str()) + "]: " + str(msg))

    @staticmethod
    def get_current_time_millis():
        '''
        :return: Returns the time in milliseconds since the epoch as an Integer
        '''
        return int(round(time.time() * 1000))

    @staticmethod
    def get_current_time_str():
        '''
        :return: Returns the current date and time as [yyyy-mm-dd hh:mm:ss]
        '''
        return str(datetime.datetime.now()).split(".")[0]
