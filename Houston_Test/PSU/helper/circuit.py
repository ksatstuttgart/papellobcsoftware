#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from helper.adafruit_mcp23017 import Adafruit_MCP230XX as MCP230XX

class Circuit:

    def __init__(self, expander):
        """ initialise a Circuit with expander (adress, port) and FuelGauge Adress """        
        self._pin = expander[1]
        self._mcp = MCP230XX(busnum = 1, address = expander[0], num_gpios = 16)
        self._mcp.config(self._pin, MCP230XX.OUTPUT)
        
    def switchOn(self):
        """Switches Circuit on through enabling Port on IOExpander"""
        self._mcp.output(self._pin,1)
 
    def switchOff(self):
        """Switches Circuit off through enabling Port on IOExpander"""
        self._mcp.output(self._pin,1)
        
       