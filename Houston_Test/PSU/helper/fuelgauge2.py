#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from helper.i2c import i2c


class FuelGauge(object):
    """Class for reading sensor information about batteries"""
    
    

    
        
    def __init__(self, adress):
        """initialise the FuelGauge"""
        _GAUGES= [0x01,0x02,0x04,0x08,0x10]
        self._adress = adress[0]; #setting i2c adress of FuelGauge
        self._swadrs = adress[1]
        self._nbr = _GAUGES[adress[2]]
        self._i2c = i2c()

    
    def _readValue(self,key):
        self._i2c.write(self._swadrs,self._nbr)
        self._i2c.read(self._adress, key)
    
    def getTemp(self): #0x1e,0x1f
        """returns Temperature in 0.1K"""
        return self._readValue.read(0x0C)
       

    def getVoltage(self): #0x04,0x05
        """return Voltage in mV"""
        return self._readValue.read(0x08)
    
    def getCurrent(self): #0x10,0x11
        """returns Current in mA"""
        return self._readValue.read(0x10)
    
    def getCapacity(self): #0x0C,0x0D
        """returns Capacity in mAh"""
        return self._readValue.read(0x04)
    
    def getFullCapacity(self): #0x0E,0x0F
        """returns Capacity when full in mAh"""
        return self._readValue.read(0x06)
    
#    def getDesignCapacity(self): 
#        """returns Design Capacity in mAh"""
#        #return self._readValue.read(0x0C)
#        pass
        
#    def getCycleCount(self):  #0x2c
#        """returns the amount of Cycles the Batterie has run"""
#        return self._readValue.read(0x2C)



#Control() CNTL 0x00 and 0x01 NA RW
#Temperature() TEMP 0x02 and 0x03 0.1°K RW

#AveragePower() 0x18 and 0x19 mW R
#StateOfCharge() SOC 0x1C and 0x1D % R

#StateOfHealth() SOH 0x20 and 0x21 num / % R
#RemainingCapacityUnfiltered() 0x28 and 0x29 mAh R
#RemainingCapacityFiltered() 0x2A and 0x2B mAh R
#FullChargeCapacityUnfiltered() 0x2C and 0x2D mAh R
#FullChargeCapacityFiltered() 0x2E and 0x2F mAh R
#StateOfChargeUnfiltered() 0x30 and 0x31 % R
#TrueRemainingCapacity() 0x6A and 0x6B mAh R









