#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 18:39:18 2017

@author: chris
"""

from smbus import SMBus

class bq27441g1(object):

    def __init__(self, adress=0x55, bus=1):
        self._adress = adress
        self._bus = SMBus(bus)

    def _writeValue(self, cmd, text):
        self._bus.write_i2c_block_data(self._adress, cmd, text)

    def _readValue(self, cmd):
        self._bus.read_word_data(self._adress, cmd)


    def getTemp(self): #0x1e,0x1f
        """returns Temperature in 0.1K"""
        return self._readValue(0x1E)

    def getVoltage(self): #0x04,0x05
        """return Voltage in mV"""
        return self._readValue(0x04)

    def getCurrent(self): #0x10,0x11
        """returns Current in mA"""
        return self._readValue(0x10)

    def getCapacity(self): #0x0C,0x0D
        """returns Capacity in mAh"""
        return self._readValue(0x0C)

    def getFullCapacity(self): #0x0E,0x0F
        """returns Capacity when full in mAh"""
        return self._readValue(0x06)

    def getStateOfCharge(self): #0x1C,0x1D
        return self._readValue(0x1C)
