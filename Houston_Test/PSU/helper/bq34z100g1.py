#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 18:39:18 2017

@author: chris
"""

from smbus import SMBus

class bq34z100g1(object):

    def __init__(self,adress=0x20,bus=0):
        self._adress= adress
        self._bus = SMBus(bus)
    
    def _writeValue(self,cmd, text):
        self._bus.write_i2c_block_data(self._adress,cmd,text)
        pass
    
    def _readValue(self,cmd):
        self._bus.read_i2c_block_data(self._adress,cmd)
    
    
    def getTemp(self): #0x0c
        """returns Temperature in 0.1K"""
        return self._readValue(0x0C)
       
    def getVoltage(self): #0x08,0x09
        """return Voltage in mV"""
        return self._readValue(0x08)
    
    def getCurrent(self): #0x10,0x11
        """returns Current in mA"""
        return self._readValue(0x10)
    
    def getCapacity(self): #0x04,0x05
        """returns Capacity in mAh"""
        return self._readValue(0x04)
    
    def getFullCapacity(self): #0x06,0x07
        """returns Capacity when full in mAh"""
        return self._readValue(0x06)
    
    def getDesignCapacity(self): 
        """returns Design Capacity in mAh"""
        #return self._readValue.read(0x0C)
        pass
        
    def getCycleCount(self):  #0x2c
        """returns the amount of Cycles the Batterie has run"""
        return self._readValue(0x2C)
    
    