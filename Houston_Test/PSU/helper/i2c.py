#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from smbus import SMBus
#Ist nur eine eigene library weil ich noch nicht weiß, ob ich da noch was umbauen muss

class i2c:
    """Function to read and write to i2c"""
    
    def __init__(self):
        self.bus = SMBus(0)
    
    def read(self,adress,cmd):
        temp =  self.bus.read_i2c_block_data(adress,cmd)
        #if necessary swap high and lowbyte (needs to be tested)
        #temp = ((temp << 8) & 0xFF00) + (temp >> 8) 
        return temp
    
    def write(self,adress,cmd):
        self.bus.write_i2c_block_data(adress,cmd)
        return 1
    