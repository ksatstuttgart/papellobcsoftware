#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#from helper.fuelgauge import FuelGauge  #for Babysitter Board replace fuelgauge with fuelgauge2
from helper.bq27441g1 import bq27441g1 # no switch possible, dont need to force use a switch
#from helper.adafruit_mcp23017 import Adafruit_MCP230XX as MCP230XX
from helper.adafruit_mcp23017 import MCP230XX_GPIO as MCP230XX

DEBUG = 1

class Battery(bq27441g1):
    """Class to control the Batteries"""
    
    def __init__(self, expander, adress):
        """ initialise a Battery with expander (adress, port) and FuelGauge Adress """
        self._pin = expander[1]
#        self._mcp = MCP230XX(busnum = 1, address = expander[0], num_gpios = 16)
#        self._mcp.setup(self._pin, 0)
        mcp=mcp23017(1,expander[0])
        
        if DEBUG: print("Battery %d initialised" %(self._pin))
        super().__init__(adress)
        
    def Disconnect(self):
        """Disconnects Battery through disabling pin on IOExpander """
        self._mcp.output(self._pin,0)
        self.mypin()
    def Connect(self):
        """Connects Battery through enabling pin on IOExpander """
        self._mcp.output(self._pin,1)
