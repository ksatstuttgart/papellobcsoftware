#!/usr/bin/env python3
# -*- coding: utf-8 -*-


##wird später von Robin übernommen, da identisch

from smbus import SMBus

class TempSensor(object):
    def __init__(self,adress=0x18,bus=1):
        self._adress= adress
        self._bus = SMBus(bus)
    
    
    def _readValue(self,cmd):
        self._bus.read_i2c_block_data(self._adress,cmd)

        
    def getTemp(self): #0x1e,0x1f
        """returns Temperature in 0.1K"""
        return self._readValue(0x05)
    
temper = TempSensor()

while 1:
    print(temper.getTemp())