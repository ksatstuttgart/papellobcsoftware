#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from helper.battery import Battery
from helper.circuit import Circuit
from helper.menu import Menu, Item
from functools import partial

DEBUG = 1

class PSUTest(object):
    """Class for PSU testing purposes"""
    
    def __init__(self):
        self._EXPANDERADR = 0x20   # input Adress of IOExpander here (GPIOA is Battery Pins, GPIOB is Circuit Pins)
        self._SWITCH = 0x70
        self._BATPIN = [0,1,2,3,4]  # input Battery disconnect GPIOS inhere
        self._BATGAUGE = 0x68  # input BatteryGauge Adresses here
        self._CIRCPIN = [0,1,2] # input Pins for the circuit here
        self._BATTERIES = []
        for element in range(len(self._BATPIN)):
            self._BATTERIES.append(Battery([self._EXPANDERADR,self._BATPIN[element]],
                                     [self._BATGAUGE,self._SWITCH,self._BATPIN[element]]))
        self._CIRCUITS = [Circuit([self._EXPANDERADR,i]) for i in [0,1,2]]
#        self._BATTERIES = [Battery([self._EXPANDERADR,i],[self._BATGAUGE,self._SWITCH,i]) for i in self._BATPIN]
        
    def SwitchCircuit(self,Circ):
        #gets Circuit as item of ["3.3V","5V","12V"]
        print("Circuit" + str(Circ) + "changed or so")
         
    def BatInfo(self,Battery):
        #gets Battery Number as 0,1,2,3,4
#        print(self._BATTERIES[Battery].getTemperature())
        print("Fancy Information about Battery"+ str(Battery))
        
    def BatteryDisconnect(self,Battery):
        #gets Battery Number as 0,1,2,3,4
        #self._BATTERIES[Battery].disconnect()
        self._BATTERIES[Battery].Disconnect()
        print("Battery %s should be disconnected now" %(Battery))

    def BatteryConnect(self,Battery):
        self._BATTERIES[0].Connect()  
        self._BATTERIES[1].Connect()  
        self._BATTERIES[2].Connect()  
        self._BATTERIES[3].Connect()  
        self._BATTERIES[4].Connect()  
        print("Battery %s should be connected now" %(Battery))

    def CreateTestMenu(self):
        self.root = Menu("Root Menu")
        
        self.BatMenu = Menu("Battery Menu")
        
        self.BatInf = Menu("Battery Information") 
        for i in range(5):
            self.BatInf.addItem(Item("Information about Batttery " +
                                     str(i),partial(self.BatInfo,Battery =int(i))))
        
        self.BatCon = Menu("Battery Connect Menu")
        for i in range(5):
            self.BatCon.addItem(Item("Connect Battery " + 
                                     str(i),partial(self.BatteryConnect,Battery =int(i))))
        
        self.BatDis = Menu("Battery Disonnect Menu")
        for i in range(5):
            self.BatDis.addItem(Item("Disconnect Battery " + 
                                     str(i),partial(self.BatteryDisconnect,Battery =int(i))))
        
        
        
        self.BatMenu.addMenu(self.BatInf)
        self.BatMenu.addMenu(self.BatDis)
        self.BatMenu.addMenu(self.BatCon)
        
        self.Circmenu = Menu("Circuit Menu")
        for circ in ["3.3V","5V","12V"]:
            self.Circmenu.addItem(Item("Switch " + 
                                       circ + " Circuit",partial(self.SwitchCircuit, Circuit=circ)))
        
        
        self.root.addMenu(self.BatMenu)
        self.root.addMenu(self.Circmenu)
        return(self.root)


menu = PSUTest().CreateTestMenu()

menu.execute()
